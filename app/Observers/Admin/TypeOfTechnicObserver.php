<?php

namespace App\Observers\Admin;

use App\Models\TypeOfTechnic;
use Illuminate\Support\Str;

class TypeOfTechnicObserver
{
    /**
     *
     */
    public function creating(TypeOfTechnic $typeOfTechnic)
    {
        $this->setSlug($typeOfTechnic);
    }
    /**
     *
     */
    public function updating(TypeOfTechnic $typeOfTechnic)
    {
        $this->setSlug($typeOfTechnic);
//        $this->setCheckImage($typeOfTechnic);
    }
    /**
     * Handle the TypeOfTechnic "created" event.
     *
     * @param  \App\Models\TypeOfTechnic  $typeOfTechnic
     * @return void
     */
    public function created(TypeOfTechnic $typeOfTechnic)
    {
        //
    }

    /**
     * Handle the TypeOfTechnic "updated" event.
     *
     * @param  \App\Models\TypeOfTechnic  $typeOfTechnic
     * @return void
     */
    public function updated(TypeOfTechnic $typeOfTechnic)
    {
        //
    }

    /**
     * Handle the TypeOfTechnic "deleted" event.
     *
     * @param  \App\Models\TypeOfTechnic  $typeOfTechnic
     * @return void
     */
    public function deleted(TypeOfTechnic $typeOfTechnic)
    {
        //
    }

    /**
     * Handle the TypeOfTechnic "restored" event.
     *
     * @param  \App\Models\TypeOfTechnic  $typeOfTechnic
     * @return void
     */
    public function restored(TypeOfTechnic $typeOfTechnic)
    {
        //
    }

    /**
     * Handle the TypeOfTechnic "force deleted" event.
     *
     * @param  \App\Models\TypeOfTechnic  $typeOfTechnic
     * @return void
     */
    public function forceDeleted(TypeOfTechnic $typeOfTechnic)
    {
        //
    }

    /**
     * Если поле slug пустое, заполняем конверсионным полем title
     *
     * @param TypeOfTechnic $typeOfTechnic
     */
    protected function setSlug(TypeOfTechnic $typeOfTechnic)
    {
        if(empty($typeOfTechnic->slug)) {
            $typeOfTechnic->slug = Str::slug($typeOfTechnic->title);
        }
    }

    /**
     * Проверка на замену фото
     *
     * @param TypeOfTechnic $typeOfTechnic
     */
    protected function setCheckImage(TypeOfTechnic $typeOfTechnic)
    {
        dd($typeOfTechnic->isDirty('image'), $typeOfTechnic->image);
        if(!$typeOfTechnic->isDirty()) {
            $typeOfTechnic->image = $typeOfTechnic->getOriginal('image');
//            dd($typeOfTechnic->image, 'its_old');
        }
    }
}
