<?php

namespace App\Observers\Admin;

use App\Models\Order;
use Carbon\Carbon;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     */
    public function creating(Order $order)
    {
        $this->setStatusCreate($order);
        $this->setCreator($order);
        $this->setRunTime($order);
        $this->setPhoneClear($order);
    }
    /**
     * Handle the Order "updated" event.
     */
    public function updating(Order $order)
    {
        $this->setEditor($order);
        $this->setPhoneClear($order);
        $this->setRunTimeUpdate($order);
        $this->setStatusUpdate($order);
    }
    /**
     * Handle the Order "created" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        //
    }

    /**
     * Handle the Order "updated" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        //
    }

    /**
     * Handle the Order "deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the Order "restored" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the Order "force deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }

    /**
     * При создании заполняем поля status_id
     *
     * @param Order $order
     */
    protected function setStatusCreate(Order $order)
    {
        if(empty($order->master_id)) {
            $order->status_id = 1;
        }elseif($order->master_id != null){
            $order->status_id = 2;
        }
    }

    /**
     * При редактировании заполняем поля status_id
     *
     * @param Order $order
     */
    protected function setStatusUpdate(Order $order)
    {
        if(empty($order->master_id)) {
            $order->status_id = 1;
        }elseif($order->master_id != null && $order->status_id == 1){
            $order->status_id = 2;
        }
    }

    /**
     * При создании заполняем поля creator_id, editor_id
     *
     * @param Order $order
     */
    protected function setCreator(Order $order)
    {
        $order->creator_id = $order->editor_id = \Auth::id();
    }

    /**
     * При создании заполняем поля creator_id, editor_id
     *
     * @param Order $order
     */
    protected function setEditor(Order $order)
    {
        $order->editor_id = \Auth::id();
    }

    /**
     * При создании заполняем поля creator_id, editor_id, метод заглуша.
     *
     * @param Order $order
     */
    protected function setRunTime(Order $order)
    {
        if(empty($order->run_time)) {
            $order->run_time = Carbon::now();
        }else{
            $order->run_time = Carbon::parse($order->run_time)->format('Y-m-d H:m');
        }
    }

    /**
     * Проверка на run_time при редактировании, кастыль, если не редактируешь приходит Null
     *
     * @param Order $order
     */
    protected function setRunTimeUpdate(Order $order)
    {
        if($order->run_time == null) {
            $order->run_time = $order->getOriginal('run_time');
        }
    }

    /**
     * Отчищает номер телефона, возврещает только цыфры.
     *
     * @param Order $order
     */
    protected function setPhoneClear(Order $order)
    {
        $order->phone = preg_replace("/[^,.0-9]/", '', $order->phone);
    }
}
