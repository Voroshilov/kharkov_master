<?php

namespace App\Observers\Admin;

use App\Models\UserTelegram;

class UserTelegramObserver
{
    /**
     * Handle the UserTelegram "created" event.
     *
     * @param  \App\Models\UserTelegram  $userTelegram
     * @return void
     */
    public function created(UserTelegram $userTelegram)
    {
        //
    }

    /**
     * Handle the UserTelegram "updated" event.
     *
     * @param  \App\Models\UserTelegram  $userTelegram
     * @return void
     */
    public function updated(UserTelegram $userTelegram)
    {
        //
    }

    /**
     * Handle the UserTelegram "deleted" event.
     *
     * @param  \App\Models\UserTelegram  $userTelegram
     * @return void
     */
    public function deleted(UserTelegram $userTelegram)
    {
        //
    }

    /**
     * Handle the UserTelegram "restored" event.
     *
     * @param  \App\Models\UserTelegram  $userTelegram
     * @return void
     */
    public function restored(UserTelegram $userTelegram)
    {
        //
    }

    /**
     * Handle the UserTelegram "force deleted" event.
     *
     * @param  \App\Models\UserTelegram  $userTelegram
     * @return void
     */
    public function forceDeleted(UserTelegram $userTelegram)
    {
        //
    }
}
