<?php

namespace App\Observers\Admin;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Support\Str;

class PostObserver
{
    /**
     *
     */
    public function creating(Post $post)
    {
        $this->setCreator($post);
        $this->setSlug($post);
        $this->setPublishedAt($post);
        $this->setShortText($post);
    }
    /**
     *
     */
    public function updating(Post $post)
    {
        $this->setEditor($post);
        $this->setSlug($post);
        $this->setPublishedAt($post);
        $this->setShortText($post);
    }

    /**
     * Handle the Post "created" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function created(Post $post)
    {
        //
    }

    /**
     * Handle the Post "updated" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function updated(Post $post)
    {
        //
    }

    /**
     * Handle the Post "deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        //
    }

    /**
     * Handle the Post "restored" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function restored(Post $post)
    {
        //
    }

    /**
     * Handle the Post "force deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function forceDeleted(Post $post)
    {
        //
    }

    /**
     * Если дата публикации не установлена и происходит установка флага - опубликовать,
     * устанавлеваем дату на текущую
     *
     * @param Post $post
     */
    protected function setPublishedAt(Post $post)
    {
        if(empty($post->published_at && $post->is_published)) {
            $post->published_at = Carbon::now();
        }
    }

    /**
     * Если поле slug пустое, заполняем конверсионным полем title
     *
     * @param Post $post
     */
    protected function setSlug(Post $post)
    {
        if(empty($post->slug)) {
            $post->slug = Str::slug($post->title);
        }
    }

    /**
     * Если изменение записи, запысываем ID пользователя который менял ее, в поле editor_id
     *
     * @param Post $post
     */
    protected function setEditor(Post $post)
    {
        $post->editor_id = \Auth::id();
    }

    /**
     * При создании заполняем поля creator_id, editor_id
     *
     * @param Post $post
     */
    protected function setCreator(Post $post)
    {
        $post->creator_id = $post->editor_id = \Auth::id();
    }

    /**
     * При создании заполняем поля creator_id, editor_id
     *
     * @param Post $post
     */
    protected function setShortText(Post $post)
    {
        if(empty($post->short_text)) {
            $post->short_text = strip_tags(substr($post->text , 0 , 99));
        }
    }


}
