<?php

namespace App\Observers\Admin;

use App\Models\Manufacturer;
use Illuminate\Support\Str;

class ManufacturerObserver
{
    /**
     *
     */
    public function creating(Manufacturer $manufacturer)
    {
        $this->setSlug($manufacturer);
    }
    /**
     *
     */
    public function updating(Manufacturer $manufacturer)
    {
        $this->setSlug($manufacturer);
    }
    /**
     * Handle the Manufacturer "created" event.
     *
     * @param  \App\Models\Manufacturer  $manufacturer
     * @return void
     */
    public function created(Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Handle the Manufacturer "updated" event.
     *
     * @param  \App\Models\Manufacturer  $manufacturer
     * @return void
     */
    public function updated(Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Handle the Manufacturer "deleted" event.
     *
     * @param  \App\Models\Manufacturer  $manufacturer
     * @return void
     */
    public function deleted(Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Handle the Manufacturer "restored" event.
     *
     * @param  \App\Models\Manufacturer  $manufacturer
     * @return void
     */
    public function restored(Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Handle the Manufacturer "force deleted" event.
     *
     * @param  \App\Models\Manufacturer  $manufacturer
     * @return void
     */
    public function forceDeleted(Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Если поле slug пустое, заполняем конверсионным полем title
     *
     * @param Manufacturer $manufacturer
     */
    protected function setSlug(Manufacturer $manufacturer)
    {
        if(empty($manufacturer->slug)) {
            $manufacturer->slug = Str::slug($manufacturer->title);
        }
    }
}
