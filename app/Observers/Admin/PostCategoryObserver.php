<?php

namespace App\Observers\Admin;

use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Support\Str;

class PostCategoryObserver
{
    /**
     *
     */
    public function creating(PostCategory $post_category)
    {
        $this->setSlug($post_category);
        $this->setParent($post_category);
    }
    /**
     *
     */
    public function updating(PostCategory $post_category)
    {
        $this->setSlug($post_category);
        $this->setParent($post_category);
//        dd($post_category);
    }
    /**
     * Handle the PostCategory "created" event.
     *
     * @param  \App\Models\PostCategory  $post_category
     * @return void
     */
    public function created(PostCategory $post_category)
    {
        //
    }

    /**
     * Handle the PostCategory "updated" event.
     *
     * @param  \App\Models\PostCategory  $post_category
     * @return void
     */
    public function updated(PostCategory $post_category)
    {
        //
    }

    /**
     * Handle the PostCategory "deleted" event.
     *
     * @param  \App\Models\PostCategory  $post_category
     * @return void
     */
    public function deleted(PostCategory $post_category)
    {
        //
    }

    /**
     * Handle the PostCategory "restored" event.
     *
     * @param  \App\Models\PostCategory  $post_category
     * @return void
     */
    public function restored(PostCategory $post_category)
    {
        //
    }

    /**
     * Handle the PostCategory "force deleted" event.
     *
     * @param  \App\Models\PostCategory  $post_category
     * @return void
     */
    public function forceDeleted(PostCategory $post_category)
    {
        //
    }

    /**
     * Если поле slug пустое, заполняем конверсионным полем title
     *
     * @param Post $post_category
     */
    protected function setSlug(PostCategory $post_category)
    {
        if(empty($post_category->slug)) {
            $post_category->slug = Str::slug($post_category->title);
        }
    }

    /**
     * Если дата публикации не установлена и происходит установка флага - опубликовать,
     * устанавлеваем дату на текущую
     *
     * @param Post $post
     */
    protected function setParent(PostCategory $post_category)
    {
        if(empty($post_category->parent_id)) {
            $post_category->parent_id = 0;
        }
    }
}
