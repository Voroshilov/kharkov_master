<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     * @param $request
     * @param Closure $next
     * @param $role
     * @param null $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $permission = null)
    {
        if(!auth()->user()) {
            return redirect()->route('login');
        }

        if(!auth()->user()->hasRole($role)) {
            return redirect()->route('login');
        }
        if($permission !== null && !auth()->user()->can($permission)) {//????
            return redirect()->route('home');
        }
        return $next($request);
    }
}
