<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class UploadFileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Загрузка трех размеров картинок, logo = 100px, small = 300px, mid = 600px.
     * Путь оригинала - app/public/storage/ . $path_arr . / original
     * @param $request
     * @param null $old_image используется когда нужно заменить картинку,
     * передается старое название картинки
     *
     * @var mixed возвращает название картинки для DB
     */
    public function saveSmallImage($request, $path_arr, $old_image = null)
    {
        $width_image = [
            'logo' => 150,
            'small' => 300,
            'mid' => 600,
            'background' => 1920,
        ];//Значение высоты в пикселях, для ограничения размера картинки.

        if($request->file('image')){
            $original_path = $request->file('image')->store($path_arr['path'] . '/originals', $path_arr['options']);

            $img = Image::make(storage_path('app/public/' . $original_path));
            $img->backup();

            foreach ($width_image as $sizeName => $width) {
                $this->saveAvatarBySize($img, $sizeName, $width, $path_arr['path']);
            }

            return $img->basename;
        }else{
            return $old_image;
        }
    }


    public function savePostImage($request, $path_arr, $old_image = null)
    {
        $width_image = [
            'logo' => 150,
            'small' => 300,
            'mid' => 600,
        ];//Значение высоты в пикселях, для ограничения размера картинки.

        if($request->file('files')){
            $original_path = $request->file('files')->store($path_arr['path'] . '/originals', $path_arr['options']);

            $img = Image::make(storage_path('app/public/' . $original_path));
            $img->backup();

            foreach ($width_image as $sizeName => $width) {
                $this->saveAvatarBySize($img, $sizeName, $width, $path_arr['path']);
            }

            return $img->basename;
        }else{
            return $old_image;
        }
    }

    /**
     * @param $img - обьект картинки
     * @param $sizeName - Ключь в массиве размеров -
     *                 название дирректории в которую будет записваться фото при итерации
     * @param $width - значение размера картинки при итерации
     * @param $dir - дирректория для сохранения картнки
     * @return string
     *
     */
    protected function saveAvatarBySize($img, $sizeName, $width, $dir) {
        $folderPath =  "$dir/$sizeName";
        $folderPathFull = public_path($folderPath);
        $filePath = "$folderPath/$img->basename";

        if (!File::exists($folderPathFull)) {
            File::makeDirectory($folderPathFull, 0775, true, true);
        }

        $img->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path($filePath));
        // возвращает к состоянию когда был сделан бэкап $img->backup() (тогда была только обрезаность)
        $img->reset();

        return $folderPathFull;
    }

    /**
     * Загрузка оригинально фото, с созданием маленького дубликата
     * @param $request
     * @param null $old_image используется когда нужно заменить картинку,
     * передается старое название картинки
     *
     * @var mixed
     */
    public function saveOriginalImage($request, $image_path, $old_image = null)
    {
        $upload_file = $request->file('image');

        if($upload_file){
            $path = $request->file('image')->store($image_path['path'], $image_path['options']);

            return $path;
        }else{
            return $old_image;
        }
    }

    /**
     * Создает маленький дубликат
     */
    protected function makeSmall($path, $width_image)
    {
        $img = Image::make(storage_path('app/public/' . $path));
        $img->resize($width_image, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path('app/public/' . $path));
        dd(storage_path('app/public/' . $path));
    }
}
