<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as GuestController;
use App\Traits\HasTelegram;
use App\Traits\HasTextMessage;
use Illuminate\Http\Request;

class ContentController extends GuestController
{
    use HasTextMessage, HasTelegram;

    /**
     *
     * @return
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome_ukr');
    }

    /**
     * Отправляет сообщение оператору о оформленной заявке
     *
     * @return mixed
     */
    public function orderFromClient(Request $request)
    {
        $data = $request->all();
        $telegram_hash = '5350648216:AAHuhRdJJUAuGHcNogt7YjoK8AOdHpQYvP0';
        $this->notifyOrderToOperator($data, $telegram_hash);

        return redirect()
            ->route('index')
            ->with(['success' => 'Заявка отправлена. В течяении 5 минут мы вам презвоним']);
    }

    /**
     * Отправляет сообщение оператору о оформленной заявке
     *
     * @return mixed
     */
    public function contacts()
    {
//        return redirect()->route('index');
        return view('pages.contacts');
    }
    public function map()
    {
        return view('pages.map');
    }

    public function dryer()
    {
        return view('pages.dryer');
    }

    public function sd()
    {
        return view('sd');
    }

    public function kiev()
    {
        return view('kiev');
    }
    // Страница для елеутро проводки
    public function electroprovodka()
    {
        return view('pages.electroprovodka');
    }
    // Сантехника
    public function plumbing()
    {
        return view('pages.plumbing');
    }

    //keys pages area
    public function saltovka()
    {
        return view('pages.area.saltovka');
    }
    public function alekseevka()
    {
        return view('pages.area.alekseevka');
    }

    //keys pages machines
    public function bosch()
    {
        return view('pages.machines.bosch');
    }
    public function electrolux()
    {
        return view('pages.machines.electrolux');
    }
    public function indesit()
    {
        return view('pages.machines.indesit');
    }
    public function samsung()
    {
        return view('pages.machines.samsung');
    }
    public function zanussi()
    {
        return view('pages.machines.zanussi');
    }
    public function ariston()
    {
        return view('pages.machines.ariston');
    }
    public function ardo()
    {
        return view('pages.machines.ardo');
    }
    public function lg()
    {
        return view('pages.machines.lg');
    }





}
