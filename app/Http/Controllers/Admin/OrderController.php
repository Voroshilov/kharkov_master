<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OrderCreateRequest;
use App\Http\Requests\Admin\OrderUpdateRequest;
use App\Http\Requests\Admin\Searches\PostSearchRequest;
use App\Models\Order;
use App\Models\Role;
use App\Models\User;
use App\Repositories\Admin\ManufacturerRepository;
use App\Repositories\Admin\OrderRepository;
use App\Repositories\Admin\OrderStatusRepository;
use App\Repositories\Admin\OrderTimeRepository;
use App\Repositories\Admin\TypeOfTechnicRepository;
use App\Repositories\Admin\UserRepository;
use App\Traits\HasTelegram;
use App\Traits\HasTextMessage;
use Carbon\Carbon;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Param;

class OrderController extends BaseController
{
    use HasTextMessage, HasTelegram;

    /**
     * @var OrderRepository
     *
     * @var mixed
     */
    private $order_repository;

    /**
     * @var ManufacturerRepository
     *
     * @var mixed
     */
    private $manufacturer_repository;

    /**
     * @var OrderStatusRepository
     *
     * @var mixed
     */
    private $status_repository;

    /**
     * @var TypeOfTechnicRepository
     *
     * @var mixed
     */
    private $tot_repository;

    /**
     * @var UserRepository
     *
     * @var mixed
     */
    private $user_repository;

    /**
     * @var OrderTimeRepository
     *
     * @var mixed
     */
    private $order_time_repository;

    /**
     * @var $permition
     */
    private $permission = 'orders';
    /**
     * @var $roles
     */
    private $role = 'master';


    public function __construct()
    {
        parent::__construct();

        $this->order_repository = app(OrderRepository::class);
        $this->manufacturer_repository = app(ManufacturerRepository::class);
        $this->status_repository = app(OrderStatusRepository::class);
        $this->tot_repository = app(TypeOfTechnicRepository::class);
        $this->user_repository = app(UserRepository::class);
        $this->order_time_repository = app(OrderTimeRepository::class);

//        $this->get_permission = app(UserRepository::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index($search = null)
    {
        $pages = 20;
        if(!empty($search)) {
            $orders = $search;
        } else {
            $orders = $this->order_repository->getAllWithPaginate($pages);
        }
//        $orders = $this->order_repository->getAllWithPaginate($pages);
        $manufacturers = $this->manufacturer_repository->getActiveComboBox();
        $statuses = $this->status_repository->getActiveComboBox();
        $type_of_technics = $this->tot_repository->getActiveComboBox();
        $masters = $this->user_repository->getUsersByRole($this->role);

        return view('admin.orders.index', compact(
            'orders',
            'manufacturers',
            'statuses',
            'type_of_technics',
            'masters',
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $masters = $this->user_repository->getUsersByRole($this->role);
        $manufacturers = $this->manufacturer_repository->getActiveComboBox();
        $type_of_technics = $this->tot_repository->getActiveComboBox();
        $order_times = $this->order_time_repository->getActiveComboBox();

        return view('admin.orders.create', compact(
            'manufacturers',
            'type_of_technics',
            'masters',
            'order_times',
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  OrderCreateRequest  $request
     * @return mixed
     */
    public function store(OrderCreateRequest $request)
    {
        //Сделать таюлицу для них
        $telegram_hash = '5350648216:AAHuhRdJJUAuGHcNogt7YjoK8AOdHpQYvP0';

        $data = $request->all();

        //Проверка на прошлое время для начала заявки
//        if(strtotime($data['run_time']) <= strtotime(Carbon::now())) {
//            return redirect()->route('admin.orders.create')
//                ->withErrors(['errors' => 'Время выбрано в рошлом, так не может быть!!!'])
//                ->withInput();
//        }


        $result = Order::create($data);

        if($result->master_id != null) {
            $this->notifyOrderToMaster($result, $telegram_hash);
        }

//        dd(12332131);
        if ($result) {
            return redirect()
                ->route('admin.orders.index')
                ->with(['success' => "Заявка '" . $result->id . "' Создано успешно"]);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка создания'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        $order = $this->order_repository->getOne($id);

        return view('admin.orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        $order = $this->order_repository->getOne($id);

        $masters = $this->user_repository->getUsersByRole($this->role);
        $manufacturers = $this->manufacturer_repository->getActiveComboBox();
        $statuses = $this->status_repository->getActiveComboBox();
        $type_of_technics = $this->tot_repository->getActiveComboBox();
        $order_times = $this->order_time_repository->getActiveComboBox();

        return view('admin.orders.edit', compact(
            'order',
            'manufacturers',
            'statuses',
            'type_of_technics',
            'masters',
            'order_times',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderUpdateRequest $request, $id)
    {
        $data = $request->all();
        $order = Order::find($id);

        $result = $order->update($data);

        if ($result) {
            return redirect()
                ->route('admin.orders.index')
                ->with(['success' => "Заявка '" . $order->id . "' отредактирована успешно!"]);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка создания'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd('Method Delete');
    }

    /**
     * Поиск по полю title
     */
    public function search(PostSearchRequest $request)
    {
        $orders = $this->order_repository->getSearch($request);

        return $this->index($orders);
    }
}
