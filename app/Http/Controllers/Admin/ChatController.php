<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Admin\ChatRepository;
use App\Repositories\Admin\MessageRepository;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    /**
     * @var ChatRepository
     *
     * @var mixed
     */
    private $chat_repository;
    /**
     * @var MessageRepository
     *
     * @var mixed
     */
    private $message_repository;
    /**
     * @var UserRepository
     *
     * @var mixed
     */
    private $user_repository;

    public function __construct()
    {
        parent::__construct();

        $this->chat_repository = app(ChatRepository::class);
        $this->message_repository = app(MessageRepository::class);
        $this->user_repository = app(UserRepository::class);
    }

    public function index()
    {
        $chats = $this->chat_repository->getUserChat();

        return view('admin.chats.index', compact('chats',));
    }

    public function show($chat_id)
    {
        $messages = $this->message_repository->getMessageFromChat($chat_id);
        $chat = $this->chat_repository->getById($chat_id);

        //Находим юзера с которым ведем чат
        if ($chat->first_user_id != \Auth::user()->id) {
            $user = $this->user_repository->getOne($chat->first_user_id);
        } else {
            $user = $this->user_repository->getOne($chat->second_user_id);
        }

        return view('admin.chats.show_chat', compact('messages','chat', 'user'));
    }

    public function store()
    {
        $chats = $this->chat_repository->getUserChat();

        return view('admin.chats.index', compact('chats',));
    }
}
