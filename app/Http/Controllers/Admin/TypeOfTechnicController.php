<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadFileController;
use App\Http\Requests\Admin\Searches\TypeOfTechnicSearchRequest;
use App\Http\Requests\Admin\TypeOfTechnicCreateRequest;
use App\Http\Requests\Admin\TypeOfTechnicUpdateRequest;
use App\Models\TypeOfTechnic;
use App\Repositories\Admin\TypeOfTechnicRepository;
use Intervention\Image\ImageManagerStatic as Image;

class TypeOfTechnicController extends Controller
{
    /**
     * @var TypeOfTechnicRepository
     *
     * @var mixed
     */
    private $type_of_technic;

    /**
     * Переменная для загрузки фото
     *
     * @var mixed
     */
    private $image_path = [
            'path' => 'uploads_images/type_of_technics',
            'options' => 'public',
        ];

    /**
     * Переменная для инициализации класса загрузки фото
     */

    private $upload_image;

    public function __construct()
    {
        parent::__construct();

        $this->type_of_technic = app(TypeOfTechnicRepository::class);
        $this->upload_image = app(UploadFileController::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $type_of_technics = $this->type_of_technic->getAllWithPaginate(20);

        return view('admin.type_of_technics.index', compact('type_of_technics'));
    }

    /**
     * Show the form for creating a new resource.
     *
//     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.type_of_technics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(TypeOfTechnicCreateRequest $request)
    {
        $data = $request->all();

        $data['image'] = $this->upload_image->saveSmallImage($request, $this->image_path);

        $result = TypeOfTechnic::create($data);

        if ($result) {
            return redirect()
                ->route('admin.type_of_technics.index')
                ->with(['success' => "Создано успешно"]);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка создания'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        $type_of_technic = $this->type_of_technic->getShow($id);

        if (empty($manufacturer)) {
            return back()
                ->withErrors(['msg' => "Запись с идентификатором - [{$id}] не найдена"]);
        }

        return view('admin.type_of_technics.show', compact('type_of_technic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        $type_of_technic = $this->type_of_technic->getEdit($id);

        return view('admin.type_of_technics.edit', compact('type_of_technic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return mixed
     */
    public function update(TypeOfTechnicUpdateRequest $request, $id)
    {
        $data = $request->all();

        $type_of_technic = TypeOfTechnic::find($id);

        $data['image'] = $this->upload_image
            ->saveSmallImage($request, $this->image_path, $type_of_technic->getOriginal('image'));

        if (empty($type_of_technic)) {
            return back()
                ->withErrors(['msg' => "Запись с идентификатором - [{$id}] не найдена"])
                ->withInput();
        }

        $result = $type_of_technic->update($data);

        if ($result) {
            return redirect()
                ->route('admin.type_of_technics.index')
                ->with(['success' => 'Сохранено успешно']);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка сохранения'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return mixed
     */
    public function destroy($id)
    {
        $tot = $this->type_of_technic->getOne($id);

        if ($tot->post->count()) {
            $errors[] = 'Нельзя удалять технику, привязанную к статьям';
        }

        if(isset($errors)) {
            return back()->withErrors($errors);
        }
        //Удаление файла вместе с удалением записи
//        $image = $tot->image;
//        dd($image);
//        if ($image) {
////            \Storage::disk('public')->delete('/storage/' . $image);
//            dd(\Storage::disk('public')->delete('/storage/' . $image));
//        }

        $result = TypeOfTechnic::find($id)->forceDelete();

        if($result) {
            return redirect()
                ->route('admin.type_of_technics.index')
                ->with(['success' => 'Успешно удалено']);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка удаления!']);
        }
    }

    /**
     * Поиск по полю title
     */
    public function search(TypeOfTechnicSearchRequest $request)
    {
        $type_of_technics = $this->type_of_technic->getSearch($request);

        return view('admin.type_of_technics.index', compact('type_of_technics'));
    }
}
