<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController as GuestController;
use Illuminate\Http\Request;

abstract class BaseController extends GuestController
{
    public function __construct()
    {
        parent::__construct();
    }
}
