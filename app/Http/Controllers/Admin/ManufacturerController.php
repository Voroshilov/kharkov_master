<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadFileController;
use App\Http\Requests\Admin\ManufacturerCreateRequest;
use App\Http\Requests\Admin\ManufacturerUpdateRequest;
use App\Http\Requests\Admin\Searches\ManufacturerSearchRequest;
use App\Models\Manufacturer;
use App\Repositories\Admin\ManufacturerRepository;

class ManufacturerController extends Controller
{
    /**
     * @var ManufacturerRepository
     *
     * @var mixed
     */
    private $manufacturer_repository;

    /**
     * Переменная для загрузки фото
     *
     * @var mixed
     */
    private $image_path = [
        'path' => 'uploads_images/manufacturers',
        'options' => 'public',
    ];

    /**
     * Переменная для инициализации класса загрузки фото
     */
    private $upload_image;

    public function __construct()
    {
        parent::__construct();

        $this->manufacturer_repository = app(ManufacturerRepository::class);
        $this->upload_image = app(UploadFileController::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $manufacturers = $this->manufacturer_repository->getAllWithPaginate(20);

        return view('admin.manufacturers.index', compact('manufacturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
//     * @return page admin.manufacturers.creat
     */
    public function create()
    {
        return view('admin.manufacturers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function store(ManufacturerCreateRequest $request)
    {
        $data = $request->all();

        $data['image'] =
            $this->upload_image->saveSmallImage($request, $this->image_path);

        $result = Manufacturer::create($data);

        if ($result) {
            return redirect()
                ->route('admin.manufacturers.index')
                ->with(['success' => "Производитель '" . $result->title . "' Создано успешно"]);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка создания'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        $manufacturer = $this->manufacturer_repository->getShow($id);

        if (empty($manufacturer)) {
            return back()
                ->withErrors(['msg' => "Запись с идентификатором - [{$id}] не найдена"]);
        }

        return view('admin.manufacturers.show', compact('manufacturer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        $manufacturer = $this->manufacturer_repository->getEdit($id);

        return view('admin.manufacturers.edit', compact('manufacturer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return mixed
     */
    public function update(ManufacturerUpdateRequest $request, $id)
    {
        $data = $request->all();

        $manufacturer = Manufacturer::find($id);

        $data['image'] = $this->upload_image
            ->saveSmallImage($request, $this->image_path, $manufacturer->getOriginal('image'));

        if (empty($manufacturer)) {
            return back()
                ->withErrors(['msg' => "Запись с идентификатором - [{$id}] не найдена"])
                ->withInput();
        }

        $result = $manufacturer->update($data);

        if ($result) {
            return redirect()
                ->route('admin.manufacturers.index')
                ->with(['success' => 'Сохранено успешно']);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка сохранения'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
//     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $manufacturer = $this->manufacturer_repository->getOne($id);

        if ($manufacturer->post->count()) {
            $errors[] = 'Нельзя удалять производитея, который привязанн к статьям';
        }

        if(isset($errors)) {
            return back()->withErrors($errors);
        }

        $result = Manufacturer::find($id)->forceDelete();

        if($result) {
            return redirect()
                ->route('admin.manufacturers.index')
                ->with(['success' => 'Успешно удалено']);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка удаления!']);
        }
    }

    /**
     * Поиск по полю title
     */
    public function search(ManufacturerSearchRequest $request)
    {
        $manufacturers = $this->manufacturer_repository->getSearch($request);

        return view('admin.manufacturers.index', compact('manufacturers'));
    }
}
