<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use App\Repositories\Admin\PermissionRepository;
use App\Repositories\Admin\RoleRepository;
use App\Repositories\Admin\StaffRepository;
use Illuminate\Http\Request;

class StaffController extends BaseController
{
    /**
     * @var StaffRepository
     *
     * @var mixed
     */
    private $staff_repository;

    /**
     * @var RoleRepository
     *
     * @var mixed
     */
    private $role_repository;

    /**
     * @var PermissionRepository
     *
     * @var mixed
     */
    private $permission_repository;

    public function __construct()
    {
        parent::__construct();

        $this->staff_repository = app(StaffRepository::class);
        $this->role_repository = app(RoleRepository::class);
        $this->permission_repository = app(PermissionRepository::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return
     */
    public function index()
    {
        $count = 20;
        $role = [
            'admin',
            'master',
            'senior-master',
            'author',
            'trainee',
        ];
        $users = $this->staff_repository->getAllWithPaginateSQL($role, $count);
//        dd($users[0]->id);

        return view('admin.staffs.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        $roles = $this->role_repository->getAll();
        $permissions = $this->permission_repository->getAll();
//        dd($roles);

        return view('admin.staffs.create', compact('roles', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $result = User::create($data);

        $this->allPermissions($result, $request->permission_id);

        $role = Role::where('id',$request->role_id)->first();
        $result->roles()->attach($role);


        if ($result) {
            return redirect()
                ->route('admin.staffs.index')
                ->with(['success' => "Работник '" . $result->firest_name . "' Создано успешно"]);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка создания'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        $user = $this->user_repository->getOne($id);

        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function allPermissions($user, $all_permissions)
    {
        foreach ($all_permissions as $perm) {
            $permission = Role::where('id', $perm)->first();
            $user->permissions()->attach($permission);
        }
    }
}
