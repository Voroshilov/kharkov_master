<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserTelegramCreateRequest;
use App\Http\Requests\Admin\UserTelegramUpdateRequest;
use App\Repositories\Admin\UserTelegramRepository;
use Illuminate\Http\Request;

class UserTelegramController extends BaseController
{
    /**
     * @var UserTelegramRepository
     *
     * @var mixed
     */
    private $user_telegram_repository;

    public function __construct()
    {
        parent::__construct();

        $this->user_telegram_repository = app(UserTelegramRepository::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return mixed posts
     */
    public function index()
    {
//        $post_categories = $this->post_category_repository->getAllWithPaginate(10);
//
//        return view('admin.post_categories.index', compact('post_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed posts
     */
    public function create()
    {
//        $general_categories = $this->post_category_repository->getForComboBox();
//
//        return view('admin.post_categories.create', compact('general_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
    //     * @param  App\Http\Requests\Admin\PostCreateRequest;
    //     * @return \Illuminate\Http\Response
     */
    public function store(UserTelegramCreateRequest $request)
    {
//        $data = $request->all();
//
//        $result = PostCategory::create($data);
//
//        if ($result) {
//            return redirect()
//                ->route('admin.post_categories.index')
//                ->with(['success' => 'Категорию Создано успешно']);
//        } else {
//            return back()
//                ->withErrors(['msg' => 'Ошибка создания'])
//                ->withInput();
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return mixed post
     */
    public function show($id)
    {
//        return redirect()->route('admin.post_categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @param PostCategoryRepository $PostCategoryRepository
     * @return mixed post
     * @return mixed categories
     */
    public function edit($id, PostCategoryRepository $postCategoryRepository)
    {
//        $category = $postCategoryRepository->getEdit($id);
//        $general_categories = $postCategoryRepository->getForComboBox();
//
//        return view('admin.post_categories.edit', compact('category', 'general_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
    //     * @param  \Illuminate\Http\Request  $request
    //     * @param  int  $id
     * @return mixed post
     */
    public function update(UserTelegramUpdateRequest $request, $id)
    {
//        $data = $request->all();
//
//        $post_category = PostCategory::find($id);
//
//        if (empty($post_category)) {
//            return back()
//                ->withErrors(['msg' => "Запись с идентификатором - [{$id}] не найдена"])
//                ->withInput();
//        }
//
//        $result = $post_category->update($data);
//
//        if ($result) {
//            return redirect()
//                ->route('admin.post_categories.index', $post_category->id)
//                ->with(['success' => 'Сохранено успешно']);
//        } else {
//            return back()
//                ->withErrors(['msg' => 'Ошибка сохранения'])
//                ->withInput();
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
    //     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $category = $this->post_category_repository->getOne($id);
//
//        if ($category->childrenCategory->count()) {
//            $errors[] = 'Нельзя удалить категорию с дочерними категориями';
//        }
//        if ($category->post->count()) {
//            $errors[] = 'Нельзя удалить категорию, которая содержит посты';
//        }
//
//        if(isset($errors)) {
//            return back()->withErrors($errors);
//        }
//
//        $result = PostCategory::find($id)->forceDelete();
//
//        if($result) {
//            return redirect()
//                ->route('admin.post_categories.index')
//                ->with(['success' => 'Успешно удалено']);
//        } else {
//            return back()
//                ->withErrors(['msg' => 'Ошибка удаления!']);
//        }
    }

    /**
     * Поиск по полю title
     */
    public function search(Request $request)
    {
        $post_categories = $this->post_category_repository->getSearch($request);

        return view('admin.post_categories.index', compact('post_categories'));
    }
}
