<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadFileController;
use App\Http\Requests\Admin\PostCreateRequest;
use App\Http\Requests\Admin\PostUpdateRequest;
use App\Http\Requests\Admin\Searches\PostSearchRequest;
use App\Models\Manufacturer;
use App\Models\Post;
use App\Models\PostCategory;
use App\Repositories\Admin\ManufacturerRepository;
use App\Repositories\Admin\PostCategoryRepository;
use App\Repositories\Admin\PostRepository;
use App\Repositories\Admin\PostLikeRepository;
use App\Repositories\Admin\PostViewRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PostController extends BaseController
{
    /**
     * @var PostRepository
     *
     * @var mixed
     */
    private $post_repository;

    /**
     * @var PostCategoryRepository
     *
     * @var mixed
     */
    private $post_category_repository;

    /**
     * @var ManufacturerRepository
     *
     * @var mixed
     */
    private $manufacturer_repository;

    /**
     * @var PostViewRepository
     *
     * @var mixed
     */
    private $post_view_repository;

    /**
     * @var PostLikeRepository
     *
     * @var mixed
     */
    private $post_like_repository;

    /**
     * Переменная для инициализации класса загрузки фото
     */

    /**
     * Переменная для загрузки фото
     *
     * @var mixed
     */
    private $image_path = [
        'path' => 'uploads_images/posts',
        'options' => 'public',
    ];

    /**
     * Переменная для инициализации класса загрузки фото
     */
    private $upload_image;

    public function __construct()
    {
        parent::__construct();

        $this->post_repository = app(PostRepository::class);
        $this->post_category_repository = app(PostCategoryRepository::class);
        $this->manufacturer_repository = app(ManufacturerRepository::class);
        $this->upload_image = app(UploadFileController::class);
        $this->post_like_repository = app(PostLikeRepository::class);
        $this->post_view_repository = app(PostViewRepository::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return mixed posts
     */
    public function index($search = null)
    {
        $pages = 20;
        if(!empty($search)) {
            $posts = $search;
        } else {
            $posts = $this->post_repository->getAllWithPaginate($pages);
        }
//        $posts = $this->post_repository->getAllWithPaginate(6);
        $categories = $this->post_category_repository->getActiveComboBox();

        return view('admin.posts.index', compact('posts', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return
     */
    public function create()
    {
        $categories = $this->post_category_repository->getActiveComboBox();
        $manufacturers = $this->manufacturer_repository->getActiveComboBox();

        return view('admin.posts.create', compact(
            'categories',
            'manufacturers'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
    //     * @param  App\Http\Requests\Admin\PostCreateRequest;
    //     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {
//        dd($request->file('files'));
        $data = $request->all();
        $images = $request->file('files');
        $data['image'] =
            $this->upload_image->savePostImage($request, $this->image_path);
//        dd($data);
//        $image = $this->upload_image->setUserAvatar();
//        dd('End');

        $result = Post::create($data);

        if ($result) {
            return redirect()
                ->route('admin.posts.index')
                ->with(['success' => "Запись в категорию '" . $result->category->title . "' Создано успешно"]);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка создания'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return mixed post
     */
    public function show($id)
    {
        $post = $this->post_repository->getShow($id);
        if (empty($post)) {
            return back()
                ->withErrors(['msg' => "Запись с идентификатором - [{$id}] не найдена"]);
        }
        $data['views'] = $this->post_view_repository->getCountViews($post->id);
        $data['likes'] = $this->post_like_repository->getLikes($post->id);
        $data['dislikes'] = $this->post_like_repository->getDisLikes($post->id);

        return view('admin.posts.show', compact('post', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return mixed post
     * @return mixed categories
     */
    public function edit($id)
    {
        $post = $this->post_repository->getEdit($id);
        $categories = $this->post_category_repository->getActiveComboBox();
        $manufacturers = $this->manufacturer_repository->getActiveComboBox();

        return view('admin.posts.edit', compact(
            'post',
            'categories',
            'manufacturers',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
    //     * @param  \Illuminate\Http\Request  $request
    //     * @param  int  $id
         * @return mixed post
     */
    public function update(PostUpdateRequest $request, $id)
    {
        $data = $request->all();
//        dd($data);

        $post = Post::find($id);

        $data['image'] = $this->upload_image
            ->saveSmallImage($request, $this->image_path, $post->getOriginal('image'));
//        dd('End');
        if(empty($post->published_at) && $data['is_published']) {
            $data['published_at'] = Carbon::now();
        }

        if (empty($post)) {
            return back()
                ->withErrors(['msg' => "Запись с идентификатором - [{$id}] не найдена"])
                ->withInput();
        }

        $result = $post->update($data);

        if ($result) {
            return redirect()
                ->route('admin.posts.index')
                ->with(['success' => 'Сохранено успешно']);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка сохранения'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
//     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Post::destroy($id);

        if($result) {
            return redirect()
                ->route('admin.posts.index')
                ->with(['success' => 'Успешно удалено']);
        } else {
            return back()
                ->withErrors(['msg' => 'Ошибка удаления!']);
        }
    }

    /**
     * Поиск по полю title
     */
    public function search(PostSearchRequest $request)
    {
        $posts = $this->post_repository->getSearch($request);
//        $categories = $this->post_category_repository->getActiveComboBox();
//
//        return view('admin.posts.index', compact('posts', 'categories'));

        return $this->index($posts);
    }

    /**
     * Загружает фото для Post методом Ajax
     */
    public function uploadImage(Request $request)
    {
//        dd($request->all());

        $db_url = $this->uploadFile($request);

        return $db_url;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function uploadFile($request)
    {
        if ($request->isMethod('post') && $request->file('file_url')) {

            $file = $request->file('file_url');
            $upload_folder = 'public/posts';
            $filename = rand(100000000, 999999999) . '_' . Carbon::now()->format('Hms') . '.' . $file->clientExtension(); // image.jpg
            $db_url = '/posts/' . $filename;
            $file_url = storage_path('app/public/posts/') . $filename;

//            dd(Storage::putFileAs($upload_folder, $file, $filename));
            Storage::putFileAs($upload_folder, $file, $filename);

            if (File::exists($file_url)) {
                File::copy($file_url, public_path($db_url));
            }

            return $db_url;
        }

        return null;
    }
}
