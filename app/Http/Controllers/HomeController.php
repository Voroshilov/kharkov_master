<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\BaseController as GuestController;

class HomeController extends GuestController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function home()
    {
        $user = \Auth::user();
        //вернёт true для текущего пользователя, если ему дано право управлять пользователями
//        Gate::allows('income-statistics');

        return view('home', compact('user'));
    }

    public function contact()
    {
        return view('contact');
    }


}
