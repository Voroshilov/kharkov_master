<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\Blog\SetCommentRequest;
use App\Models\Comment;
use App\Repositories\Blog\CommentRepository;
use Illuminate\Http\Request;

class CommentController extends BaseController
{
    /**
     * @var CommentRepository
     *
     * @var mixed
     */
    private $comment_repository;

    public function __construct()
    {
        parent::__construct();

        $this->comment_repository = app(CommentRepository::class);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setComment(Request $request)
    {
        $user = \Auth::user();
        $data = $request->all();

        if ($user) {
            $data['user_id'] = $user->id;
            $data['name'] = $user->first_name;
        }

        Comment::create($data);

        return redirect()->back()->withInput([
            'msg' => 'Спасибо! Ваш коментарий будет обработан и опубликован.'
        ]);
    }

}
