<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Controller;
use App\Models\PostLike;
use App\Repositories\Blog\CommentRepository;
use App\Repositories\Blog\PostCategoryRepository;
use App\Repositories\Blog\PostLikeRepository;
use App\Repositories\Blog\PostRepository;
use App\Repositories\Blog\PostViewRepository;
use Illuminate\Http\Request;

class PostController extends BaseController
{
    /**
     * @var PostCategoryRepository
     *
     * @var mixed
     */
    private $post_category_repository;

    /**
     * @var PostRepository
     *
     * @var mixed
     */
    private $post_repository;

    /**
     * @var PostRepository
     *
     * @var mixed
     */
    private $post_view_repository;

    /**
     * @var PostRepository
     *
     * @var mixed
     */
    private $post_like_repository;

    /**
     * @var CommentRepository
     *
     * @var mixed
     */
    private $comment_repository;

    public function __construct()
    {
        parent::__construct();

        $this->post_repository = app(PostRepository::class);
        $this->post_category_repository = app(PostCategoryRepository::class);
        $this->post_view_repository = app(PostViewRepository::class);
        $this->post_like_repository = app(PostLikeRepository::class);
        $this->comment_repository = app(CommentRepository::class);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $pages = 10;

        $posts = $this->post_repository->getAll($pages);
        $categories = $this->post_category_repository->getComboBox();
        $view_category = 'index';

        return view('blog.posts.index', compact('posts', 'categories', 'view_category'));
    }

    public function indexByCategory($category)
    {
        $pages = 10;

        $posts = $this->post_repository->getByCategorySQL($category, $pages);
        $categories = $this->post_category_repository->getComboBox();
        $view_category = $this->post_category_repository->getCategortyBySlug($category);

//        dd($view_category);
        return view('blog.posts.index', compact('posts', 'categories', 'view_category'));
    }

    public function show($slug, Request $request)
    {
        $user = \Auth::user();

        $post = $this->post_repository->getOnBySlug($slug);
        $comments = $this->comment_repository->getCommentsToPost($post->id);
        $this->post_view_repository->setViews($post->id, $request->ip());
        $data = $this->likesAndViews($post, $user);

//        dd($data);

        return view('blog.posts.show', compact(
            'post',
            'comments',
            'user',
            'data'
        ));
    }

    /**
     * Возвращает информацию по лайкам и просмотрам
     * @param $post
     * @return array
     */
    public function likesAndViews($post, $user)
    {
        $data = [];

        if ($user) {
            $user_like = $this->post_like_repository->getUserLike($post->id, $user->id);
            if(isset($user_like) && $user_like->is_like == 1) {
                $data['user_like'] = 'fa-thumbs-up';
                $data['user_dislike'] = 'fa-thumbs-o-down';
            } elseif(isset($user_like) && $user_like->is_like == 0) {
                $data['user_like'] = 'fa-thumbs-o-up';
                $data['user_dislike'] = 'fa-thumbs-down';
            } else {
                $data['user_like'] = 'fa-thumbs-o-up';
                $data['user_dislike'] = 'fa-thumbs-o-down';
            }
        } else {
            $data['user_like'] = 'fa-thumbs-o-up';
            $data['user_dislike'] = 'fa-thumbs-o-down';
        }

        $data['views'] = $this->post_view_repository->getCountViews($post->id);
        $data['likes'] = $this->post_like_repository->getLikes($post->id);
        $data['dislikes'] = $this->post_like_repository->getDisLikes($post->id);

        return $data;
    }




}
