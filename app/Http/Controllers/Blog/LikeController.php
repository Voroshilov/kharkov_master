<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Admin\BaseController;
use App\Repositories\Blog\PostLikeRepository;
use App\Repositories\Blog\PostRepository;
use Illuminate\Http\Request;

class LikeController extends BaseController
{

    /**
     * @var PostRepository
     *
     * @var mixed
     */
    private $post_like_repository;

    public function __construct()
    {
        parent::__construct();

        $this->post_like_repository = app(PostLikeRepository::class);
    }

    public function setLike(Request $request)
    {
        $user_id = \Auth::user()->id;
        $get_like = $this->post_like_repository->getCheckUserLike($request->post_id, $user_id);
        $this->post_like_repository->setLike($request, $user_id, $get_like);

        return back();
    }
}
