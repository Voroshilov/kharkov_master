<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function contacts()
    {
        dd(__METHOD__);
        return view('blog.pages.contacts');
    }

    public function gallery()
    {
        dd(__METHOD__);
        return view('blog.pages.gallery');
    }

    public function aboutUs()
    {
        dd(__METHOD__);
    }
}
