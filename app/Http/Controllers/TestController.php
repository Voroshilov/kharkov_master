<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
//    /**
//     * @param $first first number
//     * @param $second if you want to enter different numbers
//     * @param $iter_count - iterations what we want
//     * @return never
//     */
    public $iter = 0;

    public function testFib()
    {
        return $this->fib(null, 1, 7);
    }
    public function fib($first = 0, $second = 1, $iter_count)
    {
        $result = $first + $second;
        $this->iter++;

        echo "result" . $result . "in iteration". $this->iter . "<br>...";

        if($this->iter == $iter_count){
            return dd($first, $second, $iter_count);
        }else{
            return $this->fib($second, $result, $iter_count);
        }
    }

    public function river()
    {


        return view('river');
    }
}
