<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ManufacturerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:90',//добавить проверку уникальность
            'description' => 'required|min:5|max:400',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Введите название производителя',
            'title.min' => 'Минимальное кол-во символов [:min]',
            'title.max' => 'Максимальное кол-во символов [:max]',
            'description.required' => 'Введите описание производителя',
            'description.min' => 'Минимальное кол-во символов [:min]',
            'description.max' => 'Максимальное кол-во символов [:max]',
        ];
    }
}
