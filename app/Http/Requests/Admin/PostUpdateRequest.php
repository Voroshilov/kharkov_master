<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PostUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|integer|exists:post_categories,id',
            'manufacturer_id' => 'required|integer|exists:manufacturers,id',
            'title' => 'required|min:5|max:90',
//            'text' => 'required|min:10|max:10000',
        ];
    }

    public function messages()
    {
        return [
            'category_id.required' => 'Выберите категорию!',
            'manufacturer_id.required' => 'Выберите производителя!',
            'title.required' => 'Введите заголовок статьи',
            'title.min' => 'Минимальное кол-во символов [:min]',
            'title.max' => 'Максимальное кол-во символов [:max]',
            'text.required' => 'Введите данные, поле не может быть пустым',
            'text.min' => 'Минимальное кол-во символов [:min]',
            'text.max' => 'Максимальное кол-во символов [:max]',
        ];
    }
}
