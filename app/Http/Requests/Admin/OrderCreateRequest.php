<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:40',//добавить проверку уникальность
            'phone' => 'required|min:7',//добавить проверку уникальность
            'address' => 'required|min:5|max:120',//добавить проверку уникальность
            'description' => 'required|min:5|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Введите имя клиента',
            'name.min' => 'Минимальное кол-во символов [:min]',
            'name.max' => 'Максимальное кол-во символов [:max]',
            'phone.required' => 'Введите номер клиента',
            'phone.numeric' => 'Только цыфры',
            'phone.min' => 'Минимальное кол-во символов [:min]',
            'phone.max' => 'Максимальное кол-во символов [:max]',
            'address.required' => 'Введите адрес клиента',
            'address.min' => 'Минимальное кол-во символов [:min]',
            'address.max' => 'Максимальное кол-во символов [:max]',
            'description.required' => 'Введите описание',
            'description.min' => 'Минимальное кол-во символов [:min]',
            'description.max' => 'Максимальное кол-во символов [:max]',
        ];
    }
}
