<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_user_id',
        'second_user_id',
    ];

    public $timestamps = true;
    //Связь со всеми сообщениями
    public function message()
    {
        return $this->hasMany(Message::class, 'chat_id');
    }

//    public function user()
//    {
//        return $this->hasMany(Message::class, 'chat_id');
//    }
}
