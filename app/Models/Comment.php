<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use HasFactory;
    use SoftDeletes;

    //Обьявляем константу ROOT для указания отсутствия родителя у нее
    const ROOT = 0;

    protected $fillable = [
        'user_id',
        'post_id',
        'parent_id',
        'name',
        'last_name',
        'email',
        'comment',
        'evaluation',
        'is_published',
    ];

    public $timestamps = true;

    /**
     * Пользователи которые лайкнули
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Пост который лайкнули
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentComment()
    {
        return $this->belongsTo(Comment::class, 'parent_id', 'id');//???????
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrenComment()
    {
        return $this->hasMany(Comment::class, 'parent_id', 'id');
    }

    //Подумать над этим метобом
//    public function getParentCommentAttribute()
//    {
//        $name = $this->parentComment->name
//            ?? ($this->isRoot() == 0
//                ? 'Главная категория'
//                : '???');
//
//        return $name;
//    }

    public function isRoot()
    {
        return $this->id === Comment::ROOT;
    }
}
