<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailPhoto extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail_id',
        'url',
        'title',
    ];

    public $timestamps = true;
}
