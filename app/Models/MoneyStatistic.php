<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MoneyStatistic extends Model
{
    use HasFactory;

    protected $fillable = [
        'google',
        'yandex',
        'details',
        'unexpected',
        'salary_operator',
        'electronics',
        'advertising',
        'income',
        'description',
    ];

    public $timestamps = false;
}
