<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdviceCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'parent_id',
        'title',
        'slug',
        'is_active',
    ];

    public $timestamps = false;

    //выдает советы по ID сатегории
    public function advice()
    {
        return $this->hasMany(Advice::class, 'category_id');
    }
}
