<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailForMaster extends Model
{
    use HasFactory;

    protected $fillable = [
        'master_id',
        'detail_id',
        'amount',
    ];

    public $timestamps = false;
}
