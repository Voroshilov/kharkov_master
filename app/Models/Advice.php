<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Advice extends Model
{
    use HasFactory;
    use SoftDeletes;

//    protected $table = 'advices';

    protected $fillable = [
        'category_id',
        'creator_id',
        'editor_id',
        'manufacturer_id',
        'type_of_technic_id',
        'title',
        'slug',
        'image',
        'description',
        'keywords',
        'short_text',
        'text',
        'is_active'
    ];

    public $timestamps = true;

//    public function adviceCategory()
//    {
//        return $this->hasOne(AdviceCategory::class, 'id');
//    }
//
//    public function adviceCategoryTest()
//    {
//        return $this->belongsTo(Advice::class, 'category_id');
//    }

    public function AdviceCategory()
    {
        return $this->belongsTo(AdviceCategory::class, 'category_id');
    }

//    protected static function boot()
//    {
//        parent::boot();
//        static::updating (function ($instance)
//        {
//            $instance->slug = strtolower(Str::slug($instance->title,'-'));
//        });
//        static::creating(function ($instance)
//        {
//            $instance->slug = strtolower(Str::slug($instance->title,'-'));
//        });
//    }

}
