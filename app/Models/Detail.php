<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'manufacturer_id',
        'detail_manufacturer_id',
        'selling_price',
        'purchase_price',
        'amount',
        'title',
        'slug',
        'internal_description',
        'public_description',
        'is_new',
        'is_active',
    ];

    public $timestamps = true;
}
