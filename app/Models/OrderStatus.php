<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderStatus
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property int $is_active
 * @method static \Illuminate\Database\Eloquent\Builder|OrderStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderStatus whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderStatus whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderStatus whereTitle($value)
 * @mixin \Eloquent
 */
class OrderStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'is_active',
    ];

    public $timestamps = false;
}
