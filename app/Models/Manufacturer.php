<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'image',
        'description',
        'is_active'
    ];

    public $timestamps = false;

    //Связь со всеми статьми
    public function post()
    {
        return $this->hasMany(Post::class, 'manufacturer_id');
    }
}
