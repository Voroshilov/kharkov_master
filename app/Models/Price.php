<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    //Нужно обдумать логику изменения цен на товар!!!!
    use HasFactory;

    protected $fillable = [
        'type_of_technic_id',
        'title',
        'slug',
        'description',
        'price',
    ];

    public $timestamps = true;
}
