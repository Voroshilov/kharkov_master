<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CodeError extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'manufacturer_id',
        'type_of_technic_id',
        'title',
        'slug',
        'image',
        'description',
        'keywords',
        'short_text',
        'text',
        'is_active',
    ];

    public $timestamps = true;
}
