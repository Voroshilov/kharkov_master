<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Blog
 *
 * @package App\Models
 *
 * @property int                        parent_id
 * @property string                     $title
 * @property string                     $slug
 * @property string                     $is_active
 */

class PostCategory extends Model
{
    use HasFactory;

    //Обьявляем константу ROOT для указания отсутствия родителя у нее
    const ROOT = 0;

    protected $fillable = [
        'parent_id',
        'title',
        'slug',
        'is_active',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function post()
    {
        return $this->hasMany(Post::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentCategory()
    {
        return $this->belongsTo(PostCategory::class, 'parent_id', 'id');//???????
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrenCategory()
    {
        return $this->hasMany(PostCategory::class, 'parent_id', 'id');
    }

    public function getParentTitleAttribute()
    {
        $title = $this->parentCategory->title
            ?? ($this->isRoot() == 0
            ? 'Главная категория'
            : '???');

        return $title;
    }

    public function isRoot()
    {
        return $this->id === PostCategory::ROOT;
    }
}
