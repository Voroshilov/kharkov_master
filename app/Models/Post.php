<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 *
 * @package App\Models
 *
 * @property \App\Models\PostCategory   $category
 * @property \App\Models\User           $creator
 * @property \App\Models\User           $editor
 * @property \App\Models\Manufacturer   $manufacturer
 * @property \App\Models\TypeOfTechnic  $typeOfTechnic
 * @property int                        $category_id
 * @property int                        $creator_id
 * @property int                        $editor_id
 * @property int                        $manufacturer_id
 * @property int                        $typeOfTechnic_id
 * @property string                     $title
 * @property string                     $slug
 * @property string                     $image
 * @property string                     $description
 * @property string                     $keywords
 * @property string                     $short_text
 * @property string                     $text
 * @property boolean                    $is_published
 * @property string                     $published_at
 */

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;

//    protected $table = 'advices';

    protected $fillable = [
        'category_id',
        'creator_id',
        'editor_id',
        'manufacturer_id',
        'type_of_technic_id',
        'title',
        'slug',
        'image',
        'description',
        'keywords',
        'short_text',
        'text',
        'is_published',
        'published_at'
    ];

    public $timestamps = true;

//    public function adviceCategory()
//    {
//        return $this->hasOne(AdviceCategory::class, 'id');
//    }
//
//    public function adviceCategoryTest()
//    {
//        return $this->belongsTo(Advice::class, 'category_id');
//    }

    /**
     * Категории статьи
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(PostCategory::class);
    }

    /**
     * Создателб статьи
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    /**
     * Редактор статьи
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }

    /**
     * Производитель техники, если есть необходимость
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    /**
     * Тип техники которую описывает статья
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeOfTechnic()
    {
        return $this->belongsTo(TypeOfTechnic::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postLike()
    {
        return $this->hasMany(PostLike::class, 'post_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postView()
    {
        return $this->hasMany(PostView::class, 'post_id');
    }
}
