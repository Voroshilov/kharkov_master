<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 *
 * @package App\Models
 *
 * @property \App\Models\User           $master
 * @property \App\Models\User           $creator
 * @property \App\Models\User           $editor
 * @property \App\Models\Manufacturer   $manufacturer
 * @property \App\Models\TypeOfTechnic  $typeOfTechnic
 * @property \App\Models\OrderStatus    $status
 * @property \App\Models\OrderTime      $orderTime
 * @property int                        $master_id
 * @property int                        $creator_id
 * @property int                        $editor_id
 * @property int                        $manufacturer_id
 * @property int                        $type_of_technic_id_id
 * @property int                        $status_id
 * @property int                        $order_time_id
 * @property string                     $name
 * @property string                     $phone
 * @property string                     $email
 * @property string                     $address
 * @property string                     $description
 * @property string                     $machine
 * @property int                        $price
 * @property int                        $expenses
 * @property int                        $percent
 * @property string                     $run_time
 *
 */

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'master_id',
        'creator_id',
        'editor_id',
        'manufacturer_id',
        'type_of_technic_id',
        'status_id',
        'order_time_id',
        'name',
        'phone',
        'email',
        'address',
        'description',
        'machine',
        'price',
        'expenses',
        'percent',
        'run_time',
    ];

    public $timestamps = true;

    /**
     * Мастер выбраный для заявки
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function master()
    {
        return $this->belongsTo(User::class, 'master_id');
    }

    /**
     * Создатель заявки
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    /**
     * Редактор заявки
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function editor()
    {
        return $this->belongsTo(User::class, 'editor_id');
    }

    /**
     * Производитель
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    /**
     * Тип выбраной техники
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeOfTechnic()
    {
        return $this->belongsTo(TypeOfTechnic::class);
    }

    /**
     * Статус заказа
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'status_id');
    }

    /**
     * Время резервирования мастера
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderTime()
    {
        return $this->belongsTo(OrderTime::class, 'order_time_id');
    }
}
