<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Traits\HasRolesAndPermissions;

/**
 * @property \App\Models\UserTelegram           $telegram
 *
 * @property int                        id
 * @property int                        $telegram_id
 * @property string                     $first_name
 * @property string                     $second_name
 * @property string                     $phone
 * @property string                     $address
 * @property string                     $email
 * @property string                     $avatar
 * @property string                     $skills
 * @property string                     $password
 */
class User extends Authenticatable
{
    use Notifiable, HasRolesAndPermissions;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'chat_id',
        'first_name',
        'second_name',
        'phone',
        'address',
        'email',
        'avatar',
        'skills',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $timestamps = true;

    /**
     * Связь с таблице user_telegrams
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function telegram()
    {
        return $this->belongsTo(UserTelegram::class, 'telegram_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postLike()
    {
        return $this->hasMany(PostLike::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postView()
    {
        return $this->hasMany(PostView::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function craetor()
    {
        return $this->hasMany(PostView::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function editor()
    {
        return $this->hasMany(PostView::class, 'user_id');
    }
//    /**
//     * @var mixed
//     */
//    private $roles;
//    /**
//     * @var mixed
//     */
//    private $permissions;
}
