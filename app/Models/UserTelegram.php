<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTelegram extends Model
{
    use HasFactory;

    protected $fillable = [
        'telegram_id',
        'first',
        'lang',
        'is_active',
    ];

    public $timestamps = false;

    //Связь со всеми статьми
    public function user()
    {
        return $this->hasMany(User::class, 'telegram_id');
    }
}
