<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
//use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class CoreRepository.
 */
//abstract class CoreRepository extends BaseRepository
abstract class CoreRepository
{
    /**
     * @var Model
     */

    protected $model;

    /**
     * CoreRepository constructor
     */

    public function __construct()
    {
        $this->model = app($this->getModelClass());
    }

    /**
     * @return mixed
     */
    abstract protected function getModelClass();

    /**
     * @return Model|mixed
     */
    protected function startCondition()
    {
        return clone $this->model;
    }
}
