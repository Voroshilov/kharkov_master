<?php

namespace App\Repositories\Blog;

use App\Models\Post as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

/**
 * Class PostRepository.
 */
class PostRepository extends CoreRepository
{
    /**
     * @var $post_category
     */
    public $post_category;

    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем Post по ID для показа
     *
     * @param int $id
     *
     * @return \App\Models\Post
     */
    public function getShow($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем Post по ID для редактирования
     *
     * @param int $id
     *
     * @return \App\Models\Post
     */
    public function getEdit($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем Post по Slug для Блога
     *
     * @param int $id
     *
     * @return mixed
     */
    public function getOnBySlug($slug)
    {
//        $result = DB::table('posts')
//            ->where('slug', $slug)
//            ->get();
//
//        dd($result);
//
//        return $result;

        return $this->startCondition()->where('slug', $slug)->first();
    }

    /**
     * Получаем все категории с пагинацией, передаем параметром = 10, для получения 10 страниц
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getAll($pages = null)
    {
        $columns = [
            'id',
            'category_id',
            'manufacturer_id',
            'type_of_technic_id',
            'title',
            'slug',
            'image',
            'short_text',
            'is_published',
            'created_at',
        ];

        $result = $this->startCondition()
            ->select($columns)
            ->with(['category:id,title'])
            ->orderBy('created_at', 'DESC')
            ->where('is_published', 1)
            ->paginate($pages);

        return $result;
    }

    /**
     * Получаем все категории с пагинацией, передаем параметром = 10, для получения 10 страниц
     *
     * @param $slug
     * @param $pages
     * @return mixed
     */
    public function getByCategorySQL($slug, $pages = null)
    {
        $columns = [
            'posts.id',
            'posts.category_id',
            'posts.manufacturer_id',
            'posts.type_of_technic_id',
            'posts.title',
            'posts.slug',
            'posts.image',
            'posts.short_text',
            'posts.is_published',
            'posts.created_at',
        ];

        $result = DB::table('posts')
            ->join('post_categories', 'posts.category_id', '=', 'post_categories.id')
            ->where('post_categories.slug', $slug)
            ->where('is_published', 1)
            ->select($columns)
            ->paginate($pages);

        return $result;
    }

    /**
     * Поиск по полю title
     *
     * @param object $data
     *
     * @return mixed
     */
    public function getSearch($data, $pages = null)
    {
        $columns = [
            'id',
            'category_id',
            'creator_id',
            'editor_id',
            'manufacturer_id',
            'type_of_technic_id',
            'title',
            'image',
            'short_text',
            'is_published',
            'created_at',
            'updated_at',
        ];

        $result = $this->startCondition()
            ->where('title', 'LIKE', "%$data->search_title%")
            ->where('category_id', 'LIKE', $data->search_category_id)
            ->select($columns)
            ->with(['creator:id,first_name', 'category:id,title'])
            ->orderBy('created_at', 'DESC')
            ->paginate($pages);

        return $result;
    }

    /**
     * Возвращает ID категории по полю slug
     *
     * @param $slug
     * @return mixed
     */
    protected function getCategoryId($slug)
    {
        $this->post_category = new PostCategoryRepository();
        $category_id = $this->post_category->getCategortyBySlug($slug)->id;

        return $category_id;
    }
}
