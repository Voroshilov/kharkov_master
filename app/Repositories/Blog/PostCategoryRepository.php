<?php

namespace App\Repositories\Blog;

use App\Models\PostCategory as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use phpDocumentor\Reflection\Types\Collection;


/**
 * Class PostCategoryRepository.
 */
class PostCategoryRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем PostCategory по ID
     *
     * @param int $id
     *
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем PostCategory по ID, заменить на вышестоящую или наоборот
     *
     * @param int $id
     *
     * @return Model
     */
    public function getOne($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем PostCategory по ID, заменить на вышестоящую или наоборот
     *
     * @param int $id
     *
     * @return Model
     */
    public function getCategortyBySlug($slug)
    {
        return $this->startCondition()->where('slug', $slug)->first();
    }

    /**
     * Получаем выпадающий список PostCategory только активные
     *
     * @return mixed
     */
    public function getComboBox()
    {
        $columns = ['id', 'title', 'slug', 'parent_id'];

        $result = $this->startCondition()
            ->where('is_active', 1)
            ->select($columns)
            ->with([
                'parentCategory:id,title',
            ])
            ->toBase()
            ->get();

        return $result;
    }

    /**
     * Получаем выпадающий список PostCategory только активные
     *
     * @return mixed
     */
    public function getComboBoxObj()
    {
        $columns = ['id', 'title', 'slug', 'parent_id'];

        $result = $this->startCondition()
            ->where('is_active', 1)
            ->select($columns)
            ->with([
                'parentCategory:id,title',
            ])
//            ->toBase()
            ->get();

        return $result;
    }

    /**
     * Получаем все категории в сокращенном виде
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getForComboBox()
    {
        $columns = ['id', 'title', 'parent_id'];

        $result = $this->startCondition()
            ->select($columns)
            ->where('parent_id', 0)//Берет только корневые категории
            ->where('is_active', 1)
            ->toBase()
            ->get();

        return $result;
    }

    /**
     * Поиск по полю title
     *
     * @param string $data
     *
     * @return mixed
     */
    public function getSearch($data, $pages = null)
    {
        $columns = ['id', 'title', 'parent_id', 'is_active'];

        $result = $this->startCondition()
            ->where('title', 'LIKE', "%$data->search%")
            ->select($columns)
            ->with([
                'parentCategory:id,title',
            ])
            ->orderBy('id', 'DESC')
            ->paginate($pages);

        return $result;
    }
}
