<?php

namespace App\Repositories\Blog;

use App\Repositories\CoreRepository as GuestCoreRepository;
use Illuminate\Database\Eloquent\Model;

//use Your Model

/**
 * Class CoreRepository.
 */
abstract class CoreRepository extends GuestCoreRepository
{
    /**
     * @var Model
     */

    protected $model;

    /**
     * CoreRepository constructor
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    abstract protected function getModelClass();

    /**
     * @return Model|mixed
     */
    protected function startCondition()
    {
        return clone $this->model;
    }
}
