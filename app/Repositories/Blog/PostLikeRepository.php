<?php

namespace App\Repositories\Blog;

use App\Models\PostLike as Model;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use Illuminate\Support\Facades\DB;

//use Your Model

/**
 * Class PostLikeRepository.
 */
class PostLikeRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем Кол-во лайков
     *
     * @param int $post_id
     *
     * @return mixed
     */
    public function getLikes($post_id)
    {
        $data = $this->startCondition()
            ->where('post_id', $post_id)
            ->where('is_like', 1)
            ->where('is_active', 1)
            ->count();

        return $data;
    }

    /**
     * Проверяем есть ли активный лайк у юзера
     *
     * @param int $post_id
     * @param int $user_id
     *
     * @return mixed
     */
    public function getUserLike($post_id, $user_id)
    {
        $data = $this->startCondition()
            ->where('post_id', $post_id)
            ->where('user_id', $user_id)
            ->where('is_active', 1)
//            ->toBase()
            ->first();

        return $data;
    }

    /**
     * Проверяем есть ли запись о лайках у юзера
     *
     * @param int $post_id
     * @param int $user_id
     *
     * @return mixed
     */
    public function getCheckUserLike($post_id, $user_id)
    {
        $data = $this->startCondition()
            ->where('post_id', $post_id)
            ->where('user_id', $user_id)
//            ->toBase()
            ->first();

        return $data;
    }

    /**
     * Получаем Кол-во дизлайков
     *
     * @param int $post_id
     *
     * @return mixed
     */
    public function getDisLikes($post_id)
    {
        $data = $this->startCondition()
            ->where('post_id', $post_id)
            ->where('is_like', 0)
            ->where('is_active', 1)
            ->count();

        return $data;
    }

    /**
     * Проверяем на наличие лайка у пользователя, создаем новый или перезаписываем
     *
     * @param object $request
     * @param int $user_id
     * @param object $get_like
     */
    public function setLike($request, $user_id, $get_like)
    {
        if ($get_like) {
            $this->updateLike($request, $get_like);
        } else {
            $this->newLike($request, $user_id);
        }
    }

    /**
     * Создает новый лайк
     * @param $request
     * @param $user_id
     * @return void
     */
    public function newLike($request, $user_id)
    {
        $data = [
            'user_id' => $user_id,
            'post_id' => $request->post_id,
            'is_like' => $request->is_like,
        ];

        $this->startCondition()->create($data);
    }

    /**
     * Оновляет лайк
     *
     * @param $request
     * @param $get_like
     * @return void
     */
    public function updateLike($request, $get_like)
    {
        $data = $request->all();
        if ($get_like->is_like == $request->is_like) {
            if ($get_like->is_active){
                $data['is_active'] = 0;
            } else {
                $data['is_active'] = 1;
            }
        } else {
            $data['is_active'] = 1;
        }
        if ($request->is_like === null) {
            $data['is_like'] = 0;
        } else {
            $data['is_like'] = 1;
        }

        $get_like->update($data);
    }
}
