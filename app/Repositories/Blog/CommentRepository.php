<?php

namespace App\Repositories\Blog;

use App\Models\Comment as Model;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

//use Your Model

/**
 * Class CommentRepository.
 */
class CommentRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем все комментарии для статьи по ID $post_id
     *
     * @param int $post_id
     *
     * @return mixed
     */
    public function getCommentsToPost($post_id)
    {
        $columns = [
            'id',
            'user_id',
            'post_id',
            'parent_id',
            'name',
            'comment',
            'created_at',

        ];

        $data = $this->startCondition()
            ->where('post_id', $post_id)
            ->select($columns)
            ->orderBy('created_at', 'DESC')
//            ->oredrBy('created_at', 'DESC')
//            ->with([
//                'parentComment:id,name,user_id,comment,created_at',
//            ])
            ->where('is_published', 1)
            ->where('deleted_at', null)
//            ->toBase()
            ->get();

        return $data;
    }
}
