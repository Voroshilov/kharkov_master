<?php

namespace App\Repositories\Admin;

use App\Models\Post as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class PostRepository.
 */
class PostRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем Post по ID для показа
     *
     * @param int $id
     *
     * @return \App\Models\Post
     */
    public function getShow($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем Post по ID для редактирования
     *
     * @param int $id
     *
     * @return \App\Models\Post
     */
    public function getEdit($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем все категории с пагинацией, передаем параметром = 10, для получения 10 страниц
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getAllWithPaginate($pages = null)
    {
        $columns = [
            'id',
            'category_id',
            'creator_id',
            'editor_id',
            'manufacturer_id',
            'type_of_technic_id',
            'title',
            'image',
            'short_text',
            'is_published',
            'created_at',
            'updated_at',
        ];


        $result = $this->startCondition()
            ->select($columns)
//            ->with(['creator:id,first_name', 'category:id,title', 'postView:id'])
            ->with(['creator:id,first_name', 'category:id,title'])
            ->orderBy('created_at', 'DESC')
            ->paginate($pages);

        return $result;
    }

    /**
     * Поиск по полю title
     *
     * @param object $data
     *
     * @return mixed
     */
    public function getSearch($data, $pages = null)
    {
        $columns = [
            'id',
            'category_id',
            'creator_id',
            'editor_id',
            'manufacturer_id',
            'type_of_technic_id',
            'title',
            'image',
            'short_text',
            'is_published',
            'created_at',
            'updated_at',
        ];

        $result = $this->startCondition()
            ->where('title', 'LIKE', "%$data->search_title%")
            ->where('category_id', 'LIKE', $data->search_category_id)
            ->select($columns)
            ->with(['creator:id,first_name', 'category:id,title'])
            ->orderBy('created_at', 'DESC')
            ->paginate($pages);

        return $result;
    }
}
