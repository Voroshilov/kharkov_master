<?php

namespace App\Repositories\Admin;

use App\Models\OrderStatus as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class OrderStatusRepository.
 */
class OrderStatusRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем все всех аактиных производителей производителей для индексной страницы
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getAllWithPaginate($pages = null)
    {
        $columns = [
            'id',
            'title',
            'image',
            'description',
            'is_active',
        ];

        $result = $this->startCondition()
            ->orderBy('id', 'DESC')
            ->select($columns)
            ->toBase()
            ->paginate($pages);

        return $result;
    }

    /**
     * Получаем все всех актиных производителей для выпадающего списка
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getActiveComboBox()
    {
        $columns = [
            'id',
            'title',
            'slug',
        ];

        $result = $this->startCondition()
            ->select($columns)
            ->where('is_active', 1)
            ->toBase()
            ->get();

        return $result;
    }

    /**
     * Получаем Post по ID для показа
     *
     * @param int $id
     *
     * @return \App\Models\Manufacturer
     */
    public function getShow($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем Post по ID для показа
     *
     * @param int $id
     *
     * @return \App\Models\Manufacturer
     */
    public function getOne($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем Post по ID для редактирования
     *
     * @param int $id
     *
     * @return \App\Models\Manufacturer
     */
    public function getEdit($id)
    {
        return $this->startCondition()->find($id);
    }
}
