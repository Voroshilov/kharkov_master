<?php

namespace App\Repositories\Admin;

use App\Models\TypeOfTechnic as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
//use Your Model

/**
 * Class TypeOfTechnicRepository.
 */
class TypeOfTechnicRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем все всех аактиных производителей производителей для индексной страницы
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getAllWithPaginate($pages = null)
    {
        $columns = [
            'id',
            'title',
            'image',
            'description',
            'is_active',
        ];

        $result = $this->startCondition()
            ->select($columns)
            ->orderBy('id', 'DESC')
            ->toBase()
            ->paginate($pages);

        return $result;
    }

    /**
     * Получаем все всех актиных производителей для выпадающего списка
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getActiveComboBox()
    {
        $columns = [
            'id',
            'title',
            'image',
        ];

        $result = $this->startCondition()
            ->select($columns)
            ->where('is_active', 1)
            ->toBase()
            ->get();

        return $result;
    }

    /**
     * Получаем Post по ID для показа
     *
     * @param int $id
     *
//     * @return \App\Models\Manufacturer
     */
    public function getShow($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем Post по ID для показа
     *
     * @param int $id
     *
//     * @return \App\Models\Manufacturer
     */
    public function getOne($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем Post по ID для редактирования
     *
     * @param int $id
     *
//     * @return \App\Models\Manufacturer
     */
    public function getEdit($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Поиск по полю title
     *
     * @param string $data
     *
     * @return mixed
     */
    public function getSearch($data)
    {
        $columns = [
            'id',
            'title',
            'image',
            'description',
            'is_active',
        ];

        $result = $this->startCondition()
            ->where('title', 'LIKE', "%$data->search%")
            ->select($columns)
            ->where('is_active', 1)
            ->toBase()
            ->get();

        return $result;
    }

    /**
     * Загрузка фото
     *
     * @param string $data
     *
     * @return mixed
     */
    public function getImage($data)
    {
        $result = $this->startCondition()
            ->where('title', 'LIKE', "%$data->search%")
            ->select($columns)
            ->where('is_active', 1)
            ->toBase()
            ->get();

        return $result;
    }
}
