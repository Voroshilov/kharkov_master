<?php

namespace App\Repositories\Admin;

use Carbon\Carbon;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\PostView as Model;
use Illuminate\Support\Facades\DB;

//use Your Model

/**
 * Class PostViewRepository.
 */
class PostViewRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем Кол-во просмотров по записи
     *
     * @param int $id
     *
     * @return \App\Models\PostView
     */
    public function getCountViews($post_id)
    {
        return $this->startCondition()->where('post_id', $post_id)->count();
    }

    /**
     * Проверяет наличие просмотра поста по IP адресу и ID для авторизированного юзера
     * и только по IP для не авторизироваванного. Если данных о просмотре нет то сохздает запись
     *
     * @param int $id
     *
     * @return \App\Models\PostView
     */
    public function setViews($post_id, $user_ip)
    {
        $user_id = \Auth::id();

        if($user_id) {//Эту логику нужно обдумать!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            $view = $this->startCondition()
                ->where('post_id', $post_id)
                ->where('user_id', $user_id)
                ->first();
        } else {
            $view = $this->startCondition()
                ->where('post_id', $post_id)
                ->where('user_ip', $user_ip)
                ->first();
        }

        if(!$view) {
            $data = [
                'post_id' => $post_id,
                'user_id' => $user_id,
                'user_ip' => $user_ip,
                'created_at' => Carbon::now(),
            ];

            $this->startCondition()->create($data);
        }

        return $this->startCondition()->where('post_id', $post_id)->count();
    }
}
