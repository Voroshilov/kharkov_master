<?php

namespace App\Repositories\Admin;

use App\Models\Permission as Model;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class PermissionRepository.
 */
class PermissionRepository extends CoreRepository
{
    /**
     * @return string
     * Return the model
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем User по ID
     *
     * @param int $id
     *
     * @return \App\Models\Permission
     */
    public function getOne($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем Все записи
     *
     * @return \App\Models\Permission
     */
    public function getAll()
    {
        return $this->startCondition()->get();
    }
}
