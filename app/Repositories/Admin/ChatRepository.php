<?php

namespace App\Repositories\Admin;

use App\Models\Chat as Model;
use Illuminate\Pagination\LengthAwarePaginator;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class ChatRepository.
 */
class ChatRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем все всех аактиных производителей производителей для индексной страницы
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getUserChat()
    {
        $result = $this->startCondition()
            ->orderBy('id', 'DESC')
            ->get();

        return $result;
    }

    /**
     * Получаем все всех аактиных производителей производителей для индексной страницы
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getById($chat_id)
    {
        $result = $this->startCondition()
            ->find($chat_id);

        return $result;
    }
}
