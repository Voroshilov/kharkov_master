<?php

namespace App\Repositories\Admin;

use App\Models\Role;
use App\Models\User as Model;
use Illuminate\Support\Facades\DB;

//use Your Model

/**
 * Class UserRepository.
 */
class UserRepository extends CoreRepository
{
    public $role;
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Берем пользователей привязаных к роли @role
     * @param $role
     *
     * @return mixed
     */
    public function getUsersByRole($role)
    {
        $result = Role::where('slug', $role)->first()->users;

        return $result;
    }

    /**
     * Выдает все заявки с пагинацией
     * @param $pages
     *
     * @return mixed
     */
    public function getAllWithPaginate($pages = null)
    {
        $columns = [
            'id',
            'name',
            'second_name',
            'phone',
            'address',
            'avatar',
        ];

        $result = $this->startCondition()
            ->select($columns)
            ->orderBy('created_at', 'DESC')
            ->paginate($pages);

        return $result;
    }

    /**
     * Поиск по полю title
     *
     * @param string $data
     *
     * @return mixed
     */
    public function getSearch($data, $pages = null)
    {
        $columns = [
            'id',
            'category_id',
            'creator_id',
            'editor_id',
            'manufacturer_id',
            'type_of_technic_id',
            'title',
            'image',
            'short_text',
            'is_published',
            'created_at',
            'updated_at',
        ];

        $result = $this->startCondition()
            ->where('title', 'LIKE', "%$data->search_title%")
            ->where('category_id', $data->search_category_id)
            ->select($columns)
            ->with(['creator:id,name', 'category:id,title'])
            ->orderBy('created_at', 'DESC')
            ->paginate($pages);

        return $result;
    }

    /**
     * Получаем User по ID
     *
     * @param int $id
     *
     * @return \App\Models\Order
     */
    public function getOne($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Выдает все заявки с пагинацией
     * @param $pages
     *
     * @return mixed
     */
    public function getAllWithPaginateSQL($role, $pages = null)
    {
        $result = DB::table('user_role')
            ->join('users', 'user_role.user_id', '=', 'users.id')
            ->join('roles', 'user_role.role_id', '=', 'roles.id')
            ->whereIn('slug', $role)
            ->select([
                'users.id',
                'users.first_name',
                'users.second_name',
                'users.phone',
                'users.address',
                'users.avatar',
                'roles.slug',
                'roles.title',
            ])
            ->paginate($pages);

        return $result;
    }

    /**
     * Берет работающего оператора, будет учитывать грфик работы оперторов.
     *
     * @return mixed
     */
    public function getWorkOperatorSQL($role)
    {
        $result = DB::table('user_role')
            ->join('users', 'user_role.user_id', '=', 'users.id')
            ->join('roles', 'user_role.role_id', '=', 'roles.id')
            ->whereIn('slug', $role)
            ->select([
                'users.id',
                'users.first_name',
                'users.second_name',
                'users.phone',
                'users.address',
                'users.avatar',
                'roles.slug',
                'roles.title',
            ])
            ->get();

        return $result;
    }


}
