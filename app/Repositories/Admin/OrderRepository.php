<?php

namespace App\Repositories\Admin;

use App\Models\Order as Model;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class OrderRepository.
 */
class OrderRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем Order по ID
     *
     * @param int $id
     *
     * @return \App\Models\Order
     */
    public function getOne($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем все категории с пагинацией, передаем параметром = 10, для получения 10 страниц
     *
     * @param int|null $pages
     *
     * @return mixed
     */
    public function getAllWithPaginate($pages = null)
    {
        $columns = [
            'id',
            'master_id',
            'creator_id',
            'editor_id',
            'manufacturer_id',
            'type_of_technic_id',
            'status_id',
            'name',
            'phone',
            'address',
            'machine',
            'run_time',
            'created_at',
        ];

        $result = $this->startCondition()
            ->select($columns)
            ->with([
                'master:id,first_name',
                'creator:id,first_name',
                'editor:id,first_name',
                'manufacturer:id,title',
                'typeOfTechnic:id,title',
                'status:id,title',
            ])
            ->orderBy('created_at', 'DESC')
            ->paginate($pages);

        return $result;
    }
    /**
     * Поиск
     *
     * @param object $data
     *
     * @return mixed
     */
    public function getSearch($data, $pages = null)
    {
        $columns = [
            'id',
            'master_id',
            'creator_id',
            'editor_id',
            'manufacturer_id',
            'type_of_technic_id',
            'status_id',
            'name',
            'phone',
            'address',
            'machine',
            'run_time',
            'created_at',
        ];

//        dd($data->search_status_id);
        $result = $this->startCondition()
            ->where('phone', 'LIKE', "%$data->search_phone%")
            ->where('address', 'LIKE', "%$data->search_address%")
            ->where('type_of_technic_id', 'LIKE', "%$data->search_tot_id%")
            ->where('status_id', 'LIKE', $data->search_status_id)
//            ->where('master_id', 'LIKE', $data->search_master_id)
            ->select($columns)//???????
            ->with([
                'master:id,first_name',
                'creator:id,first_name',
                'editor:id,first_name',
                'manufacturer:id,title',
                'typeOfTechnic:id,title',
                'status:id,title',
            ])
            ->orderBy('created_at', 'DESC')
            ->paginate($pages);

        return $result;
    }
}
