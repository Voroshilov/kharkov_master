<?php

namespace App\Repositories\Admin;

use App\Models\PostCategory as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use phpDocumentor\Reflection\Types\Collection;


/**
 * Class PostCategoryRepository.
 */
class PostCategoryRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем PostCategory по ID
     *
     * @param int $id
     *
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем PostCategory по ID, заменить на вышестоящую или наоборот
     *
     * @param int $id
     *
     * @return Model
     */
    public function getOne($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем выпадающий список PostCategory
     *
     * @return Collection
     */
    public function getComboBox()
    {
        $this->startCondition()->all();
    }

    /**
     * Получаем выпадающий список PostCategory только активные
     *
     * @return mixed
     */
    public function getActiveComboBox()
    {
        $columns = ['id', 'title', 'parent_id'];

        $result = $this->startCondition()
            ->where('is_active', 1)
            ->select($columns)
            ->toBase()
            ->get();

        return $result;
    }

    /**
     * Получаем все категории с пагинацией, передаем параметром = 10, для получения 10 страниц
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getAllWithPaginate($pages = null)
    {
        $columns = ['id', 'title', 'parent_id', 'is_active'];

        $result = $this->startCondition()
            ->select($columns)
            ->with([
                'parentCategory:id,title',
            ])
            ->orderBy('id', 'DESC')
            ->paginate($pages);

        return $result;
    }

    /**
     * Получаем все категории в сокращенном виде
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getForComboBox()
    {
        $columns = ['id', 'title', 'parent_id'];

        $result = $this->startCondition()
            ->select($columns)
            ->where('parent_id', 0)//Берет только корневые категории
            ->where('is_active', 1)
            ->toBase()
            ->get();

        return $result;
    }

    /**
     * Поиск по полю title
     *
     * @param string $data
     *
     * @return mixed
     */
    public function getSearch($data, $pages = null)
    {
        $columns = ['id', 'title', 'parent_id', 'is_active'];

        $result = $this->startCondition()
            ->where('title', 'LIKE', "%$data->search%")
            ->select($columns)
            ->with([
                'parentCategory:id,title',
            ])
            ->orderBy('id', 'DESC')
            ->paginate($pages);

        return $result;
    }
}
