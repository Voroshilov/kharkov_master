<?php

namespace App\Repositories\Admin;

//use Your Model
use App\Models\Advice as Model;
//use Illuminate\Database\Eloquent\Model;

/**
 * Class AdviceRepository.
 */
class AdviceRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }
}
