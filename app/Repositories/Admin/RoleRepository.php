<?php

namespace App\Repositories\Admin;

use App\Models\Role as Model;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class RoleRepository.
 */
class RoleRepository extends CoreRepository
{
    /**
     * @return string
     * Return the model
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем Role по ID
     *
     * @param int $id
     *
     * @return \App\Models\Role
     */
    public function getOne($id)
    {
        return $this->startCondition()->find($id);
    }

    /**
     * Получаем Все записи
     *
     * @return \App\Models\Role
     */
    public function getAll()
    {
        return $this->startCondition()->get();
    }
}
