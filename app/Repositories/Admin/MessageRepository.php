<?php

namespace App\Repositories\Admin;

use App\Models\Message as Model;
use Illuminate\Pagination\LengthAwarePaginator;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class MessageRepository.
 */
class MessageRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем все сообщения чата по ID
     *
     * @param int|null $pages
     *
     * @return LengthAwarePaginator
     */
    public function getMessageFromChat($chat_id)
    {
        $result = $this->startCondition()
            ->orderBy('id', 'DESC')
            ->where('chat_id', $chat_id)
            ->get();

        return $result;
    }
}
