<?php

namespace App\Repositories\Admin;

use App\Models\OrderTime as Model;

/**
 * Class OrderTimeRepository.
 */
class OrderTimeRepository extends CoreRepository
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * Получаем все всех актиных производителей для выпадающего списка
     *
     * @param int|null $pages
     *
     * @return mixed
     */
    public function getActiveComboBox()
    {
        $columns = [
            'id',
            'title',
            'slug',
        ];

        $result = $this->startCondition()
            ->select($columns)
            ->where('is_active', 1)
            ->toBase()
            ->get();

        return $result;
    }
}
