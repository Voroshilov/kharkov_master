<?php

namespace App\Traits;

use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

trait HasTelegram
{
    /**
     * Заявка для матера при создании
     *
     * @param $data
     * @param $telegram_hash
     * @return \Illuminate\Http\RedirectResponse|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function notifyOrderToMaster($data, $telegram_hash)
    {
        $chat_id = $data->master->telegram->chat_id;

        $message = $this->createMessageForMaster($data);

        $client = new Client();

        try {
            $client->post('https://api.telegram.org/bot' . $telegram_hash .'/sendMessage', [
                RequestOptions::JSON => [
                    'chat_id' => $chat_id,
                    'text' => $message,
                ]
            ]);

        } catch (\Exception $e) {
            return redirect()
                ->route('admin.orders.index')
                ->with(['success' => 'Что то пошло не так! Обратитесь к администратору!']);
        }
    }
    /**
     * Запрос на заявку в идеале для опертора, сейчас для Антона
     *
     * @param $data
     * @param $telegram_hash
     * @return \Illuminate\Http\RedirectResponse|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function notifyOrderToOperator($data, $telegram_hash)
    {
        $user_first = User::where('id', 1)->first();
        $user = User::where('id', 2)->first();

        $message = $this->createMessageFromClient($data);

        $client = new Client();

        try {
            $client->post('https://api.telegram.org/bot' . $telegram_hash .'/sendMessage', [
                RequestOptions::JSON => [
                    'chat_id' => $user_first->telegram->chat_id,
                    'text' => $message,
                ]
            ]);
            $client->post('https://api.telegram.org/bot' . $telegram_hash .'/sendMessage', [
                RequestOptions::JSON => [
                    'chat_id' => $user->telegram->chat_id,
                    'text' => $message,
                ]
            ]);

        } catch (\Exception $e) {
            return redirect()
                ->route('index')
                ->with(['success' => 'Что то пошло не так! Обратитесь к администратору!']);
        }
    }

    /**
     * Заявка от клиентов на сайте
     *
     * @param $data
     * @param $telegram_hash
     * @return \Illuminate\Http\RedirectResponse|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function notifyFromClient($data, $telegram_hash)
    {
        $operator = $this->user_repository->getWorkOperatorSQL('operator');
        $chat_id = $operator->telegram->chat_id;
        $message = $this->createMessageFromClient($data);

        $client = new Client();

        try {
            $client->post('https://api.telegram.org/bot' . $telegram_hash .'/sendMessage', [
                RequestOptions::JSON => [
                    'chat_id' => $chat_id,
                    'text' => $message,
                ]
            ]);

        } catch (\Exception $e) {
            return redirect()
                ->route('welcome')//??????????????
                ->with(['success' => 'Что то пошло не так! Обратитесь к администратору!']);
        }
    }
}
