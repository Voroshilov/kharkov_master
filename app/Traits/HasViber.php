<?php


namespace App\Traits;

use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

trait HasViber
{
    private $url_api = "https://chatapi.viber.com/pa/";

    private $token = "";

    public function message_post
    (
        $from,          // ID администратора Public Account.
        array $sender,  // Данные отправителя.
        $text           // Текст.
    )
    {
        $data['from']   = $from;
        $data['sender'] = $sender;
        $data['type']   = 'text';
        $data['text']   = $text;
        return $this->call_api('post', $data);
    }

    private function call_api($method, $data)
    {
        $url = $this->url_api.$method;

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\nX-Viber-Auth-Token: ".$this->token."\r\n",
                'method'  => 'POST',
                'content' => json_encode($data)
            )
        );
        $context  = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        return json_decode($response);
    }
//    $Viber = new Viber();
//    $Viber->message_post(
//    '01234567890A=',
//    [
//    'name' => 'Admin', // Имя отправителя. Максимум символов 28.
//    'avatar' => 'http://avatar.example.com' // Ссылка на аватарку. Максимальный размер 100кб.
//    ],
//    'Test'
//    );




    /**
     * Заявка для матера при создании
     *
     * @param $data
     * @param $telegram_hash
     * @return \Illuminate\Http\RedirectResponse|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function notifyOrderToMaster($data, $telegram_hash)
    {
        $chat_id = $data->master->telegram->chat_id;
//        $message = $this->createMessageForMaster($data);

        $client = new Client();

        try {
            $client->post('https://api.telegram.org/bot' . $telegram_hash . '/sendMessage', [
                RequestOptions::JSON => [
                    'chat_id' => $chat_id,
                    'text' => '',
                ]
            ]);

        } catch (\Exception $e) {
            return redirect()
                ->route('admin.orders.index')
                ->with(['success' => 'Что то пошло не так! Обратитесь к администратору!']);
        }
    }
}
