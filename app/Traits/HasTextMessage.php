<?php

namespace App\Traits;

use Carbon\Carbon;
use Nette\Utils\DateTime;

trait HasTextMessage
{
    /**
     * Формирует сообщение для отправки мастерам, при создании заявки
     * @param $data
     * @return string
     * @throws \Exception
     */
    protected function createMessageForMaster($data)
    {
        $options = '';

        if (!empty($data->manufacturer_id)) {
            $options .= 'Производитель - ' . $data->manufacturer->title . "\n";
        }
        if (!empty($data->machine)) {
            $options .= 'Модель - ' . $data->machine . "\n";
        }
        if (!empty($data->type_of_technic_id)) {
            $options .= 'Тип - ' . $data->typeOfTechnic->title . "\n";
        }
        if (!empty($data->order_time_id)) {
            $options .= 'Сложность - ' . $data->orderTime->title . "\n";
        }
        if (!empty($data->status)) {
            $options .= 'Статус - ' . $data->status->title . "\n";
        }

        $message = '#' . $data->id . "\n" .
            'Имя - ' . $data->name . "\n" .
            'Время - ' . date_format(new DateTime($data->run_time), 'H:m d/m/y') . "\n" .
            'Номер - ' . $data->phone . "\n" .
            'Адресс - ' . $data->address . "\n" .
            $options .
            'Описание - ' . $data->description . "\n". "\n";

//            'Если время слишком мало для прибытия на заявку, уточните у оператора.';

        return $message;
    }

    protected function createMessageFromClient($data)
    {

        $message = '#' . rand(100, 999999) . "\n" .
            'Имя - ' . $data['name'] . "\n" .
            'Номер - ' . $data['phone'] . "\n" .

            //Тут доп информаци ясобираемая с разных форм, возможно моедль машинок и авторизированного клиента

            'Время - ' . Carbon::now() . "\n";

        return $message;
    }

}
