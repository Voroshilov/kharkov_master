<?php

namespace App\Providers;

use App\Models\Manufacturer;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\Post;
use App\Models\PostCategory;
use App\Models\TypeOfTechnic;
use App\Models\User;
use App\Models\UserTelegram;
use App\Observers\Admin\ManufacturerObserver;
use App\Observers\Admin\OrderObserver;
use App\Observers\Admin\OrderStatusObserver;
use App\Observers\Admin\PostCategoryObserver;
use App\Observers\Admin\PostObserver;
use App\Observers\Admin\TypeOfTechnicObserver;
use App\Observers\Admin\UserObserver;
use App\Observers\Admin\UserTelegramObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
//        \Schema::defaultStringLength(191);

        Post::observe(PostObserver::class);
        Order::observe(OrderObserver::class);
        PostCategory::observe(PostCategoryObserver::class);
        Manufacturer::observe(ManufacturerObserver::class);
        TypeOfTechnic::observe(TypeOfTechnicObserver::class);
        UserTelegram::observe(UserTelegramObserver::class);
        User::observe(UserObserver::class);
//        OrderStatus::observe(OrderStatusObserver::class); //????
    }
}
