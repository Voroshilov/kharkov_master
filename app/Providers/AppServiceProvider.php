<?php

namespace App\Providers;

use App\Repositories\Blog\PostCategoryRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->composerCategory();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function composerCategory()
    {
        view()->composer('blog.layouts.header', function ($view) {
            $view->with('categories', $this->getCategory());
        });
        view()->composer('blog.layouts.footer', function ($view) {
            $view->with('categories', $this->getCategory());
        });
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        $postCategoryRepository = new PostCategoryRepository;
        $data = $postCategoryRepository->getComboBoxObj();

//        shuffle($machines);

        return $data;
    }
}
