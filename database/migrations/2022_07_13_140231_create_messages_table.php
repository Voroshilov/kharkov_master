<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('chat_id')->unsigned();
            $table->unsignedBigInteger('from_user_id')->unsigned();
            $table->unsignedBigInteger('to_user_id')->unsigned();
            $table->string('sender_ip', 20);
            $table->boolean('is_read')->default(0);
            $table->boolean('is_visible_first_user')->default(1);
            $table->boolean('is_visible_second_user')->default(1);
            $table->string('message');
            $table->timestamps();

            $table->foreign('chat_id')
                ->references('id')
                ->on('chats');

            $table->foreign('from_user_id')
                ->references('id')
                ->on('users');

            $table->foreign('to_user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
