<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('creator_id')->unsigned();
            $table->unsignedBigInteger('editor_id')->unsigned();
            $table->unsignedBigInteger('manufacturer_id')->unsigned()->nullable();
            $table->unsignedBigInteger('category_id')->unsigned();
            $table->string('title', 90);
            $table->string('slug', 90)->unique();
            $table->string('image', 120)->nullable();
            $table->string('description', 150);
            $table->string('keywords', 120);
            $table->string('short_text');
            $table->text('text');
            $table->boolean('is_active');
            $table->timestamps();
            $table->softDeletes();//указывает время удаления

            $table->foreign('creator_id')
                ->references('id')
                ->on('users');

            $table->foreign('editor_id')
                ->references('id')
                ->on('users');

            $table->foreign('manufacturer_id')
                ->references('id')
                ->on('manufacturers');

            $table->foreign('category_id')
                ->references('id')
                ->on('advice_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advices');
    }
}
