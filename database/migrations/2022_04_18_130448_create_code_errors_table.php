<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodeErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_errors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('manufacturer_id')->unsigned();
            $table->unsignedBigInteger('type_of_technic_id')->unsigned();
            $table->string('title', 90);
            $table->string('slug', 90)->unique();
            $table->string('image', 120);
            $table->string('description', 150);
            $table->string('keywords', 120);
            $table->string('short_text', 500);
            $table->text('text');
            $table->boolean('is_active');
            $table->timestamps();
            $table->softDeletes();//указывает время удаления

            $table->foreign('manufacturer_id')
                ->references('id')
                ->on('manufacturers');

            $table->foreign('type_of_technic_id')
                ->references('id')
                ->on('type_of_technics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_errors');
    }
}
