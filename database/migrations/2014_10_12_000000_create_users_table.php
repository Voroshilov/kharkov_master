<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('telegram_id')->unsigned()->nullable();
            $table->string('first_name', 20)->nullable();
            $table->string('second_name', 25)->nullable();
            $table->string('phone', 15);
            $table->string('address', 90);
            $table->string('email')->unique();
            $table->string('avatar', 70)->nullable();
            $table->string('skills')->nullable();//Возможно будет json формат данных
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('telegram_id')
                ->references('id')
                ->on('user_telegrams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
