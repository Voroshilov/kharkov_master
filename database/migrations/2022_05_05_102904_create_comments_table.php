<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->unsigned()->nullable();
            $table->unsignedBigInteger('post_id')->unsigned()->nullable();
            $table->integer('parent_id')->default(0);
            $table->string('name', 30);
            $table->string('last_name', 30)->nullable();
            $table->string('email', 40)->nullable();
            $table->string('comment', 500);
            $table->integer('evaluation')->nullable();//Оченка статьи, но лучше ее сделать отдельной
            $table->boolean('is_published')->default(0);
            $table->timestamps();
            $table->softDeletes();//указывает время удаления

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('post_id')
                ->references('id')
                ->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
