<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailForMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_for_masters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('master_id')->unsigned();
            $table->unsignedBigInteger('detail_id')->unsigned();
            $table->integer('amount');

            $table->foreign('master_id')
                ->references('id')
                ->on('users');

            $table->foreign('detail_id')
                ->references('id')
                ->on('details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_for_masters');
    }
}
