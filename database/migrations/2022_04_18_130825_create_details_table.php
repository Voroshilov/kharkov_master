<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->unsigned();
            $table->unsignedBigInteger('manufacturer_id')->unsigned()->nullable();
            $table->unsignedBigInteger('detail_manufacturer_id')->unsigned();
            $table->integer('selling_price');
            $table->integer('purchase_price');
            $table->integer('amount');
            $table->string('title', 90);
            $table->string('slug', 90);
            $table->string('internal_description', 200)->nullable();
            $table->string('public_description', 300)->nullable();
            $table->boolean('is_new');
            $table->boolean('is_active');
            $table->timestamps();


            $table->foreign('category_id')
                ->references('id')
                ->on('detail_categories');

            $table->foreign('manufacturer_id')
                ->references('id')
                ->on('manufacturers');

            $table->foreign('detail_manufacturer_id')
                ->references('id')
                ->on('detail_manufacturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
