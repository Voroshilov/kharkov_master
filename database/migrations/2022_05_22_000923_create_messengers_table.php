<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messengers', function (Blueprint $table) {
            $table->id();
            $table->string('title', 120)->nullable();
            $table->string('slug', 120)->nullable();
            $table->string('messenger_name', 120)->nullable();//Название мессенджера
            $table->string('hash', 60);//секретный код
            $table->string('user', 120)->nullable();//Юзер на чье имя загистрирована рассылка
            $table->string('pass', 120)->nullable();//пароль для доступа к API мессенджера
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messengers');
    }
}
