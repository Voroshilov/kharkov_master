<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoneyStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('money_statistics', function (Blueprint $table) {
            $table->id();
            $table->integer('google');
            $table->integer('yandex');
            $table->integer('details');
            $table->integer('unexpected');
            $table->integer('salary_operator');
            $table->integer('electronics');
            $table->integer('advertising');
            $table->integer('income');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('money_statistics');
    }
}
