<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('type_of_technic_id')->unsigned();
            $table->string('title', 90);
            $table->string('slug', 90);
            $table->string('description', 300);
            $table->float('price', 8, 2);
            $table->timestamps();

            $table->foreign('type_of_technic_id')
                ->references('id')
                ->on('type_of_technics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
