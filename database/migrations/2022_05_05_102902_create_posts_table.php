<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->unsigned();
            $table->unsignedBigInteger('creator_id')->unsigned();
            $table->unsignedBigInteger('editor_id')->unsigned();
            $table->unsignedBigInteger('manufacturer_id')->unsigned()->nullable();
            $table->unsignedBigInteger('type_of_technic_id')->unsigned()->nullable();
            $table->string('title', 90);
            $table->string('slug', 90)->unique();
            $table->string('image', 120)->nullable();
            $table->string('description', 150)->nullable();
            $table->string('keywords', 120)->nullable();
            $table->string('short_text');
            $table->text('text');
            $table->boolean('is_published');
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
            $table->softDeletes();//указывает время удаления

            $table->foreign('category_id')
                ->references('id')
                ->on('post_categories');

            $table->foreign('creator_id')
                ->references('id')
                ->on('users');

            $table->foreign('editor_id')
                ->references('id')
                ->on('users');

            $table->foreign('manufacturer_id')
                ->references('id')
                ->on('manufacturers');

            $table->foreign('type_of_technic_id')
                ->references('id')
                ->on('type_of_technics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
