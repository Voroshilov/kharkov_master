<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('master_id')->unsigned()->nullable();
            $table->unsignedBigInteger('creator_id')->unsigned()->nullable();
            $table->unsignedBigInteger('editor_id')->unsigned()->nullable();
            $table->unsignedBigInteger('manufacturer_id')->unsigned()->nullable();
            $table->unsignedBigInteger('type_of_technic_id')->unsigned()->nullable();
            $table->unsignedBigInteger('status_id')->unsigned();
            $table->unsignedBigInteger('order_time_id')->unsigned()->nullable();
            $table->string('name', 40)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('email', 60)->nullable();
            $table->string('address', 120)->nullable();
            $table->string('description', 1000)->nullable();
            $table->string('machine', 255)->nullable();
            $table->integer('price')->default(0);
            $table->integer('expenses')->default(0);
            $table->integer('percent')->default(0);
//            $table->enum('status', array('empty', 'is_ready', 'done', 'canceled'))->default('empty');
//            $table->enum('type', array('machine', 'dishwasher', 'fridge', 'boiler'))->default('machine');
            $table->dateTime('run_time');
            $table->timestamps();

            $table->foreign('master_id')
                ->references('id')
                ->on('users');

            $table->foreign('creator_id')
                ->references('id')
                ->on('users');

            $table->foreign('editor_id')
                ->references('id')
                ->on('users');

            $table->foreign('manufacturer_id')
                ->references('id')
                ->on('manufacturers');

            $table->foreign('type_of_technic_id')
                ->references('id')
                ->on('type_of_technics');

            $table->foreign('status_id')
                ->references('id')
                ->on('order_statuses');

            $table->foreign('order_time_id')
                ->references('id')
                ->on('order_times');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
