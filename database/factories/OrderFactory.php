<?php

namespace Database\Factories;

use App\Repositories\Admin\ManufacturerRepository;
use App\Repositories\Admin\OrderStatusRepository;
use App\Repositories\Admin\OrderTimeRepository;
use App\Repositories\Admin\UserRepository;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Repositories\Admin\TypeOfTechnicRepository;

class OrderFactory extends Factory
{
    /**
     * @var $type_if_technic
     */
    public $type_if_technic;

    /**
     * @var $manufacturer
     */
    public $manufacturer;

    /**
     * @var $status
     */
    public $status;

    /**
     * @var $order_time
     */
    public $order_time;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $this->type_if_technic = app(TypeOfTechnicRepository::class);
        $this->manufacturer = app(ManufacturerRepository::class);
        $this->status = app(OrderStatusRepository::class);
        $this->order_time = app(OrderTimeRepository::class);

        $order_time = $this->order_time->getActiveComboBox();
        $status = $this->status->getActiveComboBox();
//        dd($this->getRandId($status));
        $type_if_technic = $this->type_if_technic->getActiveComboBox();
        $manufacturer = $this->manufacturer->getActiveComboBox();

        $users_by_roles = app(UserRepository::class);
        $masters = $users_by_roles->getUsersByRole('master');
        $operators = $users_by_roles->getUsersByRole('operator');


        $data = [
            'creator_id' => $this->getRandId($operators),
            'editor_id' => $this->getRandId($operators),
            'master_id' => $this->getRandId($masters),
            'type_of_technic_id' => $this->getRandId($type_if_technic),
            'manufacturer_id' => $this->getRandId($manufacturer),
            'status_id' => $this->getRandId($status),// Count IDs starting from 0
            'order_time_id' => $this->getRandId($order_time),
            'name' => $this->faker->name(),
            'phone' => preg_replace("/[^0-9]/", '', $this->faker->phoneNumber()),
            'email' => $this->faker->unique()->safeEmail(),
            'address' => $this->faker->address(),
            'description' => $this->faker->text(100),
            'machine' => $manufacturer[rand(1, $manufacturer->count()) -1]->title,
            'price' => rand(100, 1000),
            'expenses' => rand(100, 700),
            'percent' => rand(50, 200),
            'run_time' => now(),
        ];
//        dd($data);

        return $data;
    }

    /**
     * Возвращает случайный ID
     * @param $data
     * @return mixed
     */
    private function getRandId($data)
    {
        return $data[rand(1, $data->count()) - 1]->id;
    }
}
