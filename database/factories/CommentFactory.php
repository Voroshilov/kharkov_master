<?php

namespace Database\Factories;

use App\Repositories\Admin\ManufacturerRepository;
use App\Repositories\Admin\OrderStatusRepository;
use App\Repositories\Admin\OrderTimeRepository;
use App\Repositories\Admin\UserRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Repositories\Admin\TypeOfTechnicRepository;

class CommentFactory extends Factory
{
    /**
     * @var $type_if_technic
     */
    public $type_if_technic;

    /**
     * @var $manufacturer
     */
    public $manufacturer;

    /**
     * @var $status
     */
    public $status;

    /**
     * @var $order_time
     */
    public $order_time;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user_id = rand(1, rand(1, 10));
        if ($user_id == 1)
            $user_id = null;

        $post_id = rand(1, 10);
        $parent_id = 0;
        $is_published =  rand(rand(rand(0, 1), 1), 1);
        $deleted_at =  rand(0, rand(0, rand(0, rand(0, 1))));
        if ($deleted_at == 1) {
            $deleted_at = Carbon::now();
        } else {
            $deleted_at = null;
        }

        $data = [
            'user_id' => $user_id,
            'post_id' => $post_id,
            'parent_id' => $parent_id,
            'name' => $this->faker->name(),
            'last_name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'comment' => $this->faker->text(100),
            'is_published' => $is_published,
            'deleted_at' => $deleted_at,
        ];

        return $data;
    }
}
