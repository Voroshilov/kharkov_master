<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_categories')->delete();
        $data = array(
            array(
                'parent_id' => 0,
                'title' => 'Советы',
                'slug' => 'advices',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Коды ошибок',
                'slug' => 'code-errors',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Новости',
                'slug' => 'news',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Наши работы',
                'slug' => 'our-works',
                'is_active' => 1,
            ),
        );
        DB::table('post_categories')->insert($data);


        $general_categories = DB::table('post_categories')->where('parent_id', 0)->get();

        $child_categories = array(
            array(
                'parent_id' => $general_categories[0]->id,
                'title' => 'Советы по уходу за техникой',
                'slug' => Str::slug('Советы по уходу за техникой'),
                'is_active' => 1,
            ),
            array(
                'parent_id' => $general_categories[0]->id,
                'title' => 'Советы по покупке',
                'slug' => Str::slug('Советы по покупке'),
                'is_active' => 1,
            ),
            array(
                'parent_id' => $general_categories[2]->id,
                'title' => 'Новые можели',
                'slug' => Str::slug('Новые можели'),
                'is_active' => 1,
            ),
            array(
                'parent_id' => $general_categories[2]->id,
                'title' => 'Иновации',
                'slug' => Str::slug('Иновации'),
                'is_active' => 1,
            ),
        );

        DB::table('post_categories')->insert($child_categories);
    }
}
