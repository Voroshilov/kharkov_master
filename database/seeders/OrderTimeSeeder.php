<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class OrderTimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_times')->delete();
        $users = array(
            array(
                'title' => 'Диагностика',
                'slug' => 'diagnostics',
                'description' => 'Дигностика длительностью в пол часа',
                'value_per_second' => 1800,
                'is_active' => 1,
            ),
            array(
                'title' => 'Мелкий ремонт',
                'slug' => 'minor-repairs',
                'description' => 'Мелкий ремонт длительностью в 1 час часа',
                'value_per_second' => 3600,
                'is_active' => 1,
            ),
            array(
                'title' => 'Средний ремонт',
                'slug' => 'average-repair',
                'description' => 'Средний ремонт длительностью в 1,5 час часа',
                'value_per_second' => 5400,
                'is_active' => 1,
            ),
            array(
                'title' => 'Ремонт выше среднего',
                'slug' => 'above-average-repair',
                'description' => 'Ремонт выше среднего длительностью в 2 час часа',
                'value_per_second' => 7200,
                'is_active' => 1,
            ),
            array(
                'title' => 'Сложный ремонт',
                'slug' => 'complex-repair',
                'description' => 'Сложный ремонт длительностью в 3 час часа',
                'value_per_second' => 10800,
                'is_active' => 1,
            ),
            array(
                'title' => 'Ремонт на весь день',
                'slug' => 'repair-all-day',
                'description' => 'Ремонт на весь день длительностью в 8 час часа',
                'value_per_second' => 28800,
                'is_active' => 1,
            ),
        );
        DB::table('order_times')->insert($users);
    }
}
