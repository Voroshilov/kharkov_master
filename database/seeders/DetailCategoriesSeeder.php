<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class DetailCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('detail_categories')->delete();
        $users = array(
            array(
                'parent_id' => 0,
                'title' => 'Помпы',
                'slug' => 'pumps',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Тэн водяной',
                'slug' => 'water_heating_element',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Карбоновые щетки',
                'slug' => 'carbon_brushes',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Подшипники',
                'slug' => 'bearings',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Люки',
                'slug' => 'hatches',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Сальники',
                'slug' => 'oil_seals',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Амортизаторы',
                'slug' => 'shock_absorbers',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Петля люка',
                'slug' => 'hatch_hinge',
                'is_active' => 1,
            ),

            array(
                'parent_id' => 1,
                'title' => 'Помпы 3 самореза',
                'slug' => 'pump_3_samorez',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 1,
                'title' => 'Помпы 3 защелки',
                'slug' => 'pump_3_zaschelka',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 2,
                'title' => 'Тэн 170mm',
                'slug' => 'ten_170mm',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 2,
                'title' => 'Тэн 210mm',
                'slug' => 'ten_210mm',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 3,
                'title' => 'Щетки 12,5мм',
                'slug' => 'carbon_brushes_12.5mm',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 3,
                'title' => 'Щетки 13,5мм',
                'slug' => 'carbon_brushes_13.5mm',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 4,
                'title' => 'Подшипники 201',
                'slug' => 'bearings_201',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 4,
                'title' => 'Подшипники 202',
                'slug' => 'bearings_202',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 4,
                'title' => 'Подшипники 203',
                'slug' => 'bearings_203',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 4,
                'title' => 'Подшипники 204',
                'slug' => 'bearings_204',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 4,
                'title' => 'Подшипники 205',
                'slug' => 'bearings_205',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 4,
                'title' => 'Подшипники 206',
                'slug' => 'bearings_206',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 4,
                'title' => 'Подшипники 207',
                'slug' => 'bearings_207',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 6,
                'title' => 'Сальник',
                'slug' => 'oil_seals_30_52_10',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 6,
                'title' => 'Сальник',
                'slug' => 'oil_seals_35_62_10',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 7,
                'title' => 'Амортизаторы круглые',
                'slug' => 'shock_absorbers_secol',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 7,
                'title' => 'Амортизаторы железные',
                'slug' => 'shock_absorbers_still',
                'is_active' => 1,
            ),

        );
        DB::table('detail_categories')->insert($users);
    }
}
