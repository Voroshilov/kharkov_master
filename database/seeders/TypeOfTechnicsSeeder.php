<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class TypeOfTechnicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_of_technics')->delete();
        $users = array(
            array(
                'title' => 'Стиральная машина',
                'slug' => 'washing machine',
                'description' => 'Стиральная машина',
                'is_active' => 1,
            ),
            array(
                'title' => 'Сушильная машина',
                'slug' => 'dry machine',
                'description' => 'Сушильная машина',
                'is_active' => 1,
            ),
            array(
                'title' => 'Холодильник',
                'slug' => 'frig',
                'description' => 'Холодильник',
                'is_active' => 1,
            ),
            array(
                'title' => 'Кондиционер',
                'slug' => 'ac',
                'description' => 'Кондиционер',
                'is_active' => 1,
            ),
            array(
                'title' => 'Микроволновка',
                'slug' => 'micro-wave',
                'description' => 'Микроволновка',
                'is_active' => 1,
            ),
            array(
                'title' => 'Блендер',
                'slug' => 'blend',
                'description' => 'Блендер',
                'is_active' => 1,
            ),
            array(
                'title' => 'Пылесос',
                'slug' => 'vacuum-cleaner',
                'description' => 'Пылесос',
                'is_active' => 1,
            ),
            array(
                'title' => 'Посудомойка',
                'slug' => 'dishwasher',
                'description' => 'Посудомойка',
                'is_active' => 1,
            ),
        );
        DB::table('type_of_technics')->insert($users);
    }
}
