<?php

namespace Database\Seeders;

use DB;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();
        $users = array(
            array(
                'title' => 'Домашняя страница',
                'slug' => 'home',
            ),
            array(
                'title' => 'Домашняя страница Админпанели',
                'slug' => 'admin-home',
            ),
            array(
                'title' => 'Заявки',
                'slug' => 'orders',
            ),
            array(
                'title' => 'Статусы заявок',
                'slug' => 'orders-statuses',
            ),
            array(
                'title' => 'Статистика дохода',
                'slug' => 'income-statistics',
            ),
            array(
                'title' => 'Доход-расход',
                'slug' => 'income-expenses',
            ),
            array(
                'title' => 'Статьи',
                'slug' => 'admin-posts',
            ),
            array(
                'title' => 'Пользователи',
                'slug' => 'users',
            ),
            array(
                'title' => 'Запчасти',
                'slug' => 'details',
            ),
            array(
                'title' => 'Категории запчастей',
                'slug' => 'details-categories',
            ),
            array(
                'title' => 'Производители запчастей',
                'slug' => 'details-manufacturers',
            ),
            array(
                'title' => 'Запчасти для мастеров',
                'slug' => 'details-for-masters',
            ),
            array(
                'title' => 'Фото запчастей',
                'slug' => 'details-photo',
            ),
            array(
                'title' => 'Коментарии',
                'slug' => 'comments',
            ),
            array(
                'title' => 'Производители',
                'slug' => 'manufacturers',
            ),
            array(
                'title' => 'Модели техники',
                'slug' => 'type-of-technics',
            ),
            array(
                'title' => 'Цены',
                'slug' => 'prices',
            ),
        );
        DB::table('permissions')->insert($users);
    }
}
