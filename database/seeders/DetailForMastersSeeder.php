<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class DetailForMastersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('detail_for_masters')->delete();
        $users = array(
            array(
                'master_id' => 1,
                'detail_id' => 1,
                'amount' => 1,
            ),
            array(
                'master_id' => 1,
                'detail_id' => 2,
                'amount' => 2,
            ),
            array(
                'master_id' => 1,
                'detail_id' => 3,
                'amount' => 2,
            ),
            array(
                'master_id' => 1,
                'detail_id' => 4,
                'amount' => 2,
            ),
            array(
                'master_id' => 2,
                'detail_id' => 1,
                'amount' => 6,
            ),
            array(
                'master_id' => 2,
                'detail_id' => 2,
                'amount' => 1,
            ),
            array(
                'master_id' => 2,
                'detail_id' => 3,
                'amount' => 3,
            ),
            array(
                'master_id' => 2,
                'detail_id' => 4,
                'amount' => 1,
            ),
        );
        DB::table('detail_for_masters')->insert($users);
    }
}
