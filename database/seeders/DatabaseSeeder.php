<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Order;
use Database\Factories\OrderFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        Order::factory(100)->create();
//        dd('Done Seeds');

        $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(UserTelegramSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TypeOfTechnicsSeeder::class);
        $this->call(ManufacturersSeeder::class);
        $this->call(CodeErrorsSeeder::class);
        $this->call(OrderTimeSeeder::class);
        $this->call(OrderStatusesSeeder::class);
        $this->call(DetailCategoriesSeeder::class);
        $this->call(DetailManufacturersSeeder::class);
        $this->call(DetailsSeeder::class);
        $this->call(DetailForMastersSeeder::class);
        $this->call(DetailPhotosSeeder::class);
        $this->call(PricesSeeder::class);
        $this->call(AdviceCategoriesSeeder::class);
        $this->call(AdvicesSeeder::class);
        $this->call(PostCategorySeeder::class);
        $this->call(PostSeeder::class);
//        $this->call(PostLikeSeeder::class);
//        $this->call(PostViewSeeder::class);
//        User::factory(10)->create();
//        Order::factory(100)->create();
//        Comment::factory(100)->create();

//        $this->call(ChatSeeder::class);
//        $this->call(MessageSeeder::class);
//        sqlite_factory(Order::class, 10)->;
    }
}
