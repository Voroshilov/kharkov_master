<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ManufacturersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('manufacturers')->delete();
        $users = array(
            array(
                'title' => 'AEG',
                'image' => 'images/uploads/AEG.gif',
                'slug' => 'aeg',
                'is_active' => 1
            ),
            array(
                'title' => 'Akvilon',
                'image' => 'images/uploads/Akvilon.gif',
                'slug' => 'akvilon',
                'is_active' => 1
            ),
            array(
                'title' => 'Ardo',
                'image' => 'images/uploads/Ardo.gif',
                'slug' => 'ardo',
                'is_active' => 1
            ),
            array(
                'title' => 'Ariston',
                'image' => 'images/uploads/Ariston.gif',
                'slug' => 'ariston',
                'is_active' => 1
            ),
            array(
                'title' => 'Asko',
                'image' => 'images/uploads/Asko.gif',
                'slug' => 'asko',
                'is_active' => 1
            ),
            array(
                'title' => 'ATLANT',
                'image' => 'images/uploads/ATLANT.gif',
                'slug' => 'atlant',
                'is_active' => 1
            ),
            array(
                'title' => 'Ballu',
                'image' => 'images/uploads/Ballu.gif',
                'slug' => 'ballu',
                'is_active' => 1
            ),
            array(
                'title' => 'Bauknecht',
                'image' => 'images/uploads/Bauknecht.gif',
                'slug' => 'bauknecht',
                'is_active' => 1
            ),
            array(
                'title' => 'Beko',
                'image' => 'images/uploads/Beko.gif',
                'slug' => 'beko',
                'is_active' => 1
            ),
            array(
                'title' => 'Blomberg',
                'image' => 'images/uploads/Blomberg.gif',
                'slug' => 'blomberg',
                'is_active' => 1
            ),
            array(
                'title' => 'Bork',
                'image' => 'images/uploads/Bork.gif',
                'slug' => 'bork',
                'is_active' => 1
            ),
            array(
                'title' => 'Bosch',
                'image' => 'images/uploads/Bosch.gif',
                'slug' => 'bosch',
                'is_active' => 1
            ),
            array(
                'title' => 'Brandt',
                'image' => 'images/uploads/Brandt.gif',
                'slug' => 'brandt',
                'is_active' => 1
            ),
            array(
                'title' => 'Braun',
                'image' => 'images/uploads/Braun.gif',
                'slug' => 'braun',
                'is_active' => 1
            ),
            array(
                'title' => 'Candy',
                'image' => 'images/uploads/Candy.gif',
                'slug' => 'candy',
                'is_active' => 1
            ),
            array(
                'title' => 'carrier',
                'image' => 'images/uploads/carrier.gif',
                'slug' => 'carrier',
                'is_active' => 1
            ),
            array(
                'title' => 'Cata',
                'image' => 'images/uploads/Cata.gif',
                'slug' => 'cata',
                'is_active' => 1
            ),
            array(
                'title' => 'Chigo',
                'image' => 'images/uploads/Chigo.gif',
                'slug' => 'chigo',
                'is_active' => 1
            ),
            array(
                'title' => 'Cooper&Hunter',
                'image' => 'images/uploads/Cooper&Hunter.gif',
                'slug' => 'cooperhunter',
                'is_active' => 1
            ),
            array(
                'title' => 'Кристалл',
                'image' => 'images/uploads/Кристалл.gif',
                'slug' => 'kristall',
                'is_active' => 1
            ),
            array(
                'title' => 'Daewoo',
                'image' => 'images/uploads/Daewoo.gif',
                'slug' => 'daewoo',
                'is_active' => 1
            ),
            array(
                'title' => 'daikin',
                'image' => 'images/uploads/daikin.gif',
                'slug' => 'daikin',
                'is_active' => 1
            ),
            array(
                'title' => 'dantex',
                'image' => 'images/uploads/dantex.gif',
                'slug' => 'dantex',
                'is_active' => 1
            ),
            array(
                'title' => 'electra',
                'image' => 'images/uploads/electra.gif',
                'slug' => 'electra',
                'is_active' => 1
            ),
            array(
                'title' => 'Elettrobar',
                'image' => 'images/uploads/Elettrobar.gif',
                'slug' => 'elettrobar',
                'is_active' => 1
            ),
            array(
                'title' => 'Electrolux',
                'image' => 'images/uploads/Electrolux.gif',
                'slug' => 'electrolux',
                'is_active' => 1
            ),
            array(
                'title' => 'Elenberg',
                'image' => 'images/uploads/Elenberg.gif',
                'slug' => 'elenberg',
                'is_active' => 1
            ),
            array(
                'title' => 'Erisson',
                'image' => 'images/uploads/Erisson.gif',
                'slug' => 'erisson',
                'is_active' => 1
            ),
            array(
                'title' => 'Eumenia',
                'image' => 'images/uploads/Eumenia.gif',
                'slug' => 'eumenia',
                'is_active' => 1)
            ,
            array(
                'title' => 'Euronova',
                'image' => 'images/uploads/Euronova.gif',
                'slug' => 'euronova',
                'is_active' => 1
            ),
            array(
                'title' => 'Fagor',
                'image' => 'images/uploads/Fagor.gif',
                'slug' => 'fagor',
                'is_active' => 1
            ),
            array(
                'title' => 'Falmec',
                'image' => 'images/uploads/Falmec.gif',
                'slug' => 'falmec',
                'is_active' => 1
            ),
            array(
                'title' => 'fujitsu',
                'image' => 'images/uploads/fujitsu.gif',
                'slug' => 'fujitsu',
                'is_active' => 1
            ),
            array(
                'title' => 'Gaggenau',
                'image' => 'images/uploads/Gaggenau.gif',
                'slug' => 'gaggenau',
                'is_active' => 1
            ),
            array(
                'title' => 'general',
                'image' => 'images/uploads/general.gif',
                'slug' => 'general',
                'is_active' => 1
            ),
            array(
                'title' => 'Gorenje',
                'image' => 'images/uploads/Gorenje.gif',
                'slug' => 'gorenje',
                'is_active' => 1
            ),
            array(
                'title' => 'Gree',
                'image' => 'images/uploads/Gree.gif',
                'slug' => 'gree',
                'is_active' => 1
            ),
            array(
                'title' => 'haier',
                'image' => 'images/uploads/haier.gif',
                'slug' => 'haier',
                'is_active' => 1
            ),
            array(
                'title' => 'Hansa',
                'image' => 'images/uploads/Hansa.gif',
                'slug' => 'hansa',
                'is_active' => 1
            ),
            array(
                'title' => 'Hitachi',
                'image' => 'images/uploads/Hitachi.gif',
                'slug' => 'hitachi',
                'is_active' => 1
            ),
            array(
                'title' => 'Hoover',
                'image' => 'images/uploads/Hoover.gif',
                'slug' => 'hoover',
                'is_active' => 1
            ),
            array(
                'title' => 'Hyundai',
                'image' => 'images/uploads/Hyundai.gif',
                'slug' => 'hyundai',
                'is_active' => 1
            ),
            array(
                'title' => 'А-Айсберг',
                'image' => 'images/uploads/А-Айсберг.gif',
                'slug' => 'a-aysberg',
                'is_active' => 1
            ),
            array(
                'title' => 'Indesit',
                'image' => 'images/uploads/Indesit.gif',
                'slug' => 'indesit',
                'is_active' => 1
            ),
            array(
                'title' => 'Jetair',
                'image' => 'images/uploads/Jetair.gif',
                'slug' => 'jetair',
                'is_active' => 1
            ),
            array(
                'title' => 'JVC',
                'image' => 'images/uploads/JVC.gif',
                'slug' => 'jvc',
                'is_active' => 1
            ),
            array(
                'title' => 'Kaiser',
                'image' => 'images/uploads/Kaiser.gif',
                'slug' => 'kaiser',
                'is_active' => 1
            ),
            array(
                'title' => 'Kentatsu',
                'image' => 'images/uploads/Kentatsu.gif',
                'slug' => 'kentatsu',
                'is_active' => 1
            ),
            array(
                'title' => 'Kenwood',
                'image' => 'images/uploads/Kenwood.gif',
                'slug' => 'kenwood',
                'is_active' => 1),
            array(
                'title' => 'Krona',
                'image' => 'images/uploads/Krona.gif',
                'slug' => 'krona',
                'is_active' => 1),
            array(
                'title' => 'Kuppersberg',
                'image' => 'images/uploads/Kuppersberg.gif',
                'slug' => 'kuppersberg',
                'is_active' => 1),
            array(
                'title' => 'Kuppersbusch',
                'image' => 'images/uploads/Kuppersbusch.gif',
                'slug' => 'kuppersbusch',
                'is_active' => 1),
            array(
                'title' => 'Lessar',
                'image' => 'images/uploads/Lessar.gif',
                'slug' => 'lessar',
                'is_active' => 1
            ),
            array(
                'title' => 'LG',
                'image' => 'images/uploads/LG.gif',
                'slug' => 'lg',
                'is_active' => 1
            ),
            array(
                'title' => 'Liebherr',
                'image' => 'images/uploads/Liebherr.gif',
                'slug' => 'liebherr',
                'is_active' => 1
            ),
            array(
                'title' => 'Midea',
                'image' => 'images/uploads/Midea.gif',
                'slug' => 'midea',
                'is_active' => 1
            ),
            array(
                'title' => 'Miele',
                'image' => 'images/uploads/Miele.gif',
                'slug' => 'miele',
                'is_active' => 1
            ),
            array(
                'title' => 'mitsubishi',
                'image' => 'images/uploads/mitsubishi.gif',
                'slug' => 'mitsubishi',
                'is_active' => 1
            ),
            array(
                'title' => 'Moulinex',
                'image' => 'images/uploads/Moulinex.gif',
                'slug' => 'moulinex',
                'is_active' => 1
            ),
            array(
                'title' => 'Mystery',
                'image' => 'images/uploads/Mystery.gif',
                'slug' => 'mystery',
                'is_active' => 1
            ),
            array(
                'title' => 'Neoclima',
                'image' => 'images/uploads/Neoclima.gif',
                'slug' => 'neoclima',
                'is_active' => 1
            ),
            array(
                'title' => 'Nord',
                'image' => 'images/uploads/Nord.gif',
                'slug' => 'nord',
                'is_active' => 1
            ),
            array(
                'title' => 'Ока',
                'image' => 'images/uploads/Ока.gif',
                'slug' => 'oka',
                'is_active' => 1
            ),
            array(
                'title' => 'Panasonic',
                'image' => 'images/uploads/Panasonic.gif',
                'slug' => 'panasonic',
                'is_active' => 1
            ),
            array(
                'title' => 'Philco',
                'image' => 'images/uploads/Philco.gif',
                'slug' => 'philco',
                'is_active' => 1
            ),
            array(
                'title' => 'Philips',
                'image' => 'images/uploads/Philips.gif',
                'slug' => 'philips',
                'is_active' => 1
            ),
            array(
                'title' => 'Pioneer',
                'image' => 'images/uploads/Pioneer.gif',
                'slug' => 'pioneer',
                'is_active' => 1
            ),
            array(
                'title' => 'Polair',
                'image' => 'images/uploads/Polair.gif',
                'slug' => 'polair',
                'is_active' => 1
            ),
            array(
                'title' => 'Redmond',
                'image' => 'images/uploads/Redmond.gif',
                'slug' => 'redmond',
                'is_active' => 1
            ),
            array(
                'title' => 'Roda',
                'image' => 'images/uploads/Roda.gif',
                'slug' => 'roda',
                'is_active' => 1
            ),
            array(
                'title' => 'Rolsen',
                'image' => 'images/uploads/Rolsen.gif',
                'slug' => 'rolsen',
                'is_active' => 1
            ),
            array(
                'title' => 'Rubin',
                'image' => 'images/uploads/Rubin.gif',
                'slug' => 'rubin',
                'is_active' => 1
            ),
            array(
                'title' => 'Samsung',
                'image' => 'images/uploads/Samsung.gif',
                'slug' => 'samsung',
                'is_active' => 1
            ),
            array(
                'title' => 'Cевер',
                'image' => 'images/uploads/Cевер.gif',
                'slug' => 'cever',
                'is_active' => 1
            ),
            array(
                'title' => 'Sharp',
                'image' => 'images/uploads/Sharp.gif',
                'slug' => 'sharp',
                'is_active' => 1
            ),
            array(
                'title' => 'Shivaki',
                'image' => 'images/uploads/Shivaki.gif',
                'slug' => 'shivaki',
                'is_active' => 1
            ),
            array(
                'title' => 'Siemens',
                'image' => 'images/uploads/Siemens.gif',
                'slug' => 'siemens',
                'is_active' => 1
            ),
            array(
                'title' => 'Siltal',
                'image' => 'images/uploads/Siltal.gif',
                'slug' => 'siltal',
                'is_active' => 1
            ),
            array(
                'title' => 'Smeg',
                'image' => 'images/uploads/Smeg.gif',
                'slug' => 'smeg',
                'is_active' => 1
            ),
            array(
                'title' => 'Sony',
                'image' => 'images/uploads/Sony.gif',
                'slug' => 'sony',
                'is_active' => 1
            ),
            array(
                'title' => 'Stinol',
                'image' => 'images/uploads/Stinol.gif',
                'slug' => 'stinol',
                'is_active' => 1
            ),
            array(
                'title' => 'Supra',
                'image' => 'images/uploads/Supra.gif',
                'slug' => 'supra',
                'is_active' => 1
            ),
            array(
                'title' => 'Tefal',
                'image' => 'images/uploads/Tefal.gif',
                'slug' => 'tefal',
                'is_active' => 1
            ),
            array(
                'title' => 'toshiba',
                'image' => 'images/uploads/toshiba.gif',
                'slug' => 'toshiba',
                'is_active' => 1
            ),
            array(
                'title' => 'Unit',
                'image' => 'images/uploads/Unit.gif',
                'slug' => 'unit',
                'is_active' => 1
            ),
            array(
                'title' => 'Vestel',
                'image' => 'images/uploads/Vestel.gif',
                'slug' => 'vestel',
                'is_active' => 1
            ),
            array(
                'title' => 'Vestfrost',
                'image' => 'images/uploads/Vestfrost.gif',
                'slug' => 'vestfrost',
                'is_active' => 1
            ),
            array(
                'title' => 'Whirlpool',
                'image' => 'images/uploads/Whirlpool.gif',
                'slug' => 'whirlpool',
                'is_active' => 1
            ),
            array(
                'title' => 'Zanussi',
                'image' => 'images/uploads/Zanussi.gif',
                'slug' => 'zanussi',
                'is_active' => 1
            )
        );
        \DB::table('manufacturers')->insert($users);
    }
}
