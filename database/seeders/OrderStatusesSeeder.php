<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class OrderStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_statuses')->delete();
        $users = array(
            array(
                'title' => 'Без мастера',
                'slug' => 'empty',
                'is_active' => 1,
            ),
            array(
                'title' => 'Принята мастером',
                'slug' => 'get_master',
                'is_active' => 1,
            ),
            array(
                'title' => 'В работе',
                'slug' => 'in_work',
                'is_active' => 1,
            ),
            array(
                'title' => 'Выполнена',
                'slug' => 'is_ready',
                'is_active' => 1,
            ),
            array(
                'title' => 'Подтверждена',
                'slug' => 'confirmed',
                'is_active' => 1,
            ),
            array(
                'title' => 'Отменена',
                'slug' => 'canceled',
                'is_active' => 1,
            ),
        );
        DB::table('order_statuses')->insert($users);
    }
}
