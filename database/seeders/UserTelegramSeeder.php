<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserTelegramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_telegrams')->delete();
        $users = array(
            array(//Ворошилов
                'chat_id' => '619156336',
                'lang' => 'ru',
                'is_active' => 1,
            ),
            array(//Антон
                'chat_id' => '1888578757',
                'lang' => 'ru',
                'is_active' => 1,
            ),
            array(//Мастер 1
                'chat_id' => '603324463',//телега Алены
                'lang' => 'ru',
                'is_active' => 1,
            ),
            array(//Мастер 2
                'chat_id' => '619156336',
                'lang' => 'ru',
                'is_active' => 1,
            ),
            array(//Мастер 3
                'chat_id' => '619156336',
                'lang' => 'ru',
                'is_active' => 1,
            ),
            array(//Тамара
                'chat_id' => '619156336',
                'lang' => 'ru',
                'is_active' => 1,
            ),
            array(//Автор
                'chat_id' => '619156336',
                'lang' => 'ru',
                'is_active' => 1,
            ),
        );
        DB::table('user_telegrams')->insert($users);
    }
}
