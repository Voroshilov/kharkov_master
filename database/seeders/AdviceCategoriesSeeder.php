<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AdviceCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advice_categories')->delete();
        $users = array(
            array(
                'parent_id' => 0,
                'title' => 'Совет по уходу за техникой',
                'slug' => 'soveti-po-uhode-za-techikoy',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Советы при покупке',
                'slug' => 'soveti-peri-pokupke',
                'is_active' => 1,
            ),
            array(
                'parent_id' => 0,
                'title' => 'Просто советы',
                'slug' => 'prosto-soveti',
                'is_active' => 1,
            ),

            array(
                'parent_id' => 1,
                'title' => 'Советы по супер уходу ',
                'slug' => 'soveti-po-super-uhodu',
                'is_active' => 1,
            ),

        );
        DB::table('advice_categories')->insert($users);
    }
}
