<?php
namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        $users = array(
            array(
                'title' => 'Администратор',
                'slug' => 'admin',
            ),
            array(
                'title' => 'Мастер',
                'slug' => 'master',
            ),
            array(
                'title' => 'Старшиый мастер',
                'slug' => 'senior-master',
            ),
            array(
                'title' => 'Оператор',
                'slug' => 'operator',
            ),
            array(
                'title' => 'Автор',
                'slug' => 'author',
            ),
            array(
                'title' => 'Стажор',
                'slug' => 'trainee',
            ),
            array(
                'title' => 'Клиент',
                'slug' => 'client',
            ),
            array(
                'title' => 'Юзер',
                'slug' => 'user',
            ),
        );
        DB::table('roles')->insert($users);
    }
}
