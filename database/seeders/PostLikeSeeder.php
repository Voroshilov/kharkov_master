<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostLikeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_likes')->delete();
        $data = [];
            for($i = 1; $i <= 100; $i++) {
                $user_id = rand(1, 10);
                $post_id = rand(1, 10);
                $is_like = rand(rand(0, 1), 1);
                $is_active = rand(rand(rand(rand(0, 1), 1), 1), 1);

                $data[] = array(
                    'user_id' => $user_id,
                    'post_id' => $post_id,
                    'is_like' => $is_like,
                    'is_active' => $is_active,
                );
            }

        DB::table('post_likes')->insert($data);
    }
}
