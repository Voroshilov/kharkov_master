<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class DetailPhotosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('detail_photos')->delete();
        $users = array(
            array(
                'detail_id' => 1,
                'title' => 'some text',
                'url' => '/images/details_photo/imag_1.jpg',
            ),
            array(
                'detail_id' => 2,
                'title' => 'some text',
                'url' => '/images/details_photo/imag_2.jpg',
            ),
            array(
                'detail_id' => 3,
                'title' => 'some text',
                'url' => '/images/details_photo/imag_3.jpg',
            ),
            array(
                'detail_id' => 4,
                'title' => 'some text',
                'url' => '/images/details_photo/imag_4.jpg',
            ),
        );
        DB::table('detail_photos')->insert($users);
    }
}
