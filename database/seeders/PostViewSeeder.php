<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostViewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 15000;
        $data = $this->getData($count);

        DB::table('post_views')->insert($data);
        DB::table('post_views')->insert($data);
        DB::table('post_views')->insert($data);
        DB::table('post_views')->insert($data);
        DB::table('post_views')->insert($data);
        DB::table('post_views')->insert($data);
        DB::table('post_views')->insert($data);
        DB::table('post_views')->insert($data);
        DB::table('post_views')->insert($data);
        DB::table('post_views')->insert($data);
    }

    /**
     * @return array
     */
    public function getData($count)
    {
        $data = [];

        for($i = 1; $i <= $count; $i++) {
            $user_id = rand(0, rand(0, rand(0, rand(0, rand(0, rand(0, rand(0, rand(0, 10))))))));
            $ip = rand(1, 255) . '.' . rand(1, 255) . '.' . rand(1, 255) . '.' . rand(1, 255);

            if ($user_id == 0)
                $user_id = null;

            $data[] = array(
                'user_id' => $user_id,
                'post_id' => rand(1, 10),
                'user_ip' => $ip,
                'created_at' => Carbon::now(),
            );
        }

        return $data;
    }
}
