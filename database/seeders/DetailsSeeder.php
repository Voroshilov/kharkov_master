<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class DetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('details')->delete();
        $users = array(
            array(
                'category_id' => 11,
                'detail_manufacturer_id' => 1,
                'amount' => 3,
                'selling_price' => 200,
                'purchase_price' => 150,
                'title' => 'Термоват',
                'slug' => 'termovat',
                'internal_description' => 'termovat',
                'public_description' => 'termovat',
                'is_new' => 1,
                'is_active' => 1,
            ),
            array(
                'category_id' => 10,
                'detail_manufacturer_id' => 1,
                'amount' => 3,
                'selling_price' => 250,
                'purchase_price' => 150,
                'title' => 'Помпа на 3 защелки',
                'slug' => 'pump_3samoreza',
                'internal_description' => 'termovat',
                'public_description' => 'termovat',
                'is_new' => 1,
                'is_active' => 1,
            ),
            array(
                'category_id' => 13,
                'detail_manufacturer_id' => 1,
                'amount' => 3,
                'selling_price' => 150,
                'purchase_price' => 60,
                'title' => 'Щетки 12,5 без пружинки',
                'slug' => 'shetki_15.5',
                'internal_description' => 'shetki',
                'public_description' => 'shetki',
                'is_new' => 1,
                'is_active' => 1,
            ),
            array(
                'category_id' => 18,
                'detail_manufacturer_id' => 1,
                'amount' => 30,
                'selling_price' => 100,
                'purchase_price' => 80,
                'title' => 'Подшипники 204',
                'slug' => 'podshipnik_204',
                'internal_description' => 'podshipnik_204',
                'public_description' => 'podshipnik_204',
                'is_new' => 1,
                'is_active' => 1,
            ),
            array(
                'category_id' => 22,
                'detail_manufacturer_id' => 1,
                'amount' => 15,
                'selling_price' => 80,
                'purchase_price' => 40,
                'title' => 'Сальник 30/52/10',
                'slug' => 'oil_seals_30_52_10',
                'internal_description' => 'oil_seals_30_52_10',
                'public_description' => 'oil_seals_30_52_10',
                'is_new' => 1,
                'is_active' => 1,
            ),
        );
        DB::table('details')->insert($users);
    }
}
