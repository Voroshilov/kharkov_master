<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class DetailManufacturersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('detail_manufacturers')->delete();
        $users = array(
            array(
                'title' => 'Askoll',
                'slug' => 'askoll',
                'is_active' => 1,
            ),
            array(
                'title' => 'Термоват',
                'slug' => 'termovat',
                'is_active' => 1,
            ),
            array(
                'title' => 'Кавай',
                'slug' => 'kavai',
                'is_active' => 1,
            ),
            array(
                'title' => 'SKF',
                'slug' => 'skf',
                'is_active' => 1,
            ),
            array(
                'title' => 'Gaf',
                'slug' => 'gaf',
                'is_active' => 1,
            ),
        );
        DB::table('detail_manufacturers')->insert($users);
    }
}
