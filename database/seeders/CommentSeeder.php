<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->delete();
        $data = [];
        for($i = 1; $i <= 100; $i++) {
            $user_id = rand(0, 10);

            if ($user_id == 0)
                $user_id = null;

            $data[] = array(
                'user_id' => $user_id,
                'post_id' => rand(1, 10),
                'is_like' => rand(rand(0, 1), 1),
            );
        }

        DB::table('comments')->insert($data);
    }
}
