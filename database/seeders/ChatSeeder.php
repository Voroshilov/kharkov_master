<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class ChatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chats')->delete();
        $users = array(
            array(
                'first_user_id' => 1,
                'second_user_id' => 2,
            ),
            array(
                'first_user_id' => 1,
                'second_user_id' => 3,
            ),
            array(
                'first_user_id' => 1,
                'second_user_id' => 4,
            ),
            array(
                'first_user_id' => 2,
                'second_user_id' => 3,
            ),
            array(
                'first_user_id' => 1,
                'second_user_id' => 6,
            ),
        );
        DB::table('chats')->insert($users);
    }
}
