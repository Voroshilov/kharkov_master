<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->delete();
        $users = array(
            array(
                'chat_id' => 1,
                'from_user_id' => 1,
                'to_user_id' => 2,
                'sender_ip' => '127.0.0.1',
                'is_read' => 0,
                'is_visible_first_user' => 1,
                'is_visible_second_user' => 1,
                'message' => 'tead ast',
            ),
            array(
                'chat_id' => 1,
                'from_user_id' => 2,
                'to_user_id' => 1,
                'sender_ip' => '127.0.0.1',
                'is_read' => 0,
                'is_visible_first_user' => 1,
                'is_visible_second_user' => 1,
                'message' => 'ted awd awdst',
            ),
            array(
                'chat_id' => 1,
                'from_user_id' => 1,
                'to_user_id' => 2,
                'sender_ip' => '127.0.0.1',
                'is_read' => 0,
                'is_visible_first_user' => 1,
                'is_visible_second_user' => 1,
                'message' => 'tes  w dt',
            ),
            array(
                'chat_id' => 2,
                'from_user_id' => 1,
                'to_user_id' => 3,
                'sender_ip' => '127.0.0.1',
                'is_read' => 0,
                'is_visible_first_user' => 1,
                'is_visible_second_user' => 1,
                'message' => 'test  w',
            ),
            array(
                'chat_id' => 1,
                'from_user_id' => 3,
                'to_user_id' => 1,
                'sender_ip' => '127.0.0.1',
                'is_read' => 0,
                'is_visible_first_user' => 1,
                'is_visible_second_user' => 1,
                'message' => 'test',
            ),
        );
        DB::table('messages')->insert($users);
    }
}
