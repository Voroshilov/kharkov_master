<?php

namespace Database\Seeders;

use DB;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use App\Traits\HasRolesAndPermissions;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('slug','admin')->first();
        $master = Role::where('slug', 'master')->first();
        $seniorMaster = Role::where('slug', 'senior-master')->first();
        $operator = Role::where('slug', 'operator')->first();
        $author = Role::where('slug', 'author')->first();
        $client = Role::where('slug', 'client')->first();
        $user = Role::where('slug', 'user')->first();

        $all_permissions = Permission::all();
        $applications = Permission::where('slug','orders')->first();
        $admin_home = Permission::where('slug','admin-home')->first();
        $posts = Permission::where('slug','posts')->first();
//        $client = Permission::where('slug','client')->first();

        $user1 = new User();
        $user1->first_name = 'Дмитрий';
        $user1->second_name = 'Ворошилов';
        $user1->email = 'dima@mail.ru';
        $user1->phone = '0997899313';
        $user1->address = 'Там!';
        $user1->telegram_id = 1;
        $user1->avatar = '/admin/dist/img/avatar5.png';
        $user1->skills = '{"skills":["стиральные машины", "Посудомоечные машины", "Сушильные шкафы"], "all": "Могу все"}';
        $user1->password = bcrypt('123456');
        $user1->save();
        $user1->roles()->attach($admin);
        $this->allPermissions($user1, $all_permissions);

        $user2 = new User();
        $user2->first_name = 'Антон';
        $user2->second_name = 'Пупкин';
        $user2->email = 'anton@mail.ru';
        $user2->phone = '0957330479';
        $user2->address = 'Там!';
        $user2->telegram_id = 2;
        $user2->avatar = '/admin/dist/img/avatar.png';
        $user2->password = bcrypt('123456');
        $user2->save();
        $user2->roles()->attach($seniorMaster);
        $user2->permissions()->attach($applications);
        $user2->permissions()->attach($admin_home);

        $user3 = new User();
        $user3->first_name = 'Мастер';
        $user3->second_name = 'Фломастер';
        $user3->email = 'master1@mail.ru';
        $user3->phone = '0669042904';
        $user3->address = 'Там!';
        $user3->telegram_id = 3;
        $user3->avatar = '/admin/dist/img/avatar2.png';
        $user3->password = bcrypt('123456');
        $user3->save();
        $user3->roles()->attach($master);
        $user3->permissions()->attach($applications);
        $user3->permissions()->attach($admin_home);

        $user4 = new User();
        $user4->first_name = 'Дастер';
        $user4->second_name = 'Мардастер';
        $user4->email = 'master2@mail.ru';
        $user4->phone = '0997899314';
        $user4->address = 'Там!';
        $user4->telegram_id = 4;
        $user4->avatar = '/admin/dist/img/avatar3.png';
        $user4->password = bcrypt('123456');
        $user4->save();
        $user4->roles()->attach($master);
        $user4->permissions()->attach($applications);
        $user4->permissions()->attach($admin_home);

        $user5 = new User();
        $user5->first_name = 'Шишкин';
        $user5->second_name = 'Машишкин';
        $user5->email = 'master3@mail.ru';
        $user5->phone = '0997899315';
        $user5->address = 'Там!';
        $user5->telegram_id = 5;
        $user5->avatar = '/admin/dist/img/user1-128x128.jpg';
        $user5->password = bcrypt('123456');
        $user5->save();
        $user5->roles()->attach($master);
        $user5->permissions()->attach($applications);
        $user5->permissions()->attach($admin_home);

        $user6 = new User();
        $user6->first_name = 'Тамара';
        $user6->second_name = 'Омара';
        $user6->email = 'tamara@mail.ru';
        $user6->phone = '0997899316';
        $user6->address = 'Там!';
        $user6->telegram_id = 6;
        $user6->avatar = '/admin/dist/img/avatar4.png';
        $user6->password = bcrypt('123456');
        $user6->save();
        $user6->roles()->attach($operator);
        $user6->permissions()->attach($applications);
        $user6->permissions()->attach($admin_home);

        $user7 = new User();
        $user7->first_name = 'Автор';
        $user7->second_name = 'Мавтор';
        $user7->email = 'author@mail.ru';
        $user7->phone = '0997899317';
        $user7->address = 'Там!';
        $user7->telegram_id = 7;
        $user7->avatar = '/admin/dist/img/avatar5.png';
        $user7->password = bcrypt('123456');
        $user7->save();
        $user7->roles()->attach($author);
        $user7->permissions()->attach($posts);
        $user7->permissions()->attach($admin_home);

        $user8 = new User();
        $user8->first_name = 'Маша';
        $user8->second_name = 'Малаша';
        $user8->email = 'masha@mail.ru';
        $user8->phone = '0997899887';
        $user8->address = 'хто зна где!';
        $user8->telegram_id = 7;
        $user8->avatar = '/admin/dist/img/user7-128x128.jpg';
        $user8->password = bcrypt('123456');
        $user8->save();
        $user8->roles()->attach($client);

        $user9 = new User();
        $user9->first_name = 'Миша';
        $user9->second_name = 'Живиша';
        $user9->email = 'misha@mail.ru';
        $user9->phone = '0997899876';
        $user9->address = 'хто зна где!';
        $user9->telegram_id = 7;
        $user9->avatar = '/admin/dist/img/user8-128x128.jpg';
        $user9->password = bcrypt('123456');
        $user9->save();
        $user9->roles()->attach($client);

        $user10 = new User();
        $user10->first_name = 'Каша';
        $user10->second_name = 'Малаша';
        $user10->email = 'kasha@mail.ru';
        $user10->phone = '0997899876';
        $user10->address = 'хто зна где!';
        $user10->telegram_id = 7;
        $user10->avatar = '/admin/dist/img/user6-128x128.jpg';
        $user10->password = bcrypt('123456');
        $user10->save();
        $user10->roles()->attach($user);
    }

    public function allPermissions($user, $all_permissions)
    {
        foreach ($all_permissions as $perm) {
            $user->permissions()->attach($perm);
        }
    }
}
