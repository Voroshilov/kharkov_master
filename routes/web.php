<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Blog\LikeController;
use App\Http\Controllers\Admin\HomeController as AdminHomeController;
//use App\Http\Controllers\Admin\OrderController;
use \App\Http\Controllers\Blog\CommentController;
use \App\Http\Controllers\Blog\PageController;

Route::get('/', [ContentController::class, 'index'])
    ->name('index');
Route::post('/order_from_client', [ContentController::class, 'orderFromClient'])
    ->name('index.order_from_client');
Route::get('/contacts', [ContentController::class, 'contacts'])
    ->name('index.contacts');

Route::get('/contact', function () {
    return view('contact');
});

//Auth::routes();

//Route::get('/test', [App\Http\Controllers\TestController::class, 'testFib'])->name('test');
//Route::get('/river', [App\Http\Controllers\TestController::class, 'river'])->name('river');

Route::group(['middleware' => 'role:admin'], function() {
    Route::get('/dashboard', function() {
        return 'Добро пожаловать, Веб-разработчик';
    });
});



//Админка
Route::middleware(['role:admin'])->prefix('admin')->group(function() {
    Route::get('/home', [AdminHomeController::class, 'home'])->name('admin.home');
});

Route::group(['middleware' => 'role:admin'], function() {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->group(function() {
        //Routes для поиска
        Route::get('/orders/search', 'OrderController@search')
            ->name('admin.orders.search');
        Route::resource('/orders', 'OrderController')
            ->names('admin.orders');
//        Route::get('/orders/search', [OrderController::class, 'search'])->name('admin.orders.search');
//        Route::resource('orders', OrderController::class)->names('admin.orders');

        Route::get('/posts/search', 'PostController@search')
            ->name('admin.posts.search');
        Route::post('/posts/upload-file', 'PostController@uploadImage')
            ->name('admin.posts.upload_file');
        Route::resource('/posts', 'PostController')
            ->names('admin.posts');

        Route::get('/post_categories/search', 'PostCategoryController@search')
            ->name('admin.post_categories.search');
        Route::resource('/post_categories', 'PostCategoryController')
            ->names('admin.post_categories');

        Route::get('/manufacturers/search', 'ManufacturerController@search')
            ->name('admin.manufacturers.search');
        Route::resource('/manufacturers', 'ManufacturerController')
            ->names('admin.manufacturers');

        Route::get('/type_of_technics/search', 'TypeOfTechnicController@search')
            ->name('admin.type_of_technics.search');
        Route::resource('/type_of_technics', 'TypeOfTechnicController')
            ->names('admin.type_of_technics');


        Route::get('/users/search', 'UserController@search')
            ->name('admin.users.search');
        Route::resource('/users', 'UserController')
            ->names('admin.users');
        Route::get('/users/staffs', 'StaffController@search')
            ->name('admin.staffs.search');
        Route::resource('/staffs', 'StaffController')
            ->names('admin.staffs');

        Route::get('/chats', 'ChatController@index')
            ->name('admin.chats.index');
        Route::get('/chats/{chat_id}', 'ChatController@show')
            ->name('admin.chats.show');
        Route::post('/chats/store', 'ChatController@store')
            ->name('admin.chats.store');
    });
});

Auth::routes();
Route::prefix('blog')->namespace('App\Http\Controllers\Blog')->group(function() {

    Route::get('/home', [HomeController::class, 'home'])->name('blog.home');

    Route::get('/posts', 'PostController@index')
        ->name('blog.posts.index');
    Route::get('/by-category/{category}', 'PostController@indexByCategory')
        ->name('blog.posts.by-category');
    Route::get('/posts/{slug}', 'PostController@show')
        ->name('blog.posts.show');

    Route::post('/posts/set-like', [LikeController::class, 'setLike'])
        ->name('blog.posts.set-like');

    Route::post('/posts/set-comment', [CommentController::class, 'setComment'])
        ->name('blog.posts.set-comment');

    //pages
    Route::get('/contacts', [PageController::class, 'contacts'])
        ->name('blog.contacts');

    Route::get('/gallery', [PageController::class, 'gallery'])
        ->name('blog.gallery');

    Route::get('/about-us', [PageController::class, 'aboutUs'])
        ->name('blog.about-us');


//    Route::get('/', 'PostController@show')
//        ->name('template_blog.index.category');

//    Route::get('/orders/search', 'OrderController@search')
//        ->name('admin.orders.search');
//    Route::resource('/orders', 'OrderController')
//        ->names('admin.orders');

});


Route::get('/dryer', ['as' => 'index.dryer', 'uses' => 'App\Http\Controllers\ContentController@dryer']);
Route::get('/sd', ['as' => 'index.sd', 'uses' => 'App\Http\Controllers\ContentController@sd']);
Route::get('/kiev', ['as' => 'index.kiev', 'uses' => 'App\Http\Controllers\ContentController@kiev']);
Route::get('/map', ['as' => 'map', 'uses' => 'App\Http\Controllers\ContentController@map']);

//pages area
Route::get('/saltovka', ['as' => 'index.saltovka', 'uses' => 'App\Http\Controllers\ContentController@saltovka']);
Route::get('/alekseevka', ['as' => 'index.alekseevka', 'uses' => 'App\Http\Controllers\ContentController@alekseevka']);
//page machines
Route::get('/bosch', ['as' => 'index.bosch', 'uses' => 'App\Http\Controllers\ContentController@bosch']);
Route::get('/electrolux', ['as' => 'index.electrolux', 'uses' => 'App\Http\Controllers\ContentController@electrolux']);
Route::get('/indesit', ['as' => 'index.indesit', 'uses' => 'App\Http\Controllers\ContentController@indesit']);
Route::get('/samsung', ['as' => 'index.samsung', 'uses' => 'App\Http\Controllers\ContentController@samsung']);
Route::get('/zanussi', ['as' => 'index.zanussi', 'uses' => 'App\Http\Controllers\ContentController@zanussi']);
Route::get('/ariston', ['as' => 'index.ariston', 'uses' => 'App\Http\Controllers\ContentController@ariston']);
Route::get('/ardo', ['as' => 'index.ardo', 'uses' => 'App\Http\Controllers\ContentController@ardo']);
Route::get('/lg', ['as' => 'index.lg', 'uses' => 'App\Http\Controllers\ContentController@lg']);





Route::get('/streaming', 'App\Http\Controllers\WebrtcStreamingController@index');
Route::get('/streaming/{streamId}', 'App\Http\Controllers\WebrtcStreamingController@consumer');
Route::post('/stream-offer', 'App\Http\Controllers\WebrtcStreamingController@makeStreamOffer');
Route::post('/stream-answer', 'App\Http\Controllers\WebrtcStreamingController@makeStreamAnswer');
