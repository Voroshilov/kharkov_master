
<script>
    var array = [
        [13, 15, 3,  1,  5,  10, 13, 13, 15],
        [13, 15, 3,  0,  0,  10, 13, 13, 15],
        [13, 15, 0,  0,  0,  10, 13, 13, 15],
        [13, 15, 3,  0,  1,  10, 13, 13, 15],
        [13, 15, 3,  0,  1,  10, 13, 13, 15],
        [11, 12, 2,  1,  2,  11, 10, 11, 12],
        [11, 2,  3,  5,  2,  5,  7,  6,  24],
        [3,  2,  3,  5,  2,  3,  4,  5,  14],
        [5,  4,  3,  4,  3,  4,  5,  6,  18],
        [17, 9,  7,  4,  7,  8,  9,  10, 57],
        [13, 8,  6,  6,  5,  6,  7,  9,  25],
        [11, 10, 8,  7,  6,  8,  9,  11, 35],
        [13, 10, 8,  10, 11, 12, 13, 15, 19],
        [15, 11, 10, 9,  9,  10, 11, 12, 20]
    ];

    var new_pole = [];

    var pole = []


    function rieka(x, y, array) {

        //Проверка на не нулевую первую точку
        if(array[x][y] === 0){
            return alert('Начало реки не может быть точной со значением 0!!!');
        }
        //что бы не передавать массив во все методы, а обращатся к нему из любой точки класса
        this.pole = array;
        //создает новое пустое поле, на котором будем наносить новые координаты
        this.new_pole = createEmptyArray();
        //Выбрасывает из метода если точка лежит за пределами поля
        if(!this.pole[x][y]){
            return;
        }

        this.new_pole[x][y] = 1;//Точна начала течения реки

        riverBed(x, y);

        //Создает поле течения реки, на основании переменной this.new_pole
        createRiverBed();
    }

    //клон поля с нулевыми значениями
    function createEmptyArray()
    {
        var new_array = [];
        for (var i = 0; i < pole.length; i++) {
            new_array[i] = [];
            for (var j = 0; j < pole[i].length; j++) {
                if(pole[i][j] > 0) {
                    new_array[i][j] = 0;
                }else{
                    new_array[i][j] = 0;
                }
            }
        }

        return new_array;
    }

    //Выбирает 8 значений вокруг указаной точки
    function riverBed(x, y) {

        var array_by_top = getArrayByTopPoint(x, y);

        writeToNewArray(x, y, array_by_top);
    }

    //создает массив вокруг выбраной точки
    function getArrayByTopPoint(x, y)
    {
        var array_by_top = [];
        //начало обработки массива вокруг укаазной точки
        var x_start = x - 1;
        var y_start = y - 1;

        for (var i_r = x_start; i_r < this.pole.length; i_r++) {
            if(i_r >= x + 2)
                break;
            if(this.pole[i_r] === false || i_r < 0)//Проверка на выход за пределы массива
                continue;

            for (var j_r = y_start; j_r < this.pole[i_r].length; j_r++) {
                if(j_r >= y + 2)
                    break;
                if(this.pole[i_r][j_r] === false || j_r < 0)//Проверка на выход за пределы массива
                    continue;

                array_by_top.push(this.pole[i_r][j_r]);
            }
        }

        return array_by_top;
    }

    //Записываем следующие точки для русла реки
    function writeToNewArray(x, y, array_by_top)
    {
        this.new_pole[x][y] = 1//Присваиваем 1 точке вокруг которой ищем минимальную высоту для продления русла реки
        var min_point = Math.min(...array_by_top);//минимальная высота (продолжение русла)

        var x_start = x - 1;
        var y_start = y - 1;

        for (var i_r = x_start; i_r < this.pole.length; i_r++) {
            if(i_r >= x_start + 3)//завершает цикл за пределами
                break;

            if(i_r < 0)//Проверка на выход за пределы массива
                continue;

            if(this.pole[i_r].length === false)//Проверка на выход за пределы массива
                continue;

            for (var j_r = y_start; j_r < this.pole[i_r].length; j_r++) {
                if(j_r >= y_start + 3)
                    break;

                if(j_r < 0)//Проверка на выход за пределы массива
                    continue;

                if(this.pole[i_r][j_r] === false)//Проверка на выход за пределы массива
                    continue;

                if(this.pole[i_r][j_r] === min_point && this.new_pole[i_r][j_r] !== 1 ) {//Проверка минимальное значение точки и проверка
                    riverBed(i_r, j_r);
                }
            }
        }
    }


    function createRiverBed()
    {
        var table = "<table>";

        for (var i = 0; i < this.new_pole.length; i++) {

            table += "<tr>";

            for (var j = 0; j < this.new_pole[0].length; j++) {
                if(this.new_pole[i][j] > 0) {
                    table += '<th style="background-color:#ff0000;width:20px;height:20px"></th>';
                }else{
                    table +=  '<th style="background-color:#ffff00;width:20px;height:20px"></th>';
                }
            }
            table += "</tr>";
        }
        table += "</table></br>";
        document.writeln(table);
    }


    rieka(0, 0, array);

</script>
