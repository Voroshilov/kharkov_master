@extends('layouts.landing_app')

@section('title')
    Ремонт стиральных машин контакты, Ремонт стиральных машин Киев
@endsection

@section('content')
    <div class="gtco-loader"></div>
    <div id="page">
        @include('layouts.landing_nav')
        <div id="gtco-subscribe">
            <div class="gtco-container">
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center gtco-heading">

                        <h1 style="color: white">Карта сайта</h1>

                    </div>
                </div>
                <div class="row animate-box">
                    <div class="col-md-12">
                        <ul class="gtco-quick-contact">
                            <li><a href="/dryer">Сушки</a></li>
                            <li><a href="/alekseevka">Алексеевка</a></li>
                            <li><a href="/saltovka">Салтовка</a></li>
                            <li><a href="/bosch">Бош</a></li>
{{--                            <li><a href="/siemens">Сименс</a></li>--}}
                            <li><a href="/electrolux">Электролюкс</a></li>
                            <li><a href="/indesit">Индезит</a></li>
                            <li><a href="/ariston">Аристон</a></li>
                            <li><a href="/samsung">Самсунг</a></li>
                            <li><a href="/zanussi">Занусси</a></li>
{{--                            <li><a href="/sd">Ремонт в городе Северодонецк</a></li>--}}
{{--                            <li><a href="/electroprovodka">Ремонт електропроводки</a></li>--}}
{{--                            <li><a href="/plumbing">Ремонт сантехники</a></li>--}}
{{--                            <li><a href="/whirlpool">Вирпул</a></li>--}}
                            <li><a href="/lg">LG</a></li>
{{--                            <li><a href="/candy">Candy</a></li>--}}
                            <li><a href="/ardo">Ardo</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.landing_footer')
    </div>
@endsection

