@extends('layouts.landing_app')

@section('content')
    <div class="gtco-loader"></div>
    <div id="page">
        @include('layouts.landing_nav')
        <div id="gtco-subscribe">
            <div class="gtco-container">
                <br>
                <br>
                <br>
                <br>
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center gtco-heading">

                        <h1 style="color: white">Страница контактов</h1>
                        <br>
                        <br>

                        <h2>Оставить заявку</h2>
                        <p>Оставьте бесплатную заявку и мы обязательно перезвоним.</p>
                    </div>
                </div>
                <div class="row animate-box">
                    <div class="col-md-12">
                        {!! Form::open(array('route' => 'index.order_from_client', 'class' => 'form-inline')) !!}
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label for="email" class="sr-only">Телефон</label>
                                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Телефон']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label for="name" class="sr-only">Имя</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Имя']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <button style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;"
                                    type="submit" class="btn btn-default btn-block">Залишити заявку</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.landing_footer')
    </div>
@endsection

