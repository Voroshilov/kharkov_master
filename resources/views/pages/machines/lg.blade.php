@extends('layouts.landing_app')

@section('content')
    <div class="gtco-loader"></div>
    <div id="page">
        @include('layouts.landing_nav')
        <header id="gtco-header" class="gtco-cover">
            <div class="overlay"></div>
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-12 col-md-offset-0 text-left">
                        <div class="display-t">
                            <div class="display-tc">
                                <h1 class="animate-box" data-animate-effect="fadeInUp">Сломалась стиральная машина Lg?</h1>
                                <h2 class="animate-box" data-animate-effect="fadeInUp">Мы Вам поможем!</h2>
                                <h2 class="animate-box" data-animate-effect="fadeInUp">Ремонт стиральных машин Lg.</h2>
                                <h2 class="animate-box" data-animate-effect="fadeInUp">Оставьте бесплатную заявку и мы обязательно перезвоним.</h2>
                                <button type="button" class="btn"
                                        style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;"
                                        data-toggle="modal" data-target=".bs-example-modal-sm">Бесплатная консультация</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        @include('pages.content')

        @include('layouts.landing_footer')
    </div>

@endsection
