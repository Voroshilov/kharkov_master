@extends('layouts.landing_app')

@section('content')
    <div class="gtco-loader"></div>
    <div id="page">
        @include('layouts.landing_nav')
        <header id="gtco-header" class="gtco-cover">
            <div class="overlay"></div>
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-12 col-md-offset-0 text-left">
                        <div class="display-t">
                            <div class="display-tc">
                                <h1 class="animate-box" data-animate-effect="fadeInUp">Сломалась сушильная машина?</h1>
                                <h2 class="animate-box" data-animate-effect="fadeInUp">Мы Вам поможем!</h2>
                                <h2 class="animate-box" data-animate-effect="fadeInUp">Оставьте бесплатную заявку и мы обязательно перезвоним.</h2>
                                <button type="button" class="btn"
                                        style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;"
                                        data-toggle="modal" data-target=".bs-example-modal-sm">Бесплатная консультация</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="gtco-features-3">
            <div class="gtco-container">
                <div class="gtco-flex">
                    <div class="feature feature-1 animate-box" data-animate-effect="fadeInUp">
                        <div class="feature-inner">
                            <span class="icon">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </span>
                            <h3>Поиск</h3>
                            <p>Подбор лучшего мастера в Вашем районе для самого быстрого и качественного ремонта. </p>
                            <p><a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm"
                                  style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;">Оставить заявку</a></p>
                        </div>
                    </div>
                    <div class="feature feature-2 animate-box" data-animate-effect="fadeInUp">
                        <div class="feature-inner">
                            <span class="icon">
                                <i class="fa fa-bullhorn" aria-hidden="true"></i>
                            </span>
                            <h3>Бесплатная консультация</h3>
                            <p>Позвонив нам, Вы получите максимально широкую консультацию по Вашей сушильной машине. </p>
                            <p><a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm"
                                  style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;">Оставить заявку</a></p>
                        </div>
                    </div>
                    <div class="feature feature-3 animate-box" data-animate-effect="fadeInUp">
                        <div class="feature-inner">
                            <span class="icon">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                            </span>
                            <h3>Время</h3>
                            <p>Мастер приезжает в день вызова. Ремонт за 1 час. </p>
                            <p><a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm"
                                  style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;">Оставить заявку</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="gtco-features" >
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
                        <h2>Ремонт Сушильных Машин У Нас</h2>
                        <p>Ремонтируя сушильные машины у нас, Вы получаете гарантию на ремонт от 6 месяцев, подробное объяснение причин поломки и консультацию для дальнейшей эксплуатации.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                <img src="images/man-min.png" alt="">
                            </span>
                            <h3>Вежливый персонал</h3>
                            <p>Наш персонал вежлив, тактичен,пунктуален. Вы не услышите матерных слов. </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                <img src="images/speed-min.png" alt="">
                            </span>
                            <h3>Оплата после выполненных работ</h3>
                            <p>После выполненной работы мастер обязательно выписывает квитанцию. После этого вы оплачиваете сумму. 	</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                <img src="images/Clock4-min.png" alt="">
                            </span>
                            <h3>Ремонт сушильных машин в Харькове в день заказа</h3>
                            <p>Мы производим ремонт сушильной машины с выездом на дом в течение 1 часа.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                <img src="images/checkout-min.png" alt="">
                            </span>
                            <h3>Точный расчет стоимости ремонта</h3>
                            <p>Мастера проведут полную диагностику и точно рассчитают стоимость ремонта Вашей сушильной машины. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="gtco-counter" class="gtco-section">
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
                        <h2>Цены</h2>
                        <p>Цены по нашим услугам</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
                        <div class="feature-center">
                            <a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm">
                                <span class="icon">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                </span>
                                <span class="counter js-counter" data-from="0" data-to="200" data-speed="2500" data-refresh-interval="50">1</span>
                                <span class="counter-label">Не крутится барабан!</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
                        <div class="feature-center">
                            <a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm">
                                <span class="icon">
                                    <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                </span>
                                <span class="counter js-counter" data-from="0" data-to="250" data-speed="2500" data-refresh-interval="50">1</span>
                                <span class="counter-label">Не сливается вода!</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
                        <div class="feature-center">
                            <a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm">
                                <span class="icon">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                </span>
                                <span class="counter js-counter" data-from="0" data-to="450" data-speed="2500" data-refresh-interval="50">1</span>
                                <span class="counter-label">Не включается!</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
                        <div class="feature-center">
                            <a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm">
                                <span class="icon">
                                    <i class="fa fa-volume-up" aria-hidden="true"></i>
                                </span>
                                <span class="counter js-counter" data-from="0" data-to="250" data-speed="2500" data-refresh-interval="50">1</span>
                                <span class="counter-label">При отжиме сильно шумит!</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="gtco-subscribe">
            <div class="gtco-container">
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center gtco-heading">
                        <h2>Оставить заявку</h2>
                        <p>Оставьте заявку и мы обязательно Вам перезвоним.</p>
                    </div>
                </div>
                <div class="row animate-box">
                    <div class="col-md-12">
                        {!! Form::open(array('route' => 'index.order_from_client', 'class' => 'form-inline')) !!}
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label for="email" class="sr-only">Телефон</label>
                                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Телефон']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label for="name" class="sr-only">Имя</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Имя']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <button style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;"
                                    type="submit" class="btn btn-default btn-block">Оставить заявку</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.landing_footer')
    </div>

@endsection
