@extends('layouts.landing_app')

@section('content')
    <div class="gtco-loader"></div>
    <div id="page">
        @include('layouts.landing_nav_ukr')
        <header id="gtco-header" class="gtco-cover">
            <div class="overlay"></div>
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-12 col-md-offset-0 text-left">
                        <div class="display-t">
                            <div class="display-tc">
                                <h1 class="animate-box" data-animate-effect="fadeInUp">Зламалася пральна машина?</h1>
                                <h2 class="animate-box" data-animate-effect="fadeInUp">Ми Вам допоможемо!</h2>
                                <h2 class="animate-box" data-animate-effect="fadeInUp">Залишіть безкоштовну заявку і ми обов'язково передзвонимо.</h2>
                                <button type="button" class="btn"
                                        style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;"
                                        data-toggle="modal" data-target=".bs-example-modal-sm">Безкоштовна консультація</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="gtco-features-3">
            <div class="gtco-container">
                <div class="gtco-flex">
                    <div class="feature feature-1 animate-box" data-animate-effect="fadeInUp">
                        <div class="feature-inner">
                            <span class="icon">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </span>
                            <h3>Пошук</h3>
                            <p>Підбір найкращого майстра у Вашому районі для найшвидшого та якісного ремонту. </p>
                            <p><a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm"
                                  style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;">Залишити заявку</a></p>
                        </div>
                    </div>
                    <div class="feature feature-2 animate-box" data-animate-effect="fadeInUp">
                        <div class="feature-inner">
                            <span class="icon">
                                <i class="fa fa-bullhorn" aria-hidden="true"></i>
                            </span>
                            <h3>Безкоштовна консультація</h3>
                            <p>Зателефонувавши нам, Ви відримаєте максимально широку консультацію щодо Вашої пральної машини. </p>
                            <p><a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm"
                                  style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;">Залишити заявку</a></p>
                        </div>
                    </div>
                    <div class="feature feature-3 animate-box" data-animate-effect="fadeInUp">
                        <div class="feature-inner">
                            <span class="icon">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                            </span>
                            <h3>Час</h3>
                            <p>Майстер приїжджає в день виклику. Ремонт за 1 годину. </p>
                            <p><a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm"
                                  style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;">Залишити заявку</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="gtco-features" >
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
                        <h2>Ремонт Пральних Машин У Нас</h2>
                        <p>Ремонтуючи пральні машини, Ви відримуєте гарантію на ремонт від 6 місяців, детальне пояснення причин поломки та консультацію для подальшої експлуатації.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                <img src="images/man-min.png" alt="">
                            </span>
                            <h3>Ввічливий персонал</h3>
                            <p>Наш персонал ввічливий, тактовний, пунктуальний. Ви не почуєте матюків.. </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                <img src="images/speed-min.png" alt="">
                            </span>
                            <h3>Оплата після виконаних робіт</h3>
                            <p>Після виконаної робвіди майстер обов'язково виписує квитанцію. Після цього ви сплачуєте суму. 	</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                <img src="images/Clock4-min.png" alt="">
                            </span>
                            <h3>Ремонт пральних машин у Харкові в день замовлення</h3>
                            <p>Ми робимо ремонт пральної машини з виїздом додому првідягом 1 години.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
                                <img src="images/checkout-min.png" alt="">
                            </span>
                            <h3>Точний розрахунок вартості ремонту</h3>
                            <p>Майстри проведуть повну діагностику та точно розрахують вартість ремонту Вашої пральної машини. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="gtco-counter" class="gtco-section">
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
                        <h2>ЦІНИ</h2>
                        {{--<p>Цены по нашим услугам</p>--}}
                        <p>Вказані мінімальні ціни, без урахування запчастин.</p>
                        <p>У разі відмови від ремонту сума діагностики становить 300 грн.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
                        <div class="feature-center">
                            <a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm">
                                <span class="icon">
                                    <i class="fa fa-refresh" aria-hidden="true">від</i>
                                </span>
                                <span class="counter js-counter" data-from="0" data-to="450" data-speed="2500" data-refresh-interval="50">1</span>
                                <span class="counter-label">Не крутиться барабан!</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
                        <div class="feature-center">
                            <a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm">
                                <span class="icon">
                                    <i class="fa fa-cloud-download" aria-hidden="true">від</i>
                                </span>
                                <span class="counter js-counter" data-from="0" data-to="450" data-speed="2500" data-refresh-interval="50">1</span>
                                <span class="counter-label">Не зливається вода!</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
                        <div class="feature-center">
                            <a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm">
                                <span class="icon">
                                    <i class="fa fa-play" aria-hidden="true">від</i>
                                </span>
                                <span class="counter js-counter" data-from="0" data-to="650" data-speed="2500" data-refresh-interval="50">1</span>
                                <span class="counter-label">Не вмикається!</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
                        <div class="feature-center">
                            <a href="#" class="btn" data-toggle="modal" data-target=".bs-example-modal-sm">
                                <span class="icon">
                                    <i class="fa fa-volume-up" aria-hidden="true">від</i>
                                </span>
                                <span class="counter js-counter" data-from="0" data-to="750" data-speed="2500" data-refresh-interval="50">1</span>
                                <span class="counter-label">При віджимі сильно шумить!</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="gtco-subscribe">
            <div class="gtco-container">
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center gtco-heading">
                        <h2>Залишити заявку</h2>
                        <p>Залиште заявку і ми обов'язково передзвонимо Вам.</p>
                    </div>
                </div>
                <div class="row animate-box">
                    <div class="col-md-12">
                        {!! Form::open(array('route' => 'index.order_from_client', 'class' => 'form-inline')) !!}
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label for="email" class="sr-only">Телефон</label>
                                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Телефон']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <label for="name" class="sr-only">Имя</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Имя']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <button style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;"
                                    type="submit" class="btn btn-default btn-block">Залишити заявку</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        </div>
        @include('layouts.landing_footer_ukr')
    </div>

@endsection
