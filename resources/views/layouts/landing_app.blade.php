<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Animate.css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-countto/1.2.0/jquery.countTo.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ремонт стиральных машин Харьков</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Ремонт стиральных машин Харьков, +Ремонт +стиральных +машин +Харьков +на дому, Мастер по ремонту стиральных машин Харьков" />
    <meta name="keywords" content="ремонт стиральных машин харьков, ремонт стиралок" />
    <meta name="author" content="kharkov-master.com" />
    <meta name="google-site-verification" content="beQ2OHnwODnbEMEFJreZx2J2skBOOULClvTH8lq_mOA"/>
</head>

<body>
<!-- Global site tag (gtag.js) - Google Ads: 715222614 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-715222614"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-715222614');
</script>

    @yield('content')

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
    </div>

    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal_div">
                    <h2>Залиште заявку</h2>

                    {!! Form::open(array('route' => 'index.order_from_client', 'class' => 'form-inline')) !!}
                    <div class="form-group btn-block">
                        <input class="form-control" type="tel" name="phone" placeholder="Телефон - 0503339911"/>
                    </div>
                    <div class="form-group btn-block">
                        <label for="name" class="sr-only">Имя</label>
                        {!! Form::text('name', null, ['class' => 'form-control btn-block', 'placeholder' => 'Имя']) !!}
                    </div>

                    <button
                            type="submit" class="btn btn-block form_btn" onClick="ValidMail(); ValidPhone();" >Оставить заявку</button>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</body>
</html>
