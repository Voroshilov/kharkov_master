<footer id="gtco-footer" role="contentinfo">
    <div class="gtco-container">
        <div class="row row-p	b-md">
            <div class="col-md-8">
                <div class="gtco-widget">
                    <h3>О нас</h3>
                    <p>
                        Быстрый и качественный ремонт стиральных машин и крупной бытовой техники на дому. Охватываем все районы Харькова.
                        Работаем качественно и быстро. Мастера приезжают в день вызова, либо в удобное для Вас время.
                        90% неисправностей устраняются на месте в день приезда мастера. После ремонта мастер обязательно должен проверить при
                        Вас проделанную работу и исправность техники. После проверки мастер выписывает квитанцию о проделанных работах
                        со сроком гарантии. После этого уже можно производить оплату. При ремнонте в несколько этапов оплату авансом
                        мы не используем. Расчет в конце всех проведенных работ.
                    </p>
                    <p>
                        Работаем быстро и качественно, для Вас!
                    </p>
                    <p>
                        Карта сайта: <a href="/map">Карта сайта</a>
                    </p>
                    <p>
                        Рекомендуемые сайты: <a href="http://wash-centre.com">wash-centre.com</a>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="gtco-widget">
                    <h3>Наши контакты</h3>
                    <ul class="gtco-quick-contact">
{{--                        <li><a href="tel:+380997899313"><i class="icon-phone"></i> +38 (099) 789-93-13</a></li>--}}
{{--                        <li><a href="tel:+380960437625"><i class="icon-phone"></i> +38 (096) 043-76-25</a></li>--}}
{{--                        <li><a href="tel:+380502860033"><i class="icon-phone"></i> +38 (050) 286-00-33</a></li>--}}
{{--                        <li><a href="tel:+380688797199"><i class="icon-phone"></i> +38 (068) 879-71-99</a></li>--}}
                        <li><a href="tel:+380957330479"><i class="icon-phone"></i> +38 (095) 733-04-79</a></li>
                        <li><a><i class="icon-mail2"></i> washingworkshop@gmail.com</a></li>
                        <!--<li><a href="#"><i class="icon-chat"></i> Live ChatController</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-md-12">
                {{--<p class="pull-left">--}}
                <small class="block">&copy; 2017 Kharkov Master.</small>
                {{--</p>--}}
                {{--<p class="pull-right">--}}
                <ul class="gtco-social-icons pull-right">
                    <li><a href="https://vk.com/club119165440"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.facebook.com/groups/110806319609363/permalink/110806506276011/?notif_t=group_description_change&notif_id=1502740089038452"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/fixe_washing"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.instagram.com/repair_washing_machines/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li><a href="https://plus.google.com/101378320926041884210"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    {{--<li><a href="https://plus.google.com/101378320926041884210"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>--}}
                </ul>
                {{--</p>--}}
            </div>
        </div>
    </div>
</footer>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125025874-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-125025874-1');
</script>
