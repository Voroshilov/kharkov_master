<footer id="gtco-footer" role="contentinfo">
    <div class="gtco-container">
        <div class="row row-p	b-md">
            <div class="col-md-8">
                <div class="gtco-widget">
                    <h3>Про нас</h3>
                    <p>
                        Швидкий та якісний ремонт пральних машин та великої побутової техніки вдома. Охоплюємо усі райони Харкова.
                        Працюємо якісно та швидко. Майстри приїжджають у день виклику, або у зручний для Вас час.
                        90% несправностей усуваються дома у день приїзду майстра. Після ремонту майстер обов'язково повинен перевірити при
                        Вас виконану роботу та справність техніки. Після перевірки майстер виписує квитанцію про виконані роботи
                        із терміном гарантії. Після цього вже можна робити оплату. При ремонті в кілька етапів оплату авансом
                        ми не використовуємо. Розрахунок наприкінці всіх проведених робіт.
                    </p>
                    <p>
                        Працюємо швидко та якісно, для Вас!
                    </p>
                    <p>
                        Карта сайту: <a href="/map">Карта сайту</a>
                    </p>
                    <p>
                        Рекомендовані сайти: <a href="http://wash-centre.com">wash-centre.com</a>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="gtco-widget">
                    <h3>НАШІ КОНТАКТИ</h3>
                    <ul class="gtco-quick-contact">
                        {{--                        <li><a href="tel:+380997899313"><i class="icon-phone"></i> +38 (099) 789-93-13</a></li>--}}
                        {{--                        <li><a href="tel:+380960437625"><i class="icon-phone"></i> +38 (096) 043-76-25</a></li>--}}
                        {{--                        <li><a href="tel:+380502860033"><i class="icon-phone"></i> +38 (050) 286-00-33</a></li>--}}
                        {{--                        <li><a href="tel:+380688797199"><i class="icon-phone"></i> +38 (068) 879-71-99</a></li>--}}
                        <li><a href="tel:+380957330479"><i class="icon-phone"></i> +38 (095) 733-04-79</a></li>
                        <li><a href="tel:+380957330479"><i class="icon-phone"></i> +38 (095) 733-04-79</a></li>
                        <li><a><i class="icon-mail2"></i> washingworkshop@gmail.com</a></li>
                        <!--<li><a href="#"><i class="icon-chat"></i> Live Chat</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-md-12">
                {{--<p class="pull-left">--}}
                <small class="block">&copy; 2017 Kharkov Master.</small>
                {{--</p>--}}
                {{--<p class="pull-right">--}}
                <ul class="gtco-social-icons pull-right">
                    <li><a href="https://vk.com/club119165440"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.facebook.com/groups/110806319609363/permalink/110806506276011/?notif_t=group_description_change&notif_id=1502740089038452"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="https://twitter.com/fixe_washing"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.instagram.com/repair_washing_machines/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li><a href="https://plus.google.com/101378320926041884210"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    {{--<li><a href="https://plus.google.com/101378320926041884210"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>--}}
                </ul>
                {{--</p>--}}
            </div>
        </div>
    </div>
</footer>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125025874-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-125025874-1');
</script>