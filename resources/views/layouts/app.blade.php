<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
{{--        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">--}}
{{--            <div class="container">--}}
{{--                <a class="navbar-brand" href="{{ url('/') }}">--}}
{{--                    {{ config('app.name', 'Laravel') }}--}}
{{--                </a>--}}
{{--                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">--}}
{{--                    <span class="navbar-toggler-icon"></span>--}}
{{--                </button>--}}

{{--                <div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
{{--                    <!-- Left Side Of Navbar -->--}}
{{--                    <ul class="navbar-nav mr-auto">--}}

{{--                    </ul>--}}

{{--                    <!-- Right Side Of Navbar -->--}}
{{--                    <ul class="navbar-nav ml-auto">--}}
{{--                        <!-- Authentication Links -->--}}
{{--                        @guest--}}
{{--                            @if (Route::has('login'))--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
{{--                                </li>--}}
{{--                            @endif--}}

{{--                            @if (Route::has('register'))--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
{{--                                </li>--}}
{{--                            @endif--}}
{{--                        @else--}}
{{--                            <li class="nav-item dropdown">--}}
{{--                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
{{--                                    {{ Auth::user()->name }}--}}
{{--                                </a>--}}

{{--                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
{{--                                    <a class="dropdown-item" href="{{ route('logout') }}"--}}
{{--                                       onclick="event.preventDefault();--}}
{{--                                                     document.getElementById('logout-form').submit();">--}}
{{--                                        {{ __('Logout') }}--}}
{{--                                    </a>--}}

{{--                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">--}}
{{--                                        @csrf--}}
{{--                                    </form>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        @endguest--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </nav>--}}

{{--        <main class="py-4">--}}
{{--            @yield('content')--}}
{{--        </main>--}}
    </div>
</body>
</html>


<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Animate.css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-countto/1.2.0/jquery.countTo.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ремонт стиральной машины, Ремонт стиральных машин, Ремонт стиральных машин Харьков</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Ремонт стиральных машин Харьков, +Ремонт +стиральных +машин +Харьков +на дому, Мастер по ремонту стиральных машин Харьков" />
    <meta name="keywords" content="ремонт стиральных машин харьков, ремонт стиральных машин, ремонт стиралок" />
    <meta name="author" content="hk-master.in" />
    <meta name="google-site-verification" content="beQ2OHnwODnbEMEFJreZx2J2skBOOULClvTH8lq_mOA"/>
    <!-- Facebook and Twitter integration -->
    {{--<meta property="og:title" content=""/>--}}
    {{--<meta property="og:image" content=""/>--}}
    {{--<meta property="og:url" content=""/>--}}
    {{--<meta property="og:site_name" content=""/>--}}
    {{--<meta property="og:description" content=""/>--}}
    {{--<meta name="twitter:title" content="" />--}}
    {{--<meta name="twitter:image" content="" />--}}
    {{--<meta name="twitter:url" content="" />--}}
    {{--<meta name="twitter:card" content="" />--}}
</head>

<body>
<!-- Global site tag (gtag.js) - Google Ads: 715222614 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-715222614"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-715222614');
</script>

@yield('content')

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">

            {!! Form::open(array('route' => 'index.order_from_client', 'class' => 'form-inline')) !!}
            {{--<div class="form-group">--}}
            {{--<label for="email" class="sr-only">Телефон</label>--}}
            {{--{!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Телефон']) !!}--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
            {{--<label for="name" class="sr-only">Имя</label>--}}
            {{--{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Имя']) !!}--}}
            {{--</div>--}}
            <div class="form-group">
                <input class="form-control" type="tel" name="phone" placeholder="Телефон - 0503339911"/>

            </div>
            <div class="form-group">
                <label for="name" class="sr-only">Имя</label>
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Имя']) !!}
            </div>

            <button style="
                                        background: #f30000;
                                        background: -webkit-linear-gradient(top,#f30000,#aa0d0d) no-repeat;
                                        /*background: linear-gradient(180deg,#f30000 0,#aa0d0d) no-repeat;*/
                                        text-shadow: 0 1px 0 #6c090b;
                                        color: #FFFFFF;"
                    type="submit" class="btn btn-default btn-block" onClick="ValidMail(); ValidPhone();" >Оставить заявку</button>
            {!! Form::close() !!}

        </div>
    </div>
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">

    var user_ip = '<?php echo $_SERVER['REMOTE_ADDR'];?>';

    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter43971614 = new Ya.Metrika({
                    id:43971614,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true,
                    params:{'ip': user_ip}
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");

</script>

<noscript><div><img src="https://mc.yandex.ru/watch/43971614" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<script type="application/ld+json">
        {
          "@context" : "http://schema.org",
          "@type" : "Organization",
          "name" : "HK-Master.in",
          "url" : "http://hk-master.in",
          "sameAs" : [
            "https://vk.com/club119165440",
            "https://www.instagram.com/repair_washing_machines/",
            "https://plus.google.com/101378320926041884210"
          ]
        }
    </script>
<!-- /Yandex.Metrika counter -->
</body>
</html>

