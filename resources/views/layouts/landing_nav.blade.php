<nav class="gtco-nav" role="navigation">
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-12 text-right gtco-contact">
                <ul class="">
{{--                    <li><a href="tel:+380997899313"><i class="ti-mobile"></i> +38 (099) 789-93-13</a></li><br>--}}
{{--                    <li><a href="tel:+380960437625"><i class="ti-mobile"></i> +38 (096) 043-76-25</a></li><br>--}}
{{--                    <li><a href="tel:+380502860033"><i class="ti-mobile"></i> +38 (050) 286-00-33</a></li><br>--}}
{{--                    <li><a href="tel:+380688797199"><i class="ti-mobile"></i> +38 (068) 879-71-99</a></li><br>--}}
                    <li><a href="tel:+380957330479"><i class="icon-phone"></i> +38 (095) 733-04-79</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-xs-12">
                <div id="gtco-logo"><a href="/">Харьков Мастер</a></div>
            </div>
            <div class="col-xs-8 text-right menu-1">
                <ul>
                    <li @if( Request::is('*/')) class="active" @endif><a href="/">Главная</a></li>
                    <li @if( Request::is('*contacts')) class="active" @endif><a href="/contacts">Контакты</a></li>
                    <li @if( Request::is('*blog/posts')) class="active" @endif><a href="/blog/posts">Наш Блог</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>


