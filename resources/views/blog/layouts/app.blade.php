<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="description" content="Free Responsive Html5 Css3 Templates | zerotheme.com">
    <meta name="author" content="http://kharkov-master.com">

    <meta name="keywords" content="Блог Харьков мастер, Харьков мастер, Блог ремонт стиральных машин, полезные статьи ">

    <!-- Mobile Specific Metas
	================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
	================================================== -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/admin/plugins/fontawesome-free/css/all.min.css">

    <link rel="stylesheet" href="/template/fifth/css/zerogrid.css">
    <link rel="stylesheet" href="/template/fifth/css/style.css">

    <!-- Custom Fonts -->
    <link href="/template/fifth/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="/template/fifth/css/menu.css">
    <script src="/template/fifth/js/jquery1111.min.js" type="text/javascript"></script>
    <script src="/template/fifth/js/script.js"></script>

    <!-- Owl Carousel Assets -->
    <link href="/template/fifth/owl-carousel/owl.carousel.css" rel="stylesheet">

    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/Items/ie/home?ocid=ie6_countdown_bannercode">
        <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
    </div>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="/template/fifth/js/html5.js"></script>
    <script src="/template/fifth/js/css3-mediaqueries.js"></script>
    <![endif]-->

</head>

<body class="home-page">
    <div class="wrap-body">
{{--    <div class="wrap-body blur">--}}
        @include('blog.layouts.header')
        <!--////////////////////////////////////Container-->
        @yield('content')
        <!--////////////////////////////////////Footer-->
        @include('blog.layouts.footer')
        <!-- carousel -->
        <script src="/template/fifth/owl-carousel/owl.carousel.js"></script>
        <script>
            $(document).ready(function() {
                $("#owl-slide").owlCarousel({
                    autoPlay: 3000,
                    items : 1,
                    itemsDesktop : [1199,1],
                    itemsDesktopSmall : [979,1],
                    itemsTablet : [768, 1],
                    itemsMobile : [479, 1],
                    navigation: true,
                    navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
                    pagination: false
                });
            });
        </script>
    </div>
</body>
</html>

