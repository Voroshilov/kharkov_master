<footer class="footer">
    <div class="zerogrid wrap-footer">
        <div class="row">
            <div class="col-1-4 col-footer-2">
                <div class="wrap-col">
                    <h3 class="widget-title">Recent Post</h3>
                    <ul>
                        <li><a href="{{route('blog.posts.index')}}">Главная</a></li>
                        <li><a href="{{route('index')}}">Сайт визитка</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-1-4 col-footer-1">
                <div class="wrap-col">
                    <h3 class="widget-title">О нас</h3>
                    <p>Скоро будет</p>

                </div>
            </div>
            <div class="col-1-4 col-footer-3">
                <div class="wrap-col">
                    <h3 class="widget-title">Скоро будет</h3>
                </div>
            </div>
            <div class="col-1-4 col-footer-4">
                <div class="wrap-col">
                    <h3 class="widget-title">Разделы статей</h3>
                    <div class="row">
                        <div class="col-1-12">
                            <div class="wrap-col">
                                <ul>
                                @foreach($categories as $category)
                                    @if($category->parent_id == 0)
                                        <li>
                                            <a href="{{route('blog.posts.by-category', $category->slug)}}">
                                                <span>{{$category->title}}</span>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="zerogrid bottom-footer">
        <div class="row">
            <div class="bottom-social">
                <a href="https://vk.com/club119165440"><i class="fa fa-vk" aria-hidden="true"></i></a>
                <a href="https://www.facebook.com/groups/110806319609363/permalink/110806506276011/?notif_t=group_description_change&notif_id=1502740089038452"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="https://twitter.com/fixe_washing"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="https://www.instagram.com/repair_washing_machines/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="https://plus.google.com/101378320926041884210"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="copyright">
            Copyright © 2017 Kharkov Master.
        </div>
    </div>
</footer>
