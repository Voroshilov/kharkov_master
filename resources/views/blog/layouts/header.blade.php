<header class="header__color">
    <div  class="zerogrid">

    <div id="cssmenu">
        <ul>
            <li class=" ">
                <a href="{{route('index')}}"><span>Главная</span></a>
            </li>
            <li class="{{ Request::is('blog/posts') ? 'active' : ''  }}">
                <a href="{{route('blog.posts.index')}}"><span>Блог</span></a>
            </li>
            <li class="has-sub {{ Request::is('blog/by-category*') ? 'active' : '' }}">
                <a href="{{route('blog.posts.index')}}"><span>Статьи</span></a>
                <ul>
                @foreach($categories as $category)
                    @if($category->parent_id == 0)
                            <li class="has-sub">
                                <a href="{{route('blog.posts.by-category', $category->slug)}}">
                                    <span>{{$category->title}}</span>
                                </a>
                        @if($category->childrenCategory)
                            <ul>
                        @foreach($category->childrenCategory as $child)

                            <li>
                                <a href="{{route('blog.posts.by-category', $child->slug)}}">
                                    <span>{{$child->title}}</span>
                                </a>
                            </li>
                        @endforeach
                            </ul>
                        @endif
                            </li>
                    @endif

                @endforeach
                </ul>
            </li>
{{--            <li class="{{ Request::is('blog/about-us*') ? 'active' : '' }}"><a href="{{route('blog.about-us')}}"><span>О нас</span></a></li>--}}
{{--            <li class="{{ Request::is('blog/contacts*') ? 'active' : '' }}"><a href="{{route('blog.contacts')}}"><span>Контакты</span></a></li>--}}

            @if(Auth::user())
                <li class="last {{ Request::is('blog/home*') ? 'active' : '' }}">
                    <a href="{{route('blog.home')}}"><span>{{Auth::user()->first_name}}</span></a>
                    <ul>
                        <li class="has-sub">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>

                    </ul>
                </li>
            @else
                <li class="last {{ Request::is('login*') ? 'active' : '' }}">
                    <a href="{{route('login')}}"><span>Войти</span></a>
                </li>
            @endif
        </ul>
    </div>
    </div>
</header>


{{--<header class="header">--}}
{{--    <div class="container">--}}
{{--        <div class="header__body">--}}
{{--            <a href="/" class="header__logo">--}}
{{--                <img class="logo" src="/img/logo.png" alt="KM">--}}
{{--            </a>--}}
{{--            <div class="header__burger">--}}
{{--                <span>--}}

{{--                </span>--}}
{{--            </div>--}}
{{--            <nav class="header__menu">--}}
{{--                <ul class="header__list">--}}
{{--                    <li class="header__link"><a href="{{route('blog.posts.index')}}"><span>Главная</span></a></li>--}}
{{--                    <li class="header__link"><a href="{{route('blog.posts.index')}}"><span>Статьи</span></a>--}}
{{--                        <ul>--}}
{{--                            @foreach($categories as $category)--}}
{{--                                @if($category->parent_id == 0)--}}
{{--                                    <li class="has-sub">--}}
{{--                                        <a href="{{route('blog.posts.by-category', $category->slug)}}">--}}
{{--                                            <span>{{$category->title}}</span>--}}
{{--                                        </a>--}}
{{--                                        @if($category->childrenCategory)--}}
{{--                                            <ul>--}}
{{--                                                @foreach($category->childrenCategory as $child)--}}

{{--                                                    <li>--}}
{{--                                                        <a href="{{route('blog.posts.by-category', $child->slug)}}">--}}
{{--                                                            <span>{{$child->title}}</span>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                @endforeach--}}
{{--                                            </ul>--}}
{{--                                        @endif--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                    <li><a href="{{route('blog.about-us')}}" class="header__link">О нас</a></li>--}}
{{--                    <li><a href="{{route('blog.contacts')}}" class="header__link">Контакты</a></li>--}}
{{--                </ul>--}}
{{--            </nav>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</header>--}}
