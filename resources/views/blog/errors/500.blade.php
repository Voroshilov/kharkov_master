@extends('blog.layouts.app')

@section('title', '500')

@section('content')

    <section id="container">
        <div class="wrap-container">
            <!-----------------Content-Box-------------------->
            <div id="main-content">
                <div class="wrap-content">
                    <div class="row">
                        <h3>Такой страници не существует</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection()
