@extends('blog.layouts.app')

@section('title', '404')

@section('content')

    <section id="container">
        <div class="wrap-container">
            <div id="main-content">
                <div class="wrap-content">
                    <div class="row">
                        <h3>Такой страници не существует</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection()
