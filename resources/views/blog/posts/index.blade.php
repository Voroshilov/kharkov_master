@extends('blog.layouts.app')

@section('title', 'Главная')

@section('content')
    <style>
        /*.post-url {*/
        /*    vertical-align: middle;*/
        /*    width: 40px;*/
        /*    !*height: 40px;*!*/
        /*    !*border-radius: 50%;*!*/
        /*}*/
    </style>

<section id="container">
    <div class="wrap-container">
        <section class="content-box box-style-1 box-2">
            <div class="zerogrid">
                <div class="wrap-box"><!--Start Box-->

                    <div class="sub__title">
                        @if($view_category == 'index')
                            <h1>Самые популярные статьи</h1>
                        @elseif(is_object($view_category))
                            <h1>{{$view_category->title}}</h1>
                        @endif
                    </div>
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-1-3">
                                <div class="wrap-col">
                                    <article class="hover11">
{{--                                        <div class="hover11">--}}
                                            <div class=" post-thumbnail-wrap">
                                                <a href="{{route('blog.posts.show', $post->slug)}}"
                                                   class="portfolio-box">

                                                    <img src="{{asset('/uploads_images/posts/mid/' . $post->image)}}" alt="$post->title">
                                                    <div class="portfolio-box-second">
                                                        <img src="{{asset('/uploads_images/posts/mid/' . $post->image)}}" alt="$post->title">
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="entry-header ">
                                                <h3 class="entry-title"><a href="{{route('blog.posts.show', $post->slug)}}">
                                                        {{mb_substr($post->title, 0, 40)}}
                                                    </a></h3>
                                                <div class="l-tags">
                                                    <a href="{{route('blog.posts.show', $post->slug)}}">
                                                        <div class="detail">
                                                            Подробнее
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
{{--                                        </div>--}}
                                    </article>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    @if($posts->total() > $posts->count())
                        <div class="row justify-content-center">
                            <div class="card post-url">
                                <div class="card-body">
                                    {{$posts->links()}}
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </section>
    </div>
</section>

@endsection()
