@extends('blog.layouts.app_post')

@section('title', $post->title)

@section('description')
    <meta name="description" content="{{ $post->description}}">
@endsection()

@section('description')
    <meta name="keywords" content="{{ $post->keywords}}">
@endsection()

@section('content')

    <style>
        body::before{
            content: '';
            position: fixed; /* Фиксируем на одном месте */
            left: 0; right: 0; /* Вся ширин */
            top: 0; bottom: 0; /* Вся высота */
            z-index: -1; /* Фон ниже текста */
            filter: blur(2px);
            background: url(/uploads_images/posts/background/{{$post->image}}), rgb(105, 105, 105);
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        .blur {
             background: rgba(126, 126, 126, 0.34); // Make sure this color has an opacity of less than 1
         backdrop-filter: blur(8px); // This be the blur
         height: 100vh;
             /*width: 50%;*/
         }
    </style>

    <section id="container">
    <div class="wrap-container">
        <!-----------------Content-Box-------------------->
        @php /** @var \App\Models\Post */ @endphp
        <div id="main-content">
            <div class="wrap-content">
                <div class="row">
                    <article class="single-post zerogrid">
                        <div class="row wrap-post" ><!--Start Box-->
                            <div class="entry-header">
                                <h1 class="entry-title">{{$post->title}}</h1>
{{--                                <span class="cat-links"><a href="#">BUSINESS</a>, <a href="#">LIFESTYLE</a></span>--}}
                            </div>

                            <div class="entry-content">
                                {!! $post->text !!}
                                <div class="likes-size" >
                                    <div class="show_views">
                                        {{--                                    <i class="fa fa-eye" aria-hidden="true"></i>--}}
                                        {{$data['views']}}
                                        Просмотров
                                    </div>

                                    <div class="show_likes">
                                        <!--Для авторизированый пользователей-->
                                        @auth()
                                            <div class="one_like">
                                                <form method="POST" action="{{route('blog.posts.set-like')}}">
                                                    @method('POST')
                                                    @csrf
                                                    <input name="is_like" type="hidden" value="1">
                                                    <input name="post_id" type="hidden" value="{{$post->id}}">
                                                    <button type="submit" class="border-0 bg-transparent">
                                                        <i class="fa {{$data['user_like']}}" aria-hidden="false"></i>
                                                        {{$data['likes']}}
                                                    </button>
                                                </form>
                                            </div>

                                            <div class="one_like">
                                                <form method="post" action="{{route('blog.posts.set-like')}}">
                                                    @method('POST')
                                                    @csrf
                                                    <input name="is_like" type="hidden" value="0">
                                                    <input name="post_id" type="hidden" value="{{$post->id}}">
                                                    <button name="is_like" type="submit" class="border-0 bg-transparent">
                                                        <i class="fa {{$data['user_dislike']}}" aria-hidden="false"></i>
                                                        {{$data['dislikes']}}
                                                    </button>
                                                </form>
                                            </div>
                                        @endauth
                                    <!--Для неавторизированных пользователей-->
                                        @guest()
                                            <i class="fa {{$data['user_like']}}" aria-hidden="false"></i>
                                            {{$data['likes']}}
                                            <i class="fa {{$data['user_dislike']}}" aria-hidden="false"></i>
                                            {{$data['dislikes']}}
                                        @endguest
                                    </div>
                                </div>
                            </div>

                        </div>
                    </article>

                    <div class="zerogrid" >
                        <div class="comments-are">
                            <div id="comment">
                                <h3>Оставить коментарий</h3>
                                <form method="POST" action="{{route('blog.posts.set-comment')}}">
                                    @csrf
                                    @method('POST')
                                    <img src="@if(isset($user->avatar))
                                         {{asset($user->avatar)}}
                                         @else
                                            /admin/dist/img/default-150x150.png
                                         @endif"
                                         class="img-circle elevation-2 avatar" alt="User Image">
                                    <input name="post_id" type="hidden" value="{{$post->id}}">
                                    <label>
                                        <span>Коментарий:</span>
                                        <textarea name="comment" id="message"></textarea>
                                    </label>
                                    @if(!$user)
                                        <label>
                                            <span>Имя:</span>
                                            <input type="text"  name="name" id="name" required>
                                        </label>
                                        <label>
                                            <span>Email:</span>
                                            <input type="email"  name="email" id="email" required>
                                        </label>
                                    @endif
                                    <input class="sendButton" type="submit" name="submitcomment" value="Submit">

                                </form>
                            </div>
                        </div>
                        <div class="comments-are">
                            <div id="comment">
                                <h3>Коментарии</h3>
                                <br>
                                @foreach($comments as $comment)
                                    @if($comment->parent_id == 0)
                                        <div>
                                            <div>
                                                <img src="@if(isset($comment->user->avatar))
                                                 {{asset($comment->user->avatar)}}
                                                 @else
                                                    /admin/dist/img/default-150x150.png
                                                 @endif"
                                                     class="img-circle elevation-2 avatar" alt="User Image">
                                                <b>{{$comment->name}}:</b>
                                                <p>{{$comment->comment}}</p>
                                            </div>

                                                @if($comment->childrenComment)
                                                    @foreach($comment->childrenComment as $child)
                                                    @if($child->is_published != 0)
                                                    <div class="test-1">
                                                        <div>
                                                            <img src="@if(isset($child->user->avatar))
                                                         {{asset($child->user->avatar)}}
                                                         @else
                                                            /admin/dist/img/default-150x150.png
                                                         @endif"
                                                                  class="img-circle elevation-2 avatar" alt="User Image">
                                                            <b>{{$child->name}}:</b>
                                                            <p>{{$child->comment}}</p>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                @endif
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
{{--                        <div class="comments-are">--}}
{{--                            <div id="comment">--}}
{{--                                <h3>Оставить коментарий</h3>--}}
{{--                                <form name="comment_form" id="comment_form" method="post" action="">--}}
{{--                                    <img src="@if(isset($user->avatar))--}}
{{--                                                         {{asset($user->avatar)}}--}}
{{--                                                         @else--}}
{{--                                                            /admin/dist/img/default-150x150.png--}}
{{--                                                         @endif"--}}
{{--                                         class="img-circle elevation-2 avatar" alt="User Image">--}}
{{--                                    <label>--}}
{{--                                        <span>Коментарий:</span>--}}
{{--                                        <textarea name="message" id="message"></textarea>--}}
{{--                                    </label>--}}
{{--                                    @if(!$user)--}}
{{--                                    <label>--}}
{{--                                        <span>Имя:</span>--}}
{{--                                        <input type="text"  name="name" id="name" required>--}}
{{--                                    </label>--}}
{{--                                    <label>--}}
{{--                                        <span>Email:</span>--}}
{{--                                        <input type="email"  name="email" id="email" required>--}}
{{--                                    </label>--}}
{{--                                    @endif--}}
{{--                                    <input class="sendButton" type="submit" name="submitcomment" value="Submit">--}}
{{--                                </form>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{--<script>--}}
{{--    $(document).ready(function () {--}}
{{--        $(".click_like").bind("click_like", function () {--}}
{{--            var link = $(this);--}}
{{--            var post_id = link.data({{$post->id}});--}}
{{--            var user_id = link.data({{$user->id}});--}}

{{--            $.ajax({--}}
{{--                url: "/posts/set-like",--}}
{{--                type: "POST",--}}
{{--                data: {post_id:post_id, user_id:user_id},--}}
{{--                typeData: "json"--}}
{{--            })--}}
{{--        } )--}}
{{--    })--}}
{{--</script>--}}

@endsection()
<script>
    import Index from "../../../../public/template/first/index.html";
    export default {
        components: {Index}
    }
</script>
