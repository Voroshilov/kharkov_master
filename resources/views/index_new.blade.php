@extends('layouts.app')

@section('title', 'Wash Centre')

@section('content')

    <main class="page">
        <div class="page__main-block main-block">
            <div class="main-block__container _container">
                <div class="main-block__body">
                    <div class="main-block__card">
                        <h1 class="main-block__title">{{__('index.title_1')}}</h1>
                        <div class="main-block__info" >
                            <div class="button_block">
                                <a type="submit" class="main-block__button_lg-blue" href="#openModal">{{__('index.btn_2')}}</a>
                            </div>
                            <div class="text_block">
                                <ul>
                                    <li>{{__('index.first_li_2')}}</li>
                                    <li>{{__('index.first_li_1')}}</li>
                                    <li>{{__('index.first_li_3')}}</li>
                                    <li>{{__('index.first_li_4')}}</li>
                                    <li>{{__('index.first_li_5')}}</li>
                                    <li>{{__('index.first_li_6')}}</li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="main-block__image _ibg">
                <img src="{{asset('img/2.jpg')}}" alt="cover">
            </div>

            <div class="container">
                <div id="openModal" class="modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title">{{__('Залишити запит')}}</h3>
                                <a href="#close" title="Close" class="close" id="close">×</a>
                            </div>
                            <div class="modal-body">
                                <form action="{{route('response')}}"
                                    id="form_response" name="form_response"
                                    method="POST" autocomplete="off">

                                    @method('POST')
                                    @csrf
                                    <div class="row_form">
                                        <div class="form_title" id="form_title"></div>
                                        <div class="">
                                            <div class="form-group">
                                                @error('name')
                                                <label class="col-form-label" for="inputSuccess">{{ $message }}</label>
                                                @enderror
                                                <input class="form_input @error('name') is-invalid @enderror"
                                                       type="text"
                                                       placeholder="{{__('Имя')}}"
                                                       value="{{ old('name') }}"
                                                       name="name"
                                                       id="name"
                                                       autocomplete="off">
                                            </div>
                                            <div class="form-group">
                                                @error('phone')
                                                <label class="col-form-label" for="inputSuccess">{{ $message }}</label>
                                                @enderror
                                                <input class="form_input @error('phone') is-invalid @enderror"
                                                       type="text"
                                                       placeholder="+38(___)___-__-__"
                                                       value="{{ old('phone') }}"
                                                       name="phone"
                                                       id="phone"
                                                       autocomplete="off">
                                            </div>
                                            <div class="form-group-text">
                                                @error('text')
                                                <label class="col-form-label" for="inputSuccess">{{ $message }}</label>
                                                @enderror
                                                <textarea name="text" id="text"
                                                          class="form_input @error('text') is-invalid @enderror"
                                                >{{ old('text') }}</textarea>
                                            </div>
                                            <div class="button_section-us">
                                                <div class="main-block__buttons">
                                                    <input type="submit" class="main-block__button_mid" value="{{__('Отправить')}}" name="button" id="submit">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>


                            <div class="footer_modal">
                            </div>
                            <div class="empty_place">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="price_section">
            <div class="_container">
                <div class="price-block__body">
                    <h1 class="price-block__title">{{__('index.title_2')}}</h1>
                    <div class="price-block__info" >
                        <div class="text_block_price">
                            <ul class"ul_block">
                                <li><p>{{__('index.price_1')}}</p><span>Бесплатно!!!</span></li>
                                <li><p>{{__('index.price_2')}}</p><span> 200-250 грн</span></li>
                                <li><p>{{__('index.price_3')}}</p> <span>{{__('index.from')}} 400 грн</span> </li>
                                <li><p>{{__('index.price_4')}}</p><span>{{__('index.from')}} 600 грн</span> </li>
                            </ul>
                            <ul class"ul_block">
                                <li><p>{{__('index.price_5')}}</p><span>{{__('index.from')}} 800 грн</span> </li>
                                <li><p>{{__('index.price_6')}}</p><span>{{__('index.from')}} 300 грн</span> </li>
                                <li><p>{{__('index.price_7')}}</p><span>{{__('index.from')}} 400 грн</span> </li>
                                <li><p>{{__('index.price_8')}}</p><span>{{__('index.from')}} 400 грн</span> </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="cards">    
            <div class="_container">
                <div class="cards_block">
                    <div class="card_block">
                        <div class="card_block_item">   
                            <div class="card_img">
                                <img src="{{asset('/img/checkout-min.png')}}" alt="">
                            </div>
                            <div class="card_title">{{__('index.card_title_1')}}</div>
                            <div class="card_text">{{__('index.card_text_1')}}</div>
                        </div> 
                    </div>
                    <div class="card_block">
                        <div class="card_block_item">   
                            <div class="card_img">
                                <img src="{{asset('/img/Clock4-min.png')}}" alt="">
                            </div>
                            <div class="card_title">{{__('index.card_title_2')}}</div>
                            <div class="card_text">{{__('index.card_text_2')}}</div>
                        </div> 
                    </div>
                    <div class="card_block">
                        <div class="card_block_item">   
                            <div class="card_img">
                                <img src="{{asset('/img/man-min.png')}}" alt="">
                            </div>
                            <div class="card_title">{{__('index.card_title_3')}}</div>
                            <div class="card_text">{{__('index.card_text_3')}}</div>
                        </div> 
                    </div>
                    <div class="card_block">
                        <div class="card_block_item">   
                            <div class="card_img">
                                <img src="{{asset('/img/speed-min.png')}}" alt="">
                            </div>
                            <div class="card_title">{{__('index.card_title_4')}}</div>
                            <div class="card_text">{{__('index.card_text_4')}}</div>
                        </div> 
                    </div>
                </div>
            </div>
        </section>

        <section class="map_block">
            <div>
                <!-- <iframe class="footer__google-map_iframe" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Ivansk%C3%A1%20cesta%2030b,%20821%2004%20Ru%C5%BEinov+(Bencor)&amp;t=&amp;z=16&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe> -->
                    <iframe class="footer__google-map_iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5942.59964852504!2d36.30565555892138!3d49.944450263090175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x41270afd5097a09d%3A0x79a903f44ce03406!2z0L_RgNC-0YHQvy4g0JPQtdGA0L7QtdCyINCh0YLQsNC70LjQvdCz0YDQsNC00LAsIDE0NCwg0KXQsNGA0YzQutC-0LIsINCl0LDRgNGM0LrQvtCy0YHQutCw0Y8g0L7QsdC70LDRgdGC0YwsINCj0LrRgNCw0LjQvdCwLCA2MTAwMA!5e0!3m2!1sru!2ssk!4v1683305644962!5m2!1sru!2ssk"></iframe>

                    <!-- <iframe  class="footer__google-map_iframe" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJbefxEcAKJ0ERpF2H06mvCYw&key=..."></iframe> -->
            </div>
        </section>

    </main>

@endsection()

