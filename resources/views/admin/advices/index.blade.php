@extends('admin.layouts.app')

@section('title', 'Advices')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Советы</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Советы</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                @if(session('success'))
                    <div class="form-group">
                        <label class="col-form-label" for="inputSuccess">{{session()->get('success')}}</label>
                    </div>
                @endif
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">#</th>
                            <th style="width: 15%">Заголовок и дата</th>
                            <th style="width: 20%">Категория</th>
                            <th style="width: 15%">Фото</th>
                            <th style="width: 10%">Создатель</th>
{{--                            <th>--}}
{{--                                Project Progress--}}
{{--                            </th>--}}
                            <th style="width: 8%" class="text-center">
                                Статус
                            </th>
                            <th style="width: 10%">
                                <a class="btn btn-primary btn-sm" href="{{route('admin.advices.create')}}">
                                    Новый совет
                                </a>
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($advices as $advice)
                            <tr>
                                <td>{{$advice->id}}</td>
                                <td>
                                    <a>{{$advice->title}}</a>
                                    <br/>
                                    <small>
                                        Создана - {{date_format($advice->created_at, 'd.m.y')}}
                                    </small>
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            Пока так...
                                        </li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            <img alt="Avatar" class="table-avatar" src="{{asset($advice->image)}}">
                                        </li>
                                    </ul>
                                </td>
                                <td>(Кто создавал)</td>
    {{--                            <td class="project_progress">--}}
    {{--                                <div class="progress progress-sm">--}}
    {{--                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                                <small>--}}
    {{--                                    57% Complete--}}
    {{--                                </small>--}}
    {{--                            </td>--}}
                                <td class="project-state">
                                    @if($advice->is_active == 1)
                                        <span class="badge badge-success">Активна</span>
                                    @else
                                        <span class="badge badge-lose">Не активна</span>
                                    @endif
                                </td>
                                <td class="project-actions text-right">
                                    <a class="btn btn-primary btn-sm" href="{{route('admin.advices.show', $advice->id)}}">
                                        <i class="fas fa-folder">
                                        </i>
                                        View
                                    </a>
                                    <a class="btn btn-info btn-sm" href="{{route('admin.advices.edit', $advice->id)}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Edit
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="{{route('admin.advices.destroy', $advice->id)}}">
                                        <i class="fas fa-trash">
                                        </i>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>

@endsection()
