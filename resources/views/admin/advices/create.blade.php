@extends('admin.layouts.app_create_template')

@section('title', 'Advices create')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Новый Совет</h1>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Создание статьи</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">


                            <div class="card-body">
                                @php /** @var \App\Models\AdviceCategory */ @endphp
                                <form method="POST" action="{{route('admin.advices.store')}}">
                                    @method('POST')
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Заголовок
                                                    @error('title') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="title"
                                                       class="form-control @error('title') is-invalid @enderror"
                                                       placeholder="Заголовок ...">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">Ключевые слова</label>
                                                <input type="text" name="keywords" class="form-control" placeholder="Заголовок ...">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">Краткое описание</label>
                                                <input type="text" name="short_text" class="form-control" placeholder="Заголовок ...">
                                            </div>
                                            <!-- checkbox -->
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="is_active" value="1" checked>
                                                    <label class="form-check-label">Опубликовать</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- select -->
                                            <div class="form-group">
                                                <label>Выбрать категорию</label>
                                                <select class="form-control" name="category_id">
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Выбрать Производителя техники</label>
                                                <select class="form-control" name="manufacturer_id">
                                                    @foreach($manufacturers as $manufacturer)
                                                        <option value="{{$manufacturer->id}}">{{$manufacturer->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body">
                                        <div class="form-group">
                                            @error('text')
                                            <h3 style="color: red">
                                                Нужно заполнить Текстовое поле!!!
                                            </h3>
                                            @enderror
                                            <textarea id="compose-textarea" name="text"
                                                      class="form-control "
                                                      style="height: 350px"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="btn btn-default btn-file">
                                                <i class="fas fa-paperclip"></i> Закгрузить фото
                                                <input type="file" name="attachment">
                                            </div>
                                            <p class="help-block">Max. 32MB</p>
                                        </div>
                                    </div>

                                    <input type="submit" value="Сохранить">
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="{{route('admin.advices.index')}}" class="btn btn-primary btn-block mb-3">Все статьи</a>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content -->
</div>
@endsection()
