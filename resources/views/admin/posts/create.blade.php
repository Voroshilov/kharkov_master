@extends('admin.layouts.app_textarea_template')

@section('title', 'Созадание статьи')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Новая статья</h1>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Создание Поста</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">


                            <div class="card-body">
                                @php /** @var \App\Models\Post */ @endphp
                                <form method="POST" action="{{route('admin.posts.store')}}" enctype="multipart/form-data">
                                    @method('POST')
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Заголовок
                                                    @error('title') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="title"
                                                       minlength="5"
                                                       value="{{ old('title') }}"
                                                       class="form-control @error('title') is-invalid @enderror"
                                                       placeholder="Заголовок ...">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">Ключевые слова</label>
                                                <input type="text" name="keywords" class="form-control"
                                                       value="{{ old('keywords') }}" placeholder="Заголовок ...">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">Краткое описание</label>
                                                <input type="text" name="short_text" class="form-control"
                                                       value="{{ old('short_text') }}" placeholder="Заголовок ...">
                                            </div>
                                            <!-- checkbox -->
{{--                                            <div class="card card-secondary">--}}
{{--                                                <div class="card-header">--}}
{{--                                                    <h3 class="card-title">Bootstrap Switch</h3>--}}
{{--                                                </div>--}}
{{--                                                <div class="card-body">--}}
{{--                                                    <input type="checkbox" name="is_published" value="1" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="form-group">--}}
{{--                                                <div class="form-check">--}}
{{--                                                    <input type="hidden" name="is_published" value="0">--}}
{{--                                                    <input class="form-check-input" type="checkbox" name="is_published" value="1" checked>--}}
{{--                                                    <label class="form-check-label">Опубликовать</label>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- select -->
                                            <div class="form-group">
                                                <label>Выбрать категорию</label>
                                                <select class="form-control" name="category_id">
                                                    <option value="">... Категория не выбрана ...</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Выбрать Производителя техники</label>
                                                <select class="form-control" name="manufacturer_id">
                                                    @foreach($manufacturers as $manufacturer)
                                                        <option value="{{$manufacturer->id}}">{{$manufacturer->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body">
                                        <div class="form-group">
                                            @error('text')
                                            <h3 style="color: red">
                                                Нужно заполнить Текстовое поле!!!
                                            </h3>
                                            @enderror
                                            <textarea name="text"
                                                      minlength="10">
                                            {!!  old('text') !!}
                                            </textarea>
                                            <script>
                                                CKEDITOR.replace( 'text' );
                                            </script>

{{--                                            <textarea id="summernote"--}}
{{--                                                      minlength="10"--}}
{{--                                                      name="text">--}}
{{--                                            </textarea>--}}
{{--                                            <textarea id="compose-textarea"--}}
{{--                                                      minlength="10"--}}
{{--                                                      name="text"--}}
{{--                                                      class="form-control "--}}
{{--                                                      style="height: 350px"></textarea>--}}

                                        </div>
                                        <div class="form-group">
{{--                                            <div class="btn btn-default btn-file">--}}
{{--                                                <i class="fas fa-paperclip"></i> Загрузить фото--}}
{{--                                                <input type="file" name="attachment">--}}
{{--                                            </div>--}}
                                            <div class="btn btn-default btn-file">
                                                <i class="fas fa-paperclip"></i> Закгрузить фото
                                                <input type="file" id="file" name="image" class="form-control">
                                            </div>
                                            <p class="help-block">Max. 32MB</p>
                                        </div>
                                    </div>

                                 <div class="form-group">
                                     <div class="form-check">
                                         <input type="hidden" name="is_published" value="0">
                                         <input class="form-check-input" type="checkbox" name="is_published" value="1" checked>
                                         <label class="form-check-label">Опубликовать</label>
                                     </div>
                                 </div>
{{--                                    <div class="form-group">--}}
{{--                                        <div class="form-check">--}}
{{--                                            <div class="card-body">--}}
{{--                                                <input type="hidden" name="is_published" value="0">--}}
{{--                                                <input type="checkbox" name="is_published" value="1"--}}
{{--                                                       data-bootstrap-switch data-off-color="danger" data-on-color="success">--}}
{{--                                                <label class="form-check-label">Опубликовать</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <input class="btn btn-primary" type="submit" value="Сохранить">
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="{{route('admin.posts.index')}}" class="btn btn-primary btn-block mb-3">Все статьи</a>

                        <h4>Загрузить фото</h4>
                        <hr>
                        <form id="upload_file"
                              name="upload_file"
                              method="POST"
                              autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
{{--                            <input id="file" type="file" name="file" class="attachment-name">--}}
{{--                            <input class="btn btn-primary btn-block mb-3" type="submit" name="button" value="Загруить" id="submit">--}}

                            <label class="custom-file-input" for="file_url" ></label>

                            <input id="file_url" type="file" multiple="multiple" name="file_url">

                            <input type="submit" class="button" value="Загрузить" name="button" id="submit">
                        </form>

                        <div class="row">
                            <span id="output"></span>
                        </div>
                    </div>

{{--                    <div class="row">--}}
{{--                        <span id="output"></span>--}}
{{--                    </div>--}}
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script>

        $('#upload_file').on('submit',function(event){
            event.preventDefault();

            var formData = new FormData();
            formData.append('file_url', $("#file_url")[0].files[0]);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/posts/upload-file",
                type:"POST",
                contentType: false,
                processData: false,
                data:formData,
                success:function(response){
                   let text = document.getElementById('output')
                    text.innerHTML = '<img id="' + Math.random() + '" src="'+ response +'" width="100%" height="100%"/><span>' + response + '</span>'
                    // console.log(response);
                },
            });
        });
    </script>
{{--    <script>--}}

{{--        $('#form_response_vacancy').on('submit',function(event){--}}
{{--            event.preventDefault();--}}

{{--            var formData = new FormData();--}}
{{--            formData.append('photo', $("#photo")[0].files[0]);--}}

{{--            $.ajaxSetup({--}}
{{--                headers: {--}}
{{--                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--                }--}}
{{--            });--}}
{{--            $.ajax({--}}
{{--                url: "/response/vacancy-response",--}}
{{--                type:"POST",--}}
{{--                contentType: false,--}}
{{--                processData: false,--}}
{{--                // data:formData,--}}
{{--                data:{--}}
{{--                    "_token": "{{ csrf_token() }}",--}}
{{--                    vacancy_id:vacancy_id,--}}
{{--                    is_consent:is_consent,--}}
{{--                    name:name,--}}
{{--                    last_name:last_name,--}}
{{--                    email:email,--}}
{{--                    phone:phone,--}}
{{--                    text:text,--}}
{{--                    // formData:formData,--}}
{{--                    // file:file,--}}
{{--                },--}}
{{--                success:function(response){--}}
{{--                    console.log(response);--}}
{{--                },--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}

@endsection()
