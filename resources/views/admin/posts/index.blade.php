@extends('admin.layouts.app')

@section('title', 'Posts')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Все статьи</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Все статьи</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                @if(session('success'))
                    <div class="form-group alert alert-success">
                        <label class="col-form-label" for="inputSuccess">{{session()->get('success')}}</label>
                    </div>
                @endif
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <form method="GET" action="{{route('admin.posts.search')}}">
                                @method('GET')
                                @csrf

                                <th style="width: 1%">#</th>
                                <th style="width: 15%">
                                    <input type="text"
                                           name="search_title"
                                           id="search_title"
                                           class="form-control"
                                           placeholder="Введи незвание ..."
                                           value="@if(isset($_GET["search_title"])){{$_GET["search_title"]}}@endif"
                                    >
                                </th>
                                <th style="width: 20%">
                                    <select class="form-control"
                                            name="search_category_id"
                                            id="search_category_id">
                                        <option value="">... Категория не выбрана ...</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}"

                                                    @if(isset($_GET["search_category_id"]) && $category->id == $_GET["search_category_id"])
                                                        selected
                                                @endif
                                            >
                                                {{$category->title}}
                                            </option>
                                        @endforeach
                                    </select>
                                </th>
                                <th style="width: 15%"></th>
                                <th style="width: 10%"></th>
                                <th style="width: 8%" class="text-center"></th>
                                <th style="width: 10%">
                                    <input class="btn btn-primary" type="submit" value="Искать">
                                </th>
                            </form>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Заголовок и дата</th>
                            <th>Категория</th>
                            <th>Фото</th>
                            <th>Создатель</th>
                            <th>
                                Статус
                            </th>
                            <th>
                                <a class="btn btn-primary btn-sm" href="{{route('admin.posts.create')}}">
                                    Новая статья
                                </a>
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($posts as $post)
                            <tr>
                                <td>{{$post->id}}</td>
                                <td>
                                    <a>{{Str::limit($post->title, 60)}}</a>
                                    <br/>
                                    <small>
                                        Создана - {{date_format(new DateTime($post->created_at), 'd-m-y')}}
                                    </small>
                                    <br>
                                    <small>
{{--                                        Изменена - {{$post->updated_at->format('d-m-y')}}--}}
                                        Изменена - {{date_format(new DateTime($post->updated_at), 'd-m-y')}}
                                    </small>
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            {{$post->category->title}}
                                        </li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
{{--                                            <img alt="Avatar" class="table-avatar" src="{{asset($post->image)}}">--}}
                                            <img src="{{asset('/uploads_images/posts/logo/' . $post->image)}}"
                                                 alt="{{Str::limit($post->title, 30)}}" class="img-thumbnail">
                                        </li>
                                    </ul>
                                </td>
                                <td>{{$post->creator->name}}</td>
    {{--                            <td class="project_progress">--}}
    {{--                                <div class="progress progress-sm">--}}
    {{--                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                                <small>--}}
    {{--                                    57% Complete--}}
    {{--                                </small>--}}
    {{--                            </td>--}}
                                <td class="project-state">
                                    @if($post->is_published == 1)
                                        <span class="badge badge-success">Опубликована</span>
                                    @else
                                        <span class="badge badge-lose">Не Опубликована</span>
                                    @endif
                                </td>
                                <td class="project-actions text-right">
                                    <a class="btn btn-primary btn-sm" href="{{route('admin.posts.show', $post->id)}}">
                                        <i class="fas fa-folder">
                                        </i>
                                        Просмотр
                                    </a>
                                    <a class="btn btn-info btn-sm" href="{{route('admin.posts.edit', $post->id)}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Редактор
                                    </a>
                                    <button class="btn btn-danger btn-sm"
                                            data-href="{{route('admin.posts.destroy', $post->id)}}"
                                            data-toggle="modal" data-target="#confirm-delete-{{$post->id}}">
                                        <i class="fas fa-trash"></i>Удалить
                                    </button>

                                    <form method="POST" action="{{route('admin.posts.destroy', $post->id)}}">
                                        @method('DELETE')
                                        @csrf
                                        <div class="modal fade" id="confirm-delete-{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        Подтверждение
                                                    </div>
                                                    <div class="modal-body">
                                                        Действительно хотите удалить запись: {{$post->title}} ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                                                        <button type="submit"  class="btn btn-danger btn-sm">
                                                            <i class="fas fa-trash"></i>
                                                            Удалить
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </section>
        <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
                {{--                {{$posts->links()}}--}}
                <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">{{$posts->currentPage()}}</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
            </ul>
        </div>

        @if($posts->total() > $posts->count())

            <div class="row justify-content-center">
                <div class="card">
                    <div class="card-body">
                        {{$posts->links()}}
                    </div>
                </div>
            </div>

        @endif
        <!-- /.content -->
    </div>

    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    </script>

@endsection()
