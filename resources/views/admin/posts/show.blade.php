@extends('admin.layouts.app')

@section('title', "Статья - {{$post->title}}")

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1><b>Заголовок -</b> {{$post->title}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">{{$post->title}}</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-md-9">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title"></h3>
                                <i class="fa fa-eye" aria-hidden="true"></i>
                                {{$data['views']}}
                                <i class="fa fa-thumbs-o-up" aria-hidden="true">Лайк</i>
                                {{$data['likes']}}
                                <i class="fa fa-thumbs-o-down" aria-hidden="false">Дизлайк</i>
                                {{$data['dislikes']}}
<!--                                Можно будет использовать что бы листать посты-->

{{--                                <div class="card-tools">--}}
{{--                                    <a href="#" class="btn btn-tool" title="Previous"><i class="fas fa-chevron-left"></i></a>--}}
{{--                                    <a href="#" class="btn btn-tool" title="Next"><i class="fas fa-chevron-right"></i></a>--}}
{{--                                </div>--}}
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0">
                                <!-- /.mailbox-controls -->
                                <div class="mailbox-read-message">

                                    <b>Текст:</b>
                                    <br>

                                    {!! $post->text !!}

                                </div>
                                <!-- /.mailbox-read-message -->
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>


                    <div class="col-md-3">
                        <a href="{{route('admin.posts.create')}}" class="btn btn-primary btn-block mb-3">Создать новую статью</a>
                        <a href="{{route('admin.posts.index')}}" class="btn btn-primary btn-block mb-3">Все статьи</a>
                        <a href="{{route('admin.posts.edit', $post->id)}}" class="btn btn-success btn-block mb-3">Редактировать</a>
                        <form method="POST" action="{{route('admin.posts.destroy', $post->id)}}">
                            @method('DELETE')
                            @csrf
                            <button type="submit"  class="btn btn-danger btn-block mb-3">Удалить</button>
                        </form>
{{--                        <a href="{{route('admin.posts.destroy', $post->id)}}" class="btn btn-danger btn-block mb-3">Удалить</a>--}}

{{--                        <a class="btn btn-danger btn-sm" href="{{route('admin.posts.destroy', $post->id)}}">--}}
{{--                            <i class="fas fa-trash">--}}
{{--                            </i>--}}
{{--                            Delete--}}
{{--                        </a>--}}

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Категория</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item active">
                                        <a href="#" class="nav-link">
                                            <i class="fas fa-inbox"></i>
                                            {{$post->category->title}}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Производитель техники</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item active">
                                        <a href="#" class="nav-link">
                                            <i class="fas fa-inbox"></i>

                                            {{$post->manufacturer->title}}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Создатель</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item active">
                                        <a href="#" class="nav-link">
                                            <i class="fas fa-inbox"></i>

                                            {{$post->creator->name}}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Кто редактировал</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item active">
                                        <a href="#" class="nav-link">
                                            <i class="fas fa-inbox"></i>

                                            {{$post->editor->name}}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <div class="form-group">
                        <img src="{{asset('/uploads_images/posts/small/' . $post->image)}}"
                             alt="{{$post->title}}" class="img-thumbnail">
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection()
