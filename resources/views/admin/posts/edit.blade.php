@extends('admin.layouts.app_textarea_template')

@section('title', 'Редактирование статьи')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Редактируем Статью</h1>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('admin.posts.index')}}">Все статьи</a></li>
                            <li class="breadcrumb-item active">{{Str::limit($post->title, 30)}}</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">
                            <div class="card-body">
                                @php /** @var \App\Models\PostCategory */ @endphp
                                <form method="POST"
                                      action="{{route('admin.posts.update', $post->id)}}"
                                      enctype="multipart/form-data">
                                    @method('PATCH')
                                    @csrf

                                    <div class="row">
                                        <div class="col-sm-6">
                                            @php
                                            /** @var \Illuminate\Support\ViewErrorBag $errors */
                                            @endphp
                                            @if($errors->any())
                                                <div class="form-group">
                                                    <label class="col-form-label" for="inputError">{{$errors->first()}}</label>
                                                </div>
                                            @endif

                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Заголовок
                                                    @error('title') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="title"
                                                       minlength="3"
                                                       value="{{$post->title}}"
                                                       class="form-control @error('title') is-invalid @enderror"
                                                       placeholder="Заголовок ...">
                                            </div>

                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">Краткое описание</label>
                                                <input type="text" name="short_text" value="{{$post->short_text}}"
                                                       class="form-control is-valid" id="inputSuccess" placeholder="Заголовок ...">
                                            </div>
                                            <!-- checkbox -->
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input type="hidden" name="is_published" value="0">
                                                    <input class="form-check-input"
                                                           type="checkbox" name="is_published" value="1"
                                                           @if($post->is_published)
                                                               checked="checked"
                                                           @endif
                                                    >
{{--                                                    @if($post->is_published == 0)--}}
{{--                                                        <input class="form-check-input" type="checkbox" name="is_published" value="1">--}}
{{--                                                    @else--}}
{{--                                                        <input class="form-check-input" type="checkbox" name="is_published" value="1" checked>--}}
{{--                                                    @endif--}}
                                                    <label class="form-check-label">Опубликовать</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- select -->
                                            <div class="form-group">
                                                <label>Выбрать категорию</label>
                                                {{-- Решить с подкатегориями--}}
                                                <select class="form-control" name="category_id" >

                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}"

                                                        @if($category->id == $post->category_id)
                                                            selected
                                                        @endif
                                                        >
                                                            {{$category->title}}
                                                        </option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Выбрать категорию</label>
                                                <select class="form-control" name="manufacturer_id" >

                                                    @foreach($manufacturers as $manufacturer)

                                                        <option value="{{$manufacturer->id}}"

                                                        @if($manufacturer->id == $post->manufacturer_id)
                                                            selected
                                                        @endif
                                                        >
                                                            {{$manufacturer->title}}

                                                        </option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body">
                                        <div class="form-group">
                                            @error('text')
                                            <h3 style="color: red">
                                                Нужно заполнить Текстовое поле!!!
                                            </h3>
                                            @enderror
{{--                                            <textarea id="compose-textarea" name="text"--}}
{{--                                                      minlength="10" class="form-control" style="height: 350px">--}}
{{--                                                {!!  old('text', $post->text) !!}--}}
{{--                                            </textarea>--}}
{{--                                            <textarea id="summernote" name="text"--}}
{{--                                                      minlength="10" class="form-control" style="height: 350px">--}}
{{--                                                {!!  old('text', $post->text) !!}--}}
{{--                                            </textarea>--}}

                                            <textarea name="text"
                                                      minlength="10">
                                                {!!  old('text', $post->text) !!}
                                            </textarea>
                                            <script>
                                                CKEDITOR.replace( 'text' );
                                            </script>

                                        </div>
{{--                                        <div class="form-group">--}}
{{--                                            <label class="col-form-label" for="inputSuccess">--}}
{{--                                                Загрузка картинки--}}
{{--                                            </label>--}}
{{--                                            <br>--}}
{{--                                            <div class="col-md-6">--}}
{{--                                                <input type="file" id="file" name="image" class="form-control">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="form-group">
                                            <div class="btn btn-default btn-file">
                                                <i class="fas fa-paperclip"></i> Закгрузить фото
                                                <input type="file" id="file" name="image" class="form-control">
                                            </div>
                                            <p class="help-block">Max. 2MB</p>
                                        </div>
                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Сохранить">
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="{{route('admin.posts.index')}}" class="btn btn-primary btn-block mb-3">Все статьи</a>
                        <a href="{{route('admin.posts.show', $post->id)}}" class="btn btn-success btn-block mb-3">Просмотреть</a>
                        <form method="POST" action="{{route('admin.posts.destroy', $post->id)}}">
                            @method('DELETE')
                            @csrf
                            <button type="submit"  class="btn btn-danger btn-block mb-3">Удалить</button>
                        </form>
                        <div class="form-group">
                            <img src="{{asset('/uploads_images/posts/small/' . $post->image)}}"
                                 alt="{{$post->title}}" class="img-thumbnail">
                        </div>

                        <div class="row">
                            <span id="output"></span>
                        </div>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content -->
    </div>

    @include('admin.layouts.includes.preview_image')

@endsection()
