@extends('admin.layouts.app')

@section('title', 'Advices')

@section('content')
    <style>
        .avatar {
            vertical-align: middle;
            width: 50px;
            height: 50px;
            border-radius: 50%;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Все статьи</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Сотрудники</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                @if(session('success'))
                    <div class="form-group alert alert-success">
                        <label class="col-form-label" for="inputSuccess">{{session()->get('success')}}</label>
                    </div>
                @endif
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <form method="GET" action="{{route('admin.staffs.search')}}">
                                @method('GET')
                                @csrf

                                <th style="width: 10%">
                                    <input class="btn btn-primary" type="submit" value="Искать">
                                </th>
                                <th style="width: 15%"></th>
                                <th style="width: 20%">
                                    <input type="text"
                                           minlength="2"
                                           name="search_phone"
                                           id="search_phone"
                                           class="form-control"
                                           placeholder="Номер ..."
                                           value="@if(isset($_GET["search_phone"])){{$_GET["search_phone"]}}@endif"
                                    >
                                </th>
                                <th style="width: 25%">
                                    <input type="text"
                                           minlength="2"
                                           name="search_address"
                                           id="search_address"
                                           class="form-control"
                                           placeholder="Адрес ..."
                                           value="@if(isset($_GET["search_address"])){{$_GET["search_address"]}}@endif"
                                    >
                                </th>
                                <th style="width: 10%"></th>
                                <th style="width: 10%"></th>
                                <th style="width: 10%"></th>
                            </form>
                        </tr>
                        <tr>
                            <th>
                                <a class="btn btn-primary btn-sm" href="{{route('admin.staffs.create')}}">
                                    Новый
                                </a>
                            </th>
                            <th>Время</th>
                            <th>Номер</th>
                            <th>Адресс</th>
                            <th>Навыки</th>
                            <th>Должность</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($users as $user)
                            <tr>
                                <td>
                                    <a class="btn" href="{{route('admin.staffs.show', $user->id)}}">
                                        <img src="{{asset($user->avatar)}}"
                                             class="img-circle elevation-2 avatar" alt="User Image">
                                    </a>
                                </td>
                                <td>
                                    <p>{{$user->first_name}}</p>
                                    <p>{{$user->second_name}}</p>

                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            <a href="tel:{{$user->phone}}">{{$user->phone}}</a>
                                        </li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            {{$user->address}}
                                        </li>
                                    </ul>
                                </td>
                                <td></td>
                                <td>
                                    <p>{{$user->title}}</p>
                                </td>
                                <td class="project-actions text-right">
                                    <a class="btn btn-primary btn-sm" href="{{route('admin.staffs.show', $user->id)}}">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <br>
                                    <a class="btn btn-info btn-sm" href="{{route('admin.staffs.edit', $user->id)}}">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </section>

        @if($users->total() > $users->count())
            <div class="row justify-content-center">
                <div class="card">
                    <div class="card-body">
                        {{$users->links()}}
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection()
