@extends('admin.layouts.app_create_template')

@section('title', 'Созадание сотрудника')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Новый сотрудник</h1>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Новый сотрудник</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">


                            <div class="card-body">
                                @php /** @var \App\Models\User */ @endphp
                                <form method="POST" action="{{route('admin.staffs.store')}}">
                                    @method('POST')
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Имя
                                                    @error('first_name') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="first_name"
                                                       minlength="2"
                                                       value="{{ old('first_name') }}"
                                                       class="form-control @error('first_name') is-invalid @enderror"
                                                       placeholder="Имя ...">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Фамилия
                                                    @error('second_name') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="second_name"
                                                       minlength="2"
                                                       value="{{ old('second_name') }}"
                                                       class="form-control @error('second_name') is-invalid @enderror"
                                                       placeholder="Фамилия ...">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Телефон
                                                    @error('phone') Номер не менее 7 символов @enderror
                                                </label>
                                                <input type="number" name="phone"
                                                       minlength="2"
                                                       id="phone"
                                                       value="{{ old('phone') }}"
                                                       class="form-control @error('phone') is-invalid @enderror"
                                                       {{--  placeholder="Телефон ..."--}}
                                                       data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    E-mail
                                                    @error('email') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="email"
                                                       minlength="5"
                                                       value="{{ old('email') }}"
                                                       class="form-control @error('email') is-invalid @enderror"
                                                       placeholder="Адресс ...">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Адресс
                                                    @error('address') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="address"
                                                       minlength="5"
                                                       value="{{ old('address') }}"
                                                       class="form-control @error('address') is-invalid @enderror"
                                                       placeholder="Адресс ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- select -->
                                            <div class="form-group">
                                                <label>Выбрать Роль</label>
                                                <select class="form-control" name="role_id">
                                                    <option value="">... Роль не выбрана ...</option>
                                                    @foreach($roles as $role)
                                                        <option value="{{$role->id}}">{{$role->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group"><div class="form-group">
                                                    <label>Multiple</label>
                                                    <select class="select2" name="permission_id[]" multiple="multiple" data-placeholder="Select a State" style="width: 100%;">
                                                        @foreach($permissions as $permission)
                                                            <option value="{{$permission->id}}">{{$permission->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Сохранить">
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="{{route('admin.staffs.index')}}" class="btn btn-primary btn-block mb-3">Все работники</a>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

    <script>
        //Код jQuery, установливающий маску для ввода телефона элементу input
        //1. После загрузки страницы,  когда все элементы будут доступны выполнить...
        $(function(){
            //2. Получить элемент, к которому необходимо добавить маску
            $("#phone").mask("(999) 999-9999");
        });

    </script>
@endsection()
