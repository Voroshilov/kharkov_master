@extends('admin.layouts.app')

@section('title', 'Posts')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Все статьи</h1>
                        <form method="GET" action="{{route('admin.post_categories.search')}}">
                            @method('GET')
                            @csrf
                            <div class="form-group">
                                <input type="text"
                                       name="search"
                                       id="search"
                                       minlength="2"
                                       class="form-control"
                                       placeholder="Введи незвание ..."
                                       value="@if(isset($_GET["search"])){{$_GET["search"]}}@endif"
                                >
                            </div>
                            <input class="btn btn-primary" type="submit" value="Искать">
                        </form>
                    </div>
                    <div class="col-sm-6">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Все категории статей</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                @if(session('success'))
                    <div class="form-group alert alert-success">
                        <label class="col-form-label" for="inputSuccess">{{session()->get('success')}}</label>
                    </div>
                @endif
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">#</th>
                            <th style="width: 15%">Название</th>
                            <th style="width: 10%">К какой категории относиться</th>
                            {{--                            <th>--}}
                            {{--                                Project Progress--}}
                            {{--                            </th>--}}
                            <th style="width: 8%" class="text-center">
                                Статус
                            </th>
                            <th style="width: 10%">
                                <a class="btn btn-primary btn-sm" href="{{route('admin.post_categories.create')}}">
                                    Новая категория
                                </a>
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($post_categories as $pc)
                            <tr>
                                <td>{{$pc->id}}</td>
                                <td>
                                    {{$pc->title}}
                                </td>
                                <td>
{{--                                    Нужно будет переделать--}}
                                    @if($pc->parent_id != 0)
                                        {{$pc->parentTitle}}
{{--                                        {{$post_categories[$pc->parent_id - 1]->title}}--}}
                                    @else
                                        Главная категория
                                    @endif
                                </td>
                                <td class="project-state">
                                    @if($pc->is_active == 1)
                                        <span class="badge badge-success">Активна</span>
                                    @else
                                        <span class="badge badge-lose">Не активна</span>
                                    @endif
                                </td>
                                <td class="project-actions text-right">
                                    <a class="btn btn-primary btn-sm" href="{{route('admin.post_categories.show', $pc->id)}}">
                                        <i class="fas fa-folder">
                                        </i>
                                        Просмотр
                                    </a>
                                    <a class="btn btn-info btn-sm" href="{{route('admin.post_categories.edit', $pc->id)}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Редактор
                                    </a>
{{--                                    <form method="POST" action="{{route('admin.post_categories.destroy', $pc->id)}}">--}}
{{--                                        @method('DELETE')--}}
{{--                                        @csrf--}}
{{--                                        <button type="submit"  class="btn btn-danger btn-sm">--}}
{{--                                            <i class="fas fa-trash"></i>--}}
{{--                                            Удалить--}}
{{--                                        </button>--}}
{{--                                    </form>--}}

                                    <button class="btn btn-danger btn-sm"
                                            data-href="{{route('admin.post_categories.destroy', $pc->id)}}"
                                            data-toggle="modal" data-target="#confirm-delete-{{$pc->id}}">
                                        <i class="fas fa-trash"></i>Удалить
                                    </button>

                                    <form method="POST" action="{{route('admin.post_categories.destroy', $pc->id)}}">
                                        @method('DELETE')
                                        @csrf
                                        <div class="modal fade" id="confirm-delete-{{$pc->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        Подтверждение
                                                    </div>
                                                    <div class="modal-body">
                                                        Действительно хотите удалить запись: {{$pc->title}} ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                                                        <button type="submit"  class="btn btn-danger btn-sm">
                                                            <i class="fas fa-trash"></i>
                                                            Удалить
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>

@endsection()
