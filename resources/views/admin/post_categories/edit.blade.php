@extends('admin.layouts.app_create_template')

@section('title', 'Редактирование категории')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Редактируем категорию</h1>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Редактируем Категорию</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">
                            <div class="card-body">
                                @php /** @var \App\Models\PostCategory */ @endphp
                                <form method="POST" action="{{route('admin.post_categories.update', $category->id)}}">
                                    @method('PATCH')
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Название категории
                                                    @error('title') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="title"
                                                       value="{{$category->title}}"
                                                       minlength="3"
                                                       class="form-control @error('title') is-invalid @enderror"
                                                       placeholder="Заголовок ...">
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input type="hidden" name="is_active" value="0">
                                                    <input class="form-check-input"
                                                           type="checkbox" name="is_active" value="1"
                                                           @if($category->is_active)
                                                               checked="checked"
                                                        @endif
                                                    >
                                                    <label class="form-check-label">Опубликовать</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- select -->
                                            <div class="form-group">
                                                <label>Выбрать главную категорию</label>
                                                <select class="form-control" name="parent_id" >
                                                    <option value="0">... Категория не выбрана</option>
                                                    @foreach($general_categories as $general_category)

                                                        <option value="{{$general_category->id}}"

                                                                @if($general_category->id == $category->parent_id)
                                                                    selected
                                                            @endif
                                                        >
                                                            {{$general_category->title}}

                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Сохранить">
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="{{route('admin.post_categories.index')}}" class="btn btn-primary btn-block mb-3">Все категории</a>

                        <form method="POST" action="{{route('admin.post_categories.destroy', $category->id)}}">
                            @method('DELETE')
                            @csrf
                            <button type="submit"  class="btn btn-danger btn-block mb-3">
                                <i class="fas fa-trash"></i>
                                Удалить
                            </button>
                        </form>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection()
