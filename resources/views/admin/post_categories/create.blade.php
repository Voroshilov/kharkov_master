@extends('admin.layouts.app_create_template')

@section('title', 'Созадание категории')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Новая категория</h1>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Создание Категории</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">
                            <div class="card-body">
                                @php /** @var \App\Models\PostCategory */ @endphp
                                <form method="POST" action="{{route('admin.post_categories.store')}}">
                                    @method('POST')
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Название категории
                                                    @error('title') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="title"
                                                       minlength="3"
                                                       class="form-control @error('title') is-invalid @enderror"
                                                       placeholder="Заголовок ...">
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input type="hidden" name="is_published" value="0">
                                                    <input class="form-check-input" type="checkbox" name="is_published" value="1" checked>
                                                    <label class="form-check-label">Опубликовать</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- select -->
                                            <div class="form-group">
                                                <label>Выбрать главную категорию</label>
                                                <select class="form-control" name="parent_id">
                                                    <option value="0">... Категория не выбрана</option>
                                                    @foreach($general_categories as $general_category)
                                                        <option value="{{$general_category->id}}">{{$general_category->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Сохранить">
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="{{route('admin.post_categories.index')}}" class="btn btn-primary btn-block mb-3">Все категории</a>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection()
