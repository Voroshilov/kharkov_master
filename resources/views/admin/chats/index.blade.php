@extends('admin.layouts.app')

@section('title', 'Advices')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Чаты</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Чаты</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th>
                                <a class="btn btn-primary btn-sm" href="{{route('admin.chats.store')}}">
                                    Новая
                                </a>
                            </th>
                            <th>Время</th>
                            <th>Номер</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($chats as $chat)
                            <tr>
                                <td>
                                    <a class="btn btn-primary btn-sm" href="{{route('admin.chats.show', $chat->id)}}">
                                        {{$chat->id}}
                                    </a>

                                <td>
                                    {{date_format(new DateTime($chat->updated_at), 'd-m-y')}}
                                    <br/>
                                    {{date_format(new DateTime($chat->updated_at), 'H:m')}}
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            <a href="tel:{{$chat->phone}}">{{$chat->phone}}</a>
                                        </li>
                                    </ul>
                                </td>
                                <td class="project-actions text-right">
                                    <a class="btn btn-primary btn-sm" href="{{route('admin.chats.show', $chat->id)}}">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-->
            </div>
            <!-- /.card -->
        </section>
        <!-- /.content -->
    </div>

    <script>
        let socket = new WebSocket("ws://192.168.0.113:8080/");

        // let socket = new WebSocket("wss://javascript.info/article/websocket/demo/hello");

        socket.onopen = function (e) {
            console.log("[open] Connection established");
            console.log("Sending to server");
            // socket.send("My name is John");
            socket.send(e);
        };

        socket.onmessage = function (event) {
            document.getElementById('message').append(event.data)
            alert(`[message] Data received from server: ${event.data}`);
        };

        socket.onclose = function (event) {
            if (event.wasClean) {
                console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
            } else {
                // e.g. server process killed or network down
                // event.code is usually 1006 in this case
                console.log('[close] Connection died');
            }
        };

        socket.onerror = function (error) {
            console.log(`[error] ${error.message}`);
        };
    </script>
@endsection()
