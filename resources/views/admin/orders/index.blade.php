@extends('admin.layouts.app')

@section('title', 'orders')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Все статьи</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Заявки</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                @if(session('success'))
                    <div class="form-group alert alert-success">
                        <label class="col-form-label" for="inputSuccess">{{session()->get('success')}}</label>
                    </div>
                @endif
{{--                @if(session('search_error'))--}}
{{--                    <div class="form-group alert alert-success">--}}
{{--                        <label class="col-form-label" for="inputSuccess">{{session()->get('search_error')}}</label>--}}
{{--                    </div>--}}
{{--                @endif--}}
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <form method="GET" action="{{route('admin.orders.search')}}">
                                @method('GET')
                                @csrf

                                <th style="width: 5%">
                                    <input class="btn btn-primary" type="submit" value="Искать">
                                </th>
                                <th style="width: 10%"></th>
                                <th style="width: 15%">
                                    <input type="text"
                                           minlength="2"
                                           name="search_phone"
                                           id="search_phone"
                                           class="form-control"
                                           placeholder="Номер ..."
                                           value="@if(isset($_GET["search_phone"])){{$_GET["search_phone"]}}@endif"
                                    >
                                </th>
                                <th style="width: 25%">
                                    <input type="text"
                                           minlength="2"
                                           name="search_address"
                                           id="search_address"
                                           class="form-control"
                                           placeholder="Адрес ..."
                                           value="@if(isset($_GET["search_address"])){{$_GET["search_address"]}}@endif"
                                    >
                                </th>
                                <th style="width: 10%"></th>
                                <th style="width: 10%">
                                    <select class="form-control"
                                            name="search_tot_id"
                                            id="search_tot_id">
                                        <option value="">......</option>
                                        @foreach($type_of_technics as $tot)
                                            <option value="{{$tot->id}}"

                                                @if(isset($_GET["search_tot_id"]) && $tot->id == $_GET["search_tot_id"])
                                                    selected
                                                @endif
                                            >
                                                {{$tot->title}}
                                            </option>
                                        @endforeach
                                    </select>
                                </th>
                                <th style="width: 10%">
                                    <select class="form-control"
                                            name="search_status_id"
                                            id="search_status_id">
                                        <option value="">......</option>
                                        @foreach($statuses as $status)
                                            <option value="{{$status->id}}"

                                                @if(isset($_GET["search_status_id"]) && $status->id == $_GET["search_status_id"])
                                                    selected
                                                @endif
                                            >
                                                {{$status->title}}
                                            </option>
                                        @endforeach
                                    </select>
                                </th>
                                <th style="width: 10%">
{{--                                    <select class="form-control"--}}
{{--                                            name="search_master_id"--}}
{{--                                            id="search_master_id">--}}
{{--                                        <option value="">......</option>--}}
{{--                                        @foreach($masters as $master)--}}
{{--                                            <option value="{{$master->id}}"--}}

{{--                                                    @if(isset($_GET["search_master_id"]) && $master->id == $_GET["search_master_id"])--}}
{{--                                                        selected--}}
{{--                                                @endif--}}
{{--                                            >--}}
{{--                                                {{$master->name}}--}}
{{--                                            </option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
                                </th>
                                <th style="width: 5%"></th>
                            </form>
                        </tr>
                        <tr>
                            <th>
                                <a class="btn btn-primary btn-sm" href="{{route('admin.orders.create')}}">
                                    Новая
                                </a>
                            </th>
                            <th>Время</th>
                            <th>Номер</th>
                            <th>Адресс</th>
                            <th>Машинка</th>
                            <th>Тип тех.</th>
                            <th>Статус</th>
                            <th>Мастер</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($orders as $order)
                            <tr>
                                <td>
                                    <a class="btn btn-primary btn-sm" href="{{route('admin.orders.show', $order->id)}}">
                                        {{$order->id}}
                                    </a>

                                <td>

                                    @if(isset($order->run_time))
                                        {{date_format(new DateTime($order->run_time), 'd-m-y')}}
                                        <br/>
                                        {{date_format(new DateTime($order->run_time), 'H:m')}}
                                    @else
                                        {{date_format(new DateTime($order->created_at), 'd-m-y')}}
                                        <br/>
                                        {{date_format(new DateTime($order->created_at), 'H:m')}}
                                    @endif
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            <a href="tel:{{$order->phone}}">{{$order->phone}}</a>
                                        </li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            {{$order->address}}
                                        </li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            @if(isset($order->manufacturer->title))
                                                {{$order->manufacturer->title}}
                                            @else
                                                -??-
                                            @endif
                                            {{$order->machine}}
                                        </li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            @if(isset($order->typeOfTechnic->title))
                                                {{$order->typeOfTechnic->title}}
                                            @else
                                                не привязана
                                            @endif
                                        </li>
                                    </ul>
                                </td>
                                <td class="project-state">
                                        <span class="badge badge-success">
                                            {{$order->status->title}}

                                    @if($order->status->slug == 'empty')
                                        <i class="fa fa-hourglass-start" aria-hidden="true"></i>
                                    @elseif($order->status->slug == 'in_work')
                                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                                    @elseif($order->status->slug == 'is_ready')
                                        <i class="fa fa-flag-checkered" aria-hidden="true"></i>
                                    @elseif($order->status->slug == 'confirmed')
                                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                    @elseif($order->status->slug == 'canceled')
                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                    @endif
                                        </span>
                                </td>
                                <td>
                                    <ul class="list-inline">  <!--Или здесь аватар того кто создавал-->
                                        <li class="list-inline-item">
                                            @if(isset($order->master->first_name))
                                                {{$order->master->first_name}}
                                            @else
                                                    не привязана
                                            @endif
                                        </li>
                                    </ul>
                                </td>
                                <td class="project-actions text-right">
                                    <a class="btn btn-primary btn-sm" href="{{route('admin.orders.show', $order->id)}}">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <br>
                                    <a class="btn btn-info btn-sm" href="{{route('admin.orders.edit', $order->id)}}">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </section>
        <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
                {{--                {{$orders->links()}}--}}
                <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">{{$orders->currentPage()}}</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
            </ul>
        </div>

        @if($orders->total() > $orders->count())

            <div class="row justify-content-center">
                <div class="card">
                    <div class="card-body">
                        {{$orders->links()}}
                    </div>
                </div>
            </div>

        @endif
        <!-- /.content -->
    </div>

    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    </script>

@endsection()
