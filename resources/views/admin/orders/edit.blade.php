@extends('admin.layouts.app_create_template')

@section('title', 'Редактирование заявки')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Заявка №{{$order->id}}</h1>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Редактирование Заявки</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">
                            <div class="card-body">
                                @php /** @var \App\Models\Order */ @endphp
                                <form method="POST" action="{{route('admin.orders.update', $order->id)}}">
                                    @method('PATCH')
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Имя
                                                    @error('name') Не менее 2х символов @enderror
                                                </label>
                                                <input type="text" name="name"
                                                       minlength="2"
                                                       value="{{ $order->first_name }}"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       placeholder="Имя ...">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Телефон
                                                    @error('phone') Номер не менее 7 символов @enderror
                                                </label>
                                                <input type="text" name="phone"
                                                       minlength="5"
                                                       id="phone"
                                                       value="{{ $order->phone }}"
                                                       class="form-control @error('phone') is-invalid @enderror"
                                                       {{--                                                       placeholder="Телефон ..."--}}
                                                       data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Адрес
                                                    @error('address') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="address"
                                                       minlength="5"
                                                       value="{{ $order->address }}"
                                                       class="form-control @error('address') is-invalid @enderror"
                                                       placeholder="Адрес ...">
                                            </div>
                                            <div class="form-group">
                                                <label>Время начала заявки: {{ date_format(new DateTime($order->run_time), 'd-m-y h:m') }}</label>
                                                <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                                                    <input type="text" name="run_time"
                                                           class="form-control datetimepicker-input"
                                                           data-target="#reservationdatetime"
                                                           value="{{ date_format(new DateTime($order->run_time), 'd/m/y h:m') }}"/>
                                                    <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Тип заявки</label>
                                                <select class="form-control" name="order_time_id">
                                                    <option value="">... Тип заявки не выбран ...</option>
                                                    @foreach($order_times as $order_time)
                                                        <option value="{{$order_time->id}}"
                                                                @if($order->order_time_id == $order_time->id)
                                                                    selected
                                                            @endif
                                                        >{{$order_time->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Выбрать статус заявки</label>
                                                <select class="form-control" name="status_id">
                                                    @foreach($statuses as $status)
                                                        <option value="{{$status->id}}"
                                                                @if($order->status_id == $status->id)
                                                                    selected
                                                            @endif
                                                        >{{$status->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Выбрать мастера</label>
                                                <select class="form-control" name="master_id">
                                                    <option value="">... Мастер не выбран ...</option>
                                                    @foreach($masters as $master)
                                                        <option value="{{$master->id}}"
                                                                @if($order->master_id == $master->id)
                                                                    selected
                                                            @endif
                                                        >{{$master->first_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Выбрать тип техники</label>
                                                <select class="form-control" name="type_of_technic_id">
                                                    <option value="">... Техники не выбрана ...</option>
                                                    @foreach($type_of_technics as $tot)
                                                        <option value="{{$tot->id}}"
                                                                @if($order->type_of_technic_id == $tot->id)
                                                                    selected
                                                            @endif
                                                        >{{$tot->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Выбрать Производителя техники</label>
                                                {{--                                                <input type="text" value="" id="input_search" >--}}
                                                <select class="form-control" name="manufacturer_id" id="city">
                                                    <option value="">... Производитель не выбран ...</option>
                                                    @foreach($manufacturers as $manufacturer)
                                                        <option value="{{$manufacturer->id}}"
                                                                @if($order->manufacturer_id == $manufacturer->id)
                                                                    selected
                                                            @endif
                                                        >{{$manufacturer->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            {{--                                            </div> --}}
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Название техники (если нет в списке)
                                                </label>
                                                <input type="text" name="machine"
                                                       value="{{ $order->machine  }}"
                                                       class="form-control @error('machine') is-invalid @enderror"
                                                       placeholder="Название техники ...">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label" for="inputSuccess">
                                            Краткое описание
                                            @error('description') Не менее 5х символов @enderror
                                        </label>
                                        <textarea class="form-control @error('description') is-invalid @enderror"
                                                  name="description" minlength="5"
                                                  id="exampleFormControlTextarea1" rows="5">
                                                    {{ $order->description }}
                                                </textarea>
                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Сохранить">
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="{{route('admin.orders.index')}}" class="btn btn-primary btn-block mb-3">Все Заявки</a>
                        <a href="{{route('admin.orders.create')}}" class="btn btn-primary btn-block mb-3">Новая заявка</a>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

    <script>
        //Код jQuery, установливающий маску для ввода телефона элементу input
        //1. После загрузки страницы,  когда все элементы будут доступны выполнить...
        $(function(){
            //2. Получить элемент, к которому необходимо добавить маску
            $("#phone").mask("(999) 999-9999");
        });
    </script>
@endsection()
