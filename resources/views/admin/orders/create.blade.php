@extends('admin.layouts.app_create_template')

@section('title', 'Созадание заявки')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Новая заявка</h1>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Создание Заявки</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">

                            <div class="card-body">
                                @php /** @var \App\Models\Order */ @endphp
                                <form method="POST" action="{{route('admin.orders.store')}}">
                                    @method('POST')
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Имя
                                                    @error('name') Не менее 2х символов @enderror
                                                </label>
                                                <input type="text" name="name"
                                                       minlength="2"
                                                       value="{{ old('name') }}"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       placeholder="Имя ...">
                                            </div>

<!--                                            Поиск в INPUT-->

{{--                                            <input  id="phone" type="text">--}}

{{--                                            <div class="form-group">--}}
{{--                                                <label>US phone mask:</label>--}}

{{--                                                <div class="input-group">--}}
{{--                                                    <div class="input-group-prepend">--}}
{{--                                                        <span class="input-group-text"><i class="fas fa-phone"></i></span>--}}
{{--                                                    </div>--}}
{{--                                                    <input type="text" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask>--}}
{{--                                                </div>--}}
{{--                                                <!-- /.input group -->--}}
{{--                                            </div>--}}
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Телефон
                                                    @error('phone') Номер не менее 7 символов @enderror
                                                </label>
                                                <input type="number" name="phone"
                                                       minlength="5"
                                                       id="phone"
                                                       value="{{ old('phone') }}"
                                                       class="form-control @error('phone') is-invalid @enderror"
{{--                                                       placeholder="Телефон ..."--}}
                                                       data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Адрес
                                                    @error('address') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="address"
                                                       minlength="5"
                                                       value="{{ old('address') }}"
                                                       class="form-control @error('address') is-invalid @enderror"
                                                       placeholder="Адрес ...">
                                            </div>
                                            <div class="form-group">
                                                <label>Время начала заявки</label>
                                                <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                                                    <input type="text" name="run_time"
                                                           class="form-control datetimepicker-input"
                                                           data-target="#reservationdatetime"
                                                           value="{{ old('run_time') }}"/>
                                                    <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Тип заявки</label>
                                                <select class="form-control" name="order_time_id">
                                                    <option value="">... Тип заявки не выбран ...</option>
                                                    @foreach($order_times as $order_time)
                                                        <option value="{{$order_time->id}}"
                                                        @if(old('order_time_id') == $order_time->id)
                                                            selected
                                                        @endif
                                                        >{{$order_time->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Выбрать мастера</label>
                                                <select class="form-control" name="master_id">
                                                    <option value="">... Мастер не выбран ...</option>
                                                    @foreach($masters as $master)
                                                        <option value="{{$master->id}}"
                                                            @if(old('master_id') == $master->id)
                                                                selected
                                                            @endif
                                                        >{{$master->first_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Выбрать тип техники</label>
                                                <select class="form-control" name="type_of_technic_id">
                                                    <option value="">... Техники не выбрана ...</option>
                                                    @foreach($type_of_technics as $tot)
                                                        <option value="{{$tot->id}}"
                                                            @if(old('type_of_technic_id') == $tot->id)
                                                                selected
                                                            @endif
                                                        >{{$tot->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Выбрать Производителя техники</label>
{{--                                                <input type="text" value="" id="input_search" >--}}
                                                <select class="form-control" name="manufacturer_id" id="city">
                                                    <option value="">... Производитель не выбран ...</option>
                                                    @foreach($manufacturers as $manufacturer)
                                                        <option value="{{$manufacturer->id}}"
                                                            @if(old('manufacturer_id') == $manufacturer->id)
                                                                selected
                                                            @endif
                                                        >{{$manufacturer->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
{{--                                            </div> --}}
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Название техники (если нет в списке)
                                                </label>
                                                <input type="text" name="machine"
                                                       value="{{ old('machine') }}"
                                                       class="form-control @error('machine') is-invalid @enderror"
                                                       placeholder="Название техники ...">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label" for="inputSuccess">
                                            Краткое описание
                                            @error('description') Не менее 5х символов @enderror
                                        </label>
                                        <textarea class="form-control @error('description') is-invalid @enderror"
                                                  name="description" minlength="5"
                                                  id="exampleFormControlTextarea1" rows="5">
                                                    {{ old('description') }}
                                                </textarea>
                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Сохранить">
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="{{route('admin.orders.index')}}" class="btn btn-primary btn-block mb-3">Все Заявки</a>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

    <script>
        //Код jQuery, установливающий маску для ввода телефона элементу input
        //1. После загрузки страницы,  когда все элементы будут доступны выполнить...
        $(function(){
            //2. Получить элемент, к которому необходимо добавить маску
            $("#phone").mask("(999) 999-9999");
        });
    </script>

    <!--                                            Поиск в INPUT-->
{{--    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>--}}
{{--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>--}}

{{--    <style>--}}
{{--        #city {--}}
{{--            display: compact;--}}
{{--        }--}}
{{--    </style>--}}

{{--    <script>--}}
{{--        var options = $("#city option");--}}
{{--        var array_option = new Array();--}}
{{--        for(var i=1; i<options.length; i++) {--}}
{{--            array_option.push(options[i].text);--}}
{{--        }--}}
{{--        $("#input_search").autocomplete({--}}
{{--            source: array_option,--}}
{{--            minLength: 1 // Количество символов, от скольки начинать поиск--}}
{{--        });--}}
{{--        $.expr[":"].exact = $.expr.createPseudo(function(arg) {--}}
{{--            return function(element) {--}}
{{--                return $(element).text() === arg.trim();--}}
{{--            };--}}
{{--        });--}}
{{--        $(document).on("click", ".ui-widget-content li div", function() {--}}
{{--            var target_option = $(this).text();--}}
{{--            $("#city option:exact("+target_option+")").attr("selected", "selected");--}}
{{--        });--}}
{{--    </script>--}}
@endsection()
