@extends('admin.layouts.app')

@section('title', "Заявка - {{$order->id}}")

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1><b>Заголовок -</b> {{$order->id}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('admin.orders.index')}}">Заявки</a></li>
                            <li class="breadcrumb-item active">{{$order->id}}</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-md-7">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">{{$order->id}}</h3>
<!--                                Можно будет использовать что бы листать посты-->
{{--                                <div class="card-tools">--}}
{{--                                    <a href="#" class="btn btn-tool" title="Previous"><i class="fas fa-chevron-left"></i></a>--}}
{{--                                    <a href="#" class="btn btn-tool" title="Next"><i class="fas fa-chevron-right"></i></a>--}}
{{--                                </div>--}}
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0">
                                <!-- /.mailbox-controls -->
                                <div class="mailbox-read-message">
                                    <p>Время / Дата:
                                        <b>
                                            {{date_format(new DateTime($order->run_time), 'H:m')}} /
                                            {{date_format(new DateTime($order->run_time), 'd-m-y')}}
                                        </b>
                                    </p>
                                    <p>Имя: <b>{{$order->first_name}}</b></p>
                                    <p>Номер: <a href="tel:{{$order->phone}}">{{$order->phone}}</a></p>
                                    <p>Адрес: <b>{{$order->address}}</b></p>
                                    <p>Статус: <b>{{$order->status->title}}</b></p>
                                    <p>Сложность: <b>{{$order->orderTime->title}}</b></p>

                                    <b>Жалоба:</b>
                                    <p>{{$order->description}}</p>
                                    <br>

                                    {!! $order->text !!}

                                </div>
                                <!-- /.mailbox-read-message -->
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Производитель и модель</h3>
                            </div>
                            <div class="card-body p-0">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item active">
                                        <a href="#" class="nav-link">
                                            <b>{{$order->manufacturer->title}}</b>
                                            -
                                            {{$order->machine}}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Производитель и модель</h3>
                            </div>
                            <div class="card-body p-0">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item active">
                                        <a href="#" class="nav-link">
                                            <b>{{$order->typeOfTechnic->title}}</b>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Мастер</h3>
                                </div>
                                <div class="card-body p-0">
                                    <ul class="nav nav-pills flex-column">
                                        <li class="nav-item active">
                                            @if(isset($order->master->id))
                                            <a href="{{route('admin.users.show', $order->master->id )}}" class="nav-link">
                                                <b>{{$order->master->first_name}}</b>
                                            </a>
                                            @else
                                                <a href="#" class="nav-link" style="color: red">
                                                    <b>Мастер не привязан!!!</b>
                                                </a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                        </div>
<!--                        Для админа!!!-->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Создатель</h3>
                            </div>
                            <div class="card-body p-0">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item active">
                                        <a href="{{route('admin.users.show', $order->creator->id )}}" class="nav-link">
                                            <b>{{$order->creator->first_name}}</b>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Редактор</h3>
                            </div>
                            <div class="card-body p-0">
                                <ul class="nav nav-pills flex-column">
                                    <li class="nav-item active">
                                        <a href="{{route('admin.users.show', $order->editor->id )}}" class="nav-link">
                                            <b>{{$order->editor->first_name}}</b>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a href="{{route('admin.orders.create')}}" class="btn btn-primary btn-block mb-3">Сохдать новый совет</a>
                        <a href="{{route('admin.orders.index')}}" class="btn btn-primary btn-block mb-3">Все советы</a>
                        <a href="{{route('admin.orders.edit', $order->id)}}" class="btn btn-success btn-block mb-3">Редактировать</a>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection()
