@extends('admin.layouts.app')

@section('title', "Производитель - {{$manufacturer->title}}")

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1><b>Производитель -</b>{{$manufacturer->title}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">{{$manufacturer->title}}</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-md-9">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
{{--                                <h3 class="card-title">{{$manufacturer->title}}</h3>--}}
                                <img class="img-fluid" src="{{asset($manufacturer->image)}}"
                                     alt="{{$manufacturer->title}}" class="img-thumbnail">
                            </div>
                            <div class="col-md-6">
                                <div class="card-body p-0">
                                    <!-- /.mailbox-controls -->
                                    <div class="mailbox-read-message">
                                        <b>Текст:</b>
                                        <br>
                                        {!! $manufacturer->description !!}
                                    </div>
                                    <!-- /.mailbox-read-message -->
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>


                    <div class="col-md-3">
                        <a href="{{route('admin.manufacturers.create')}}" class="btn btn-primary btn-block mb-3">Сохдать</a>
                        <a href="{{route('admin.manufacturers.index')}}" class="btn btn-primary btn-block mb-3">Все</a>
                        <a href="{{route('admin.manufacturers.edit', $manufacturer->id)}}" class="btn btn-primary btn-block mb-3">Редактировать</a>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection()
