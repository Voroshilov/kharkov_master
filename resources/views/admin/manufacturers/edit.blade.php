@extends('admin.layouts.app_create_template')

@section('title', 'Редактирование')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Редактируем категорию</h1>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Редактируем </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">
                            <div class="card-body">
                                @php /** @var \App\Models\Manufacturer */ @endphp
                                <form method="POST"
                                      action="{{route('admin.manufacturers.update', $manufacturer->id)}}"
                                      enctype="multipart/form-data">
                                    @method('PATCH')
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Название Производителя
                                                    @error('title') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="title"
                                                       minlength="3"
                                                       class="form-control @error('title') is-invalid @enderror"
                                                       placeholder="Заголовок ..."
                                                       value="{{ $manufacturer->title }}"
                                                >
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Краткое описание
                                                    @error('title') Не менее 5х символов @enderror
                                                </label>
                                                <textarea class="form-control" name="description"
                                                          id="exampleFormControlTextarea1" rows="5">
                                                    {{ $manufacturer->description }}
                                                </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Загрузка картинки
                                                </label>
                                                <br>
                                                <div class="col-md-6">
                                                    <input type="file" id="file" name="image" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input type="hidden" name="is_active" value="0">
                                                    <input class="form-check-input"
                                                           type="checkbox" name="is_active" value="1"
                                                           @if($manufacturer->is_active)
                                                               checked="checked"
                                                           @endif
                                                    >
                                                    <label class="form-check-label">Опубликовать</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <img src="{{asset('/uploads_images/manufacturers/small/' . $manufacturer->image)}}"
                                                     alt="{{$manufacturer->title}}" class="img-thumbnail">
                                            </div>
                                            <div class="row">
                                                <span id="output"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Сохранить">
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="{{route('admin.manufacturers.index')}}" class="btn btn-primary btn-block mb-3">Все категории</a>

                        <form method="POST" action="{{route('admin.manufacturers.destroy', $manufacturer->id)}}">
                            @method('DELETE')
                            @csrf
                            <button type="submit"  class="btn btn-danger btn-block mb-3">
                                <i class="fas fa-trash"></i>
                                Удалить
                            </button>
                        </form>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

    @include('admin.layouts.includes.preview_image')

@endsection()
