@extends('admin.layouts.app_create_template')

@section('title', 'Созадание статьи')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Новая статья</h1>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Создание Поста</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <!-- general form elements disabled -->
                        <div class="card card-warning">


                            <div class="card-body">
                                @php /** @var \App\Models\PostCategory */ @endphp
                                <form method="POST" action="{{route('admin.posts.store')}}" enctype="multipart/form-data">
                                    @method('POST')
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">
                                                    Заголовок
                                                    @error('title') Не менее 5х символов @enderror
                                                </label>
                                                <input type="text" name="title"
                                                       minlength="5"
                                                       value="{{ old('title') }}"
                                                       class="form-control @error('title') is-invalid @enderror"
                                                       placeholder="Заголовок ...">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">Ключевые слова</label>
                                                <input type="text" name="keywords" class="form-control"
                                                       value="{{ old('keywords') }}" placeholder="Заголовок ...">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="inputSuccess">Краткое описание</label>
                                                <input type="text" name="short_text" class="form-control"
                                                       value="{{ old('short_text') }}" placeholder="Заголовок ...">
                                            </div>
                                            <!-- checkbox -->
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input type="hidden" name="is_published" value="0">
                                                    <input class="form-check-input" type="checkbox" name="is_published" value="1" checked>
                                                    <label class="form-check-label">Опубликовать</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- select -->
                                            <div class="form-group">
                                                <label>Выбрать категорию</label>
                                                <select class="form-control" name="category_id">
                                                    <option value="">... Категория не выбрана ...</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Выбрать Производителя техники</label>
                                                <select class="form-control" name="manufacturer_id">
                                                    @foreach($manufacturers as $manufacturer)
                                                        <option value="{{$manufacturer->id}}">{{$manufacturer->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body">
                                        <div class="form-group">
                                            @error('text')
                                            <h3 style="color: red">
                                                Нужно заполнить Текстовое поле!!!
                                            </h3>
                                            @enderror
                                            <textarea id="compose-textarea"
                                                      minlength="10"
                                                      name="text"
                                                      class="form-control "
                                                      style="height: 350px"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="btn btn-default btn-file">
                                                <i class="fas fa-paperclip"></i> Загрузить фото
                                                <input type="file" name="attachment">
                                            </div>
                                            <p class="help-block">Max. 32MB</p>
                                        </div>
                                    </div>

                                    <input class="btn btn-primary" type="submit" value="Сохранить">
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="{{route('admin.posts.index')}}" class="btn btn-primary btn-block mb-3">Все статьи</a>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection()
