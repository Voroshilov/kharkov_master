@extends('admin.layouts.app')

@section('title', 'Тип быт. техники')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Тип быт. техники</h1>
                        <form method="GET" action="{{route('admin.type_of_technics.search')}}">
                            @method('GET')
                            @csrf
                            <div class="form-group">
                                <input type="text"
                                       name="search"
                                       id="search"
                                       minlength="2"
                                       class="form-control"
                                       placeholder="Введи незвание ..."
                                       value="@if(isset($_GET["search"])){{$_GET["search"]}}@endif"
                                >
                            </div>
                            <input class="btn btn-primary" type="submit" value="Искать">
                        </form>
                    </div>
                    <div class="col-sm-6">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Список типов быт. техники</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                @if(session('success'))
                    <div class="form-group alert alert-success">
                        <label class="col-form-label" for="inputSuccess">{{session()->get('success')}}</label>
                    </div>
                @endif
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">#</th>
                            <th style="width: 15%">Название</th>
                            <th style="width: 10%">Фото</th>
                            <th>
                                Краткое описание
                            </th>
                            <th style="width: 5%" class="text-center">
                                Статус
                            </th>
                            <th style="width: 13%">
                                <a class="btn btn-primary btn-sm" href="{{route('admin.type_of_technics.create')}}">
                                    Добавить новый
                                </a>
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($type_of_technics as $tot)
                            <tr>
                                <td>{{$tot->id}}</td>
                                <td>
                                    {{$tot->title}}
                                </td>
                                <td>
                                    <img src="{{asset('/uploads_images/type_of_technics/logo/' . $tot->image)}}"
                                         alt="{{$tot->title}}" class="img-thumbnail">
                                </td>
                                <td>
                                    {{$tot->description}}
                                </td>
                                <td class="project-state">
                                    @if($tot->is_active == 1)
                                        <span class="badge badge-success">Активна</span>
                                    @else
                                        <span class="badge badge-lose">Не активна</span>
                                    @endif
                                </td>
                                <td class="project-actions text-right">
{{--                                    <a class="btn btn-primary btn-sm" href="{{route('admin.type_of_technics.show', $tot->id)}}">--}}
{{--                                        <i class="fas fa-folder">--}}
{{--                                        </i>--}}
{{--                                        Просмотр--}}
{{--                                    </a>--}}
                                    <a class="btn btn-info btn-sm" href="{{route('admin.type_of_technics.edit', $tot->id)}}">
{{--                                        <i class="fas fa-pencil-alt">--}}
{{--                                        </i>--}}
                                        Редактор
                                    </a>
                                    <button class="btn btn-danger btn-sm"
                                            data-href="{{route('admin.type_of_technics.destroy', $tot->id)}}"
                                            data-toggle="modal" data-target="#confirm-delete-{{$tot->id}}">
{{--                                        <i class="fas fa-trash"></i>--}}
                                        Удалить
                                    </button>

                                    <form method="POST" action="{{route('admin.type_of_technics.destroy', $tot->id)}}">
                                        @method('DELETE')
                                        @csrf
                                        <div class="modal fade" id="confirm-delete-{{$tot->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        Подтверждение
                                                    </div>
                                                    <div class="modal-body">
                                                        Действительно хотите удалить запись: {{$tot->title}} ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                                                        <button type="submit"  class="btn btn-danger btn-sm">
                                                            <i class="fas fa-trash"></i>
                                                            Удалить
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>

@endsection()
