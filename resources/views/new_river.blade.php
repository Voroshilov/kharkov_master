
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
</head>
<body>
<script>
    function rieka(x, y, pole) {
        var vystup = [
            [13, 15, 3, 1, 5, 10, 13, 13, 15],
            [13, 15, 3, 0, 0, 10, 13, 13, 15],
            [13, 15, 0, 0, 0, 10, 13, 13, 15],
            [13, 15, 3, 0, 1, 10, 13, 13, 15],
            [13, 15, 3, 0, 1, 10, 13, 13, 15],
            [11, 12, 2, 1, 2, 11, 10, 11, 12],
            [11, 2, 3, 5, 2, 5, 7, 6, 24],
            [3, 2, 3, 5, 2, 3, 4, 5, 14],
            [5, 4, 3, 4, 3, 4, 5, 6, 18],
            [17, 9, 7, 4, 7, 8, 9, 10, 57],
            [13, 8, 6, 6, 5, 6, 7, 9, 25],
            [11, 10, 8, 7, 6, 8, 9, 11, 35],
            [13, 10, 8, 10, 11, 12, 13, 15, 19],
            [15, 11, 10, 9, 9, 10, 11, 12, 20],
        ];
        var minXY = pole[x][y];
        var minX = x;
        var minY = y;
        var endOfRiver = false;
        var xEnd = 1;
        var yEnd = 1;
        var xBeg = 1;
        var yBeg = 1;
        do {
            if (x === pole.length - 1) {
                xEnd = 0;
            }
            if (x === 0) {
                xBeg = 0;
            }
            if (y === pole[0].length - 1) {
                yEnd = 0;
            }
            if (y === 0) {
                yBeg = 0;
            }
            if (pole[x - xBeg][y - yBeg] <= minXY && x - 1 >= 0 && y - 1 >= 0) {
                minXY = pole[x - xBeg][y - yBeg];
                minX = x - 1;
                minY = y - 1;
            }
            if (pole[x - xBeg][y] <= minXY && x - 1 >= 0) {
                minXY = pole[x - xBeg][y];
                minX = x - 1;
                minY = y;
            }
            if (
                pole[x - xBeg][y + yEnd] <= minXY &&
                x - 1 >= 0 &&
                y + 1 <= pole[0].length
            ) {
                minXY = pole[x - xBeg][y + yEnd];
                minX = x - 1;
                minY = y + 1;
            }
            if (pole[x][y - yBeg] <= minXY && y - 1 >= 0) {
                minXY = pole[x][y - yBeg];
                minX = x;
                minY = y - 1;
            }
            if (pole[x][y + yEnd] <= minXY && y + 1 <= pole[0].length) {
                minXY = pole[x][y + yEnd];
                minX = x;
                minY = y + 1;
            }
            if (
                pole[x + xEnd][y - yBeg] <= minXY &&
                x + 1 <= pole.length &&
                y - 1 >= 0
            ) {
                minXY = pole[x + xEnd][y - yBeg];
                minX = x + 1;
                minY = y - 1;
            }
            if (pole[x + xEnd][y] <= minXY && x + 1 <= pole.length) {
                minXY = pole[x + xEnd][y];
                minX = x + 1;
                minY = y;
            }
            if (
                pole[x + xEnd][y + yEnd] <= minXY &&
                x + 1 <= pole.length &&
                y + 1 <= pole[0].length
            ) {
                minXY = pole[x + xEnd][y + yEnd];
                minX = x + 1;
                minY = y + 1;
            }
            if (minX === x && minY === y) {
                endOfRiver = true;
            }
            vystup[x][y] = 1;
            x = minX;
            y = minY;
        } while (endOfRiver === false);
        for (k = 0; k < vystup.length; k++) {
            for (l = 0; l < vystup[0].length; l++) {
                if (vystup[k][l] > 1) {
                    vystup[k][l] = 0;
                }
            }
        }

        /*
          //TODO sem vyplnit svoj kod (ak treba mozte aj inde)
          */

        printArray(vystup);
    }

    function printArray(array) {
        // len na vykreslenie matice s 0,1 prvkami
        var table = "<table>";
        for (var i = 0; i < array.length; i++) {
            table += "<tr>";
            for (var j = 0; j < array[0].length; j++) {
                table +=
                    array[i][j] > 0
                        ? '<th style="background-color:#ff0000;width:20px;height:20px"></th>'
                        : '<th style="background-color:#ffff00;width:20px;height:20px"></th>';
            }
            table += "</tr>";
        }
        table += "</table></br>";
        document.writeln(table);
    }

    let art0 = [
        [13, 15, 3, 1, 5, 10, 13, 13, 15],
        [13, 15, 3, 0, 0, 10, 13, 13, 15],
        [13, 15, 0, 0, 0, 10, 13, 13, 15],
        [13, 15, 3, 0, 1, 10, 13, 13, 15],
        [13, 15, 3, 0, 1, 10, 13, 13, 15],
        [11, 12, 2, 1, 2, 11, 10, 11, 12],
        [11, 2, 3, 5, 2, 5, 7, 6, 24],
        [3, 2, 3, 5, 2, 3, 4, 5, 14],
        [5, 4, 3, 4, 3, 4, 5, 6, 18],
        [17, 9, 7, 4, 7, 8, 9, 10, 57],
        [13, 8, 6, 6, 5, 6, 7, 9, 25],
        [11, 10, 8, 7, 6, 8, 9, 11, 35],
        [13, 10, 8, 10, 11, 12, 13, 15, 19],
        [15, 11, 10, 9, 9, 10, 11, 12, 20],
    ];

    //nemente zaciatocne suradnice pre tieto priklady;
    rieka(13, 8, art0);
    //   rieka(5, 6, art1);
    //   rieka(10, 10, art2);
    //   rieka(0, 0, art3);
    //   rieka(1, 2, art4);
    //   rieka(5, 6, art5);
    //   rieka(0, 23, art6);
    //   rieka(0, 7, art7);
    //   rieka(0, 0, art8);
</script>
</body>
</html>
