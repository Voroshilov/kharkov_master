@extends('blog.layouts.app')

@section('title', 'Главная')

@section('content')
    <section id="container">
        <div class="wrap-container">
            <section class="content-box box-style-1 box-2">
                <div class="zerogrid">
                    <div class="wrap-box"><!--Start Box-->

                        <div class="card_header">Привет {{Auth::user()->first_name}}</div>
                        <div class="card">
                            <div class="first_block">
                                <div class="card_body">
                                    <div class="link_title">
                                        Полезные ссылки и информация
                                    </div>
                                    <div class="user_links">
                                        @role('admin')
                                        <a href="{{ route('admin.home') }}">
                                            Войти в админ-панель
                                        </a>
                                        @endrole
                                    </div>

                                    <div class="user_role">
                                        Статус пользователя -
                                        <span>
                                            @role('admin')
                                            Admin
                                            @endrole
                                            @role('master')
                                            Master
                                            @endrole
                                        </span>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div class="second_block">
                                <div class="user_card">
                                    <div class="logo">
                                        <img src="@if(isset($user->avatar))
                                        {{asset($user->avatar)}}
                                        @else
                                            /admin/dist/img/default-150x150.png
                                         @endif"
                                             class="img-circle elevation-2 avatar_home" alt="User Image">
                                    </div>
                                    <div class="short_info">
                                        <div>{{$user->first_name}}</div>
                                        <div>{{$user->second_name}}</div>
                                        <div>{{$user->phone}}</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
